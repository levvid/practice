import java.util.ArrayDeque;
import java.util.Deque;

public class LongestFilePath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String file = "user\r\tpictures\r\t\tphoto.png\r\t\tcamera\r\tdocuments\r\t\tlectures\r\t\t\tnotes.txt";
		System.out.println("Longest Path: " + longestPath(file));
	}
	
	
	static int longestPath(String input) {
	    Deque<Integer> stack = new ArrayDeque<>();
	    stack.push(0); // "dummy" length
	    int maxLen = 0;
	    String[] str = input.split("\\n+"); 
	    System.out.println(str.length);
	    for(String s:str){
	        int lev = s.lastIndexOf("\t")+1; // number of "\t"
	        System.out.println(s);
	        while(lev+1<stack.size()) stack.pop(); // find parent
	            int len = stack.peek()+s.length()-lev+1; // remove "/t", add"/"
	            stack.push(len);
	            // check if it is file
	            if(s.contains(".")) maxLen = Math.max(maxLen, len-1); 
	        }
	        return maxLen;
	}


}
