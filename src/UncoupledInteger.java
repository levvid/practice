import java.io.*;
import java.util.*;

public class UncoupledInteger {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
//        Scanner sc = new Scanner(System.in).useDelimiter(", ");
//        int b = 0;
//        while(sc.hasNext()){
//            b = b ^ sc.nextInt();
//        }

    	
    	Scanner sc = new Scanner(System.in);
    	String[] str = sc.nextLine().split(", ");
    	int b = 0;
    	for(int i = 0; i< str.length; i++){
    		b = b ^ Integer.parseInt(str[i]);
    	}
    	
    	System.out.println(b);
        
    }
}