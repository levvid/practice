import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.stream.LongStream;

public class ExtremeElement {

	public static void main(String[] args) {
		int[] a = {9, 4, -3, -10};
		System.out.println(solution(a));
	}
	
	public static int solution(int[] a){
		if(a.length == 0){
			return -1;
		}
		
		return findExtremeElement(a);
		
	}
	
	public static BigDecimal findAverage(int[] a){
		BigInteger sum = BigInteger.ZERO;
		for(int i = 0; i < a.length; i++){
		    sum = sum.add(BigInteger.valueOf(a[i]));
		}
		BigDecimal average = new BigDecimal(sum).divide(BigDecimal.valueOf(a.length));
		return average;
	}
	
	public static int findExtremeElement(int[] a){
		BigDecimal average = findAverage(a);
		int indexOfExtremeElement = 0;
		BigDecimal maxDeviation = BigDecimal.ZERO;
		for(int i = 0; i < a.length; i++){
			BigDecimal deviation = average.subtract(BigDecimal.valueOf(a[i])).abs();
			if(deviation.compareTo(maxDeviation) == 1){
				maxDeviation = deviation;
				indexOfExtremeElement = i;
			}
		}
		return indexOfExtremeElement;
	}

}









/*********************************************************************************************/
//you can also use imports, for example:
//import java.util.*;

//you can write to stdout for debugging purposes, e.g.
//System.out.println("this is a debug message");

//import java.math.BigDecimal;
//import java.math.BigInteger;
//
//
//class Solution {    
// public int solution(int[] A){
//		if(A.length == 0){ //return -1 if A is empty
//			return -1;
//		}
//		return findExtremeElement(A);
//	}
//	
//	
//	/**Finds and returns the average of the elements in array a as a BigDecimal**/
//	public  BigDecimal findAverage(int[] a){
//		BigInteger sum = BigInteger.ZERO;
//		for(int i = 0; i < a.length; i++){
//		    sum = sum.add(BigInteger.valueOf(a[i]));
//		}
//		BigDecimal average = new BigDecimal(sum).divide(BigDecimal.valueOf(a.length));
//		return average;
//	}
//	
//	
//	/**Finds and returns the index of the first occurence of an extreme element 
//	in array a**/
//	public int findExtremeElement(int[] a){
//		BigDecimal average = findAverage(a); //find the average
//		int indexOfExtremeElement = 0; //the index of the max element
//		BigDecimal maxDeviation = BigDecimal.ZERO; //to track the deviation of the max element so far in the array
//		for(int i = 0; i < a.length; i++){
//			BigDecimal deviation = average.subtract(BigDecimal.valueOf(a[i])).abs();
//			if(deviation.compareTo(maxDeviation) == 1){
//				maxDeviation = deviation;
//				indexOfExtremeElement = i;
//			}
//		}
//		return indexOfExtremeElement;
//	}
//}
