
public class FIrstNotRepeatingCharacter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "abacabad";
		firstNotRepeatingCharacter(s);
	}
	
	public static char firstNotRepeatingCharacter(String s) {
		byte[] flags = new byte[256]; //all is initialized by 0 

	    for (int i = 0; i < s.length(); i++) { // O(n)
	        flags[(int)s.charAt(i)]++ ;
	    }

	    for (int i = 0; i < s.length(); i++) { // O(n)
	        if(flags[(int)s.charAt(i)] == 1)
	            return s.charAt(i);
	    }

	    return '_';

	}
	
	char firstNotRepeatingCharacter2(String s) {
	    char[] c=s.toCharArray();
	for(int i=0;i<s.length();i++){
	    if(s.indexOf(c[i])==s.lastIndexOf(c[i]))
	        return c[i];
	}
	    return '_';
	}



}
