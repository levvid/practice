
public class PrefixSums {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	/*************************************************************************************
	 * 
	 * @param A
	 * @param B
	 * @param K
	 * @return
	 * 
	 * Write a function:

		class Solution { public int solution(int A, int B, int K); }
		that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:
		
		{ i : A ≤ i ≤ B, i mod K = 0 }
		For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.
		
		Assume that:
		
		A and B are integers within the range [0..2,000,000,000];
		K is an integer within the range [1..2,000,000,000];
		A ≤ B.
		Complexity:
		
		expected worst-case time complexity is O(1);
		expected worst-case space complexity is O(1).
	 */
	public static int CountDiv(int A, int B, int K){
		
		int diffs = B/K - A/K;
		if(A%K == 0)
			return diffs+1;
		return diffs;
	}
	
	
	/************************************************************************************************
	 * 
	 * @param A
	 * @return
	 * 
	 * A non-empty zero-indexed array A consisting of N integers is given. The consecutive elements of array A represent consecutive cars on a road.

		Array A contains only 0s and/or 1s:
		
		0 represents a car traveling east,
		1 represents a car traveling west.
		The goal is to count passing cars. We say that a pair of cars (P, Q), where 0 ≤ P < Q < N, is passing when P is traveling to the east and Q is traveling to the west.
		
		For example, consider array A such that:
		
		  A[0] = 0
		  A[1] = 1
		  A[2] = 0
		  A[3] = 1
		  A[4] = 1
		We have five pairs of passing cars: (0, 1), (0, 3), (0, 4), (2, 3), (2, 4).
		
		Write a function:
		
		class Solution { public int solution(int[] A); }
		that, given a non-empty zero-indexed array A of N integers, returns the number of pairs of passing cars.
		
		The function should return −1 if the number of pairs of passing cars exceeds 1,000,000,000.
		
		For example, given:
		
		  A[0] = 0
		  A[1] = 1
		  A[2] = 0
		  A[3] = 1
		  A[4] = 1
		the function should return 5, as explained above.
		
		Assume that:
		
		N is an integer within the range [1..100,000];
		each element of array A is an integer that can have one of the following values: 0, 1.
		Complexity:
		
		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	public static int passingCars(int[] A){
		int numPairs = 0, numOnes = 0;
		int[] b = new int[A.length];
        for(int i = A.length - 1; i >= 0; i--){ //save number of ones on the rest of array into array b 
            if(A[i] == 1){
            	numOnes++;
            }
            b[i] = numOnes;
        }
        
        for(int i = 0; i < A.length; i++){
        	if(A[i] == 0){
        		numPairs += b[i];
        	}
        	if(numPairs > 1000000000)
        		return -1;
        }
		
		return numPairs;
	}
	
	/*******************************************
	 * 
	 * @param S
	 * @param P
	 * @param Q
	 * @return
	 * 
	 * 
	 * A DNA sequence can be represented as a string consisting of the letters A, C, G and T, which correspond to the types of successive nucleotides in the sequence. Each nucleotide has an impact factor, which is an integer. Nucleotides of types A, C, G and T have impact factors of 1, 2, 3 and 4, respectively. You are going to answer several queries of the form: What is the minimal impact factor of nucleotides contained in a particular part of the given DNA sequence?

		The DNA sequence is given as a non-empty string S = S[0]S[1]...S[N-1] consisting of N characters. There are M queries, which are given in non-empty arrays P and Q, each consisting of M integers. The K-th query (0 ≤ K < M) requires you to find the minimal impact factor of nucleotides contained in the DNA sequence between positions P[K] and Q[K] (inclusive).
		
		For example, consider string S = CAGCCTA and arrays P, Q such that:
		
		    P[0] = 2    Q[0] = 4
		    P[1] = 5    Q[1] = 5
		    P[2] = 0    Q[2] = 6
		The answers to these M = 3 queries are as follows:
		
		The part of the DNA between positions 2 and 4 contains nucleotides G and C (twice), whose impact factors are 3 and 2 respectively, so the answer is 2.
		The part between positions 5 and 5 contains a single nucleotide T, whose impact factor is 4, so the answer is 4.
		The part between positions 0 and 6 (the whole string) contains all nucleotides, in particular nucleotide A whose impact factor is 1, so the answer is 1.
		Write a function:
		
		class Solution { public int[] solution(String S, int[] P, int[] Q); }
		
		that, given a non-empty zero-indexed string S consisting of N characters and two non-empty zero-indexed arrays P and Q consisting of M integers, returns an array consisting of M integers specifying the consecutive answers to all queries.
		
		The sequence should be returned as:
		
		a Results structure (in C), or
		a vector of integers (in C++), or
		a Results record (in Pascal), or
		an array of integers (in any other programming language).
		For example, given the string S = CAGCCTA and arrays P, Q such that:
		
		    P[0] = 2    Q[0] = 4
		    P[1] = 5    Q[1] = 5
		    P[2] = 0    Q[2] = 6
		the function should return the values [2, 4, 1], as explained above.
		
		Assume that:
		
		N is an integer within the range [1..100,000];
		M is an integer within the range [1..50,000];
		each element of arrays P, Q is an integer within the range [0..N − 1];
		P[K] ≤ Q[K], where 0 ≤ K < M;
		string S consists only of upper-case English letters A, C, G, T.
		Complexity:
		
		expected worst-case time complexity is O(N+M);
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
		
	 * https://rafal.io/posts/codility-genomic-range-query.html
	 */
	public static int[] GenomicRangeQuery(String S, int[] P, int[] Q){
		
//		int[] impactFactorArray = new int[S.length()];
//		int[] minimalImpactFactor = new int[P.length];
//		for(int i = 0; i < S.length(); i++){
//			if(S.charAt(i) == 'A'){
//				impactFactorArray[i] = 1;
//			}else if(S.charAt(i) == 'C'){
//				impactFactorArray[i] = 2;
//			}
//			else if(S.charAt(i) == 'G'){
//				impactFactorArray[i] = 3;
//			}
//			else{ //T = 4
//				impactFactorArray[i] = 4;
//			}
//		}
//		if(impactFactorArray.length == 1){
//			return impactFactorArray;
//		}
		
		int len = S.length();
		   int[][] arr = new int[len][4];
		   int[] result = new int[P.length];
		   
		   for(int i = 0; i < len; i++){
		     char c = S.charAt(i);
		     if(c == 'A') arr[i][0] = 1;
		     if(c == 'C') arr[i][1] = 1;
		     if(c == 'G') arr[i][2] = 1;
		     if(c == 'T') arr[i][3] = 1;
		   }
		   // compute prefixes
		   for(int i = 1; i < len; i++){
		     for(int j = 0; j < 4; j++){
		       arr[i][j] += arr[i-1][j];
		     }
		   }	
		   
		   for(int i = 0; i < P.length; i++){
		     int x = P[i];
		     int y = Q[i];
		     
		     for(int a = 0; a < 4; a++){
		       int sub = 0;
		       if(x-1 >= 0) sub = arr[x-1][a];
		       if(arr[y][a] - sub > 0){
		         result[i] = a+1;
		         break;
		       }
		     }
		     
		   }
		   return result;
	}
	
	/*******************************************************************
	 * 
	 * @param A
	 * @return
	 * 
	 * A non-empty zero-indexed array A consisting of N integers is given. A pair of integers (P, Q), such that 0 ≤ P < Q < N, is called a slice of array A (notice that the slice contains at least two elements). The average of a slice (P, Q) is the sum of A[P] + A[P + 1] + ... + A[Q] divided by the length of the slice. To be precise, the average equals (A[P] + A[P + 1] + ... + A[Q]) / (Q − P + 1).

		For example, array A such that:
		
		    A[0] = 4
		    A[1] = 2
		    A[2] = 2
		    A[3] = 5
		    A[4] = 1
		    A[5] = 5
		    A[6] = 8
		contains the following example slices:
		
		slice (1, 2), whose average is (2 + 2) / 2 = 2;
		slice (3, 4), whose average is (5 + 1) / 2 = 3;
		slice (1, 4), whose average is (2 + 2 + 5 + 1) / 4 = 2.5.
		The goal is to find the starting position of a slice whose average is minimal.
		
		Write a function:
		
		class Solution { public int solution(int[] A); }
		that, given a non-empty zero-indexed array A consisting of N integers, returns the starting position of the slice with the minimal average. If there is more than one slice with a minimal average, you should return the smallest starting position of such a slice.
		
		For example, given array A such that:
		
		    A[0] = 4
		    A[1] = 2
		    A[2] = 2
		    A[3] = 5
		    A[4] = 1
		    A[5] = 5
		    A[6] = 8
		the function should return 1, as explained above.
		
		Assume that:
		
		N is an integer within the range [2..100,000];
		each element of array A is an integer within the range [−10,000..10,000].
		Complexity:
		
		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	
	public static int minAvgTwoSlice(int[] A){
		int indexOfMinAVg = 0;
		double minAverage = Double.MAX_VALUE;
		int length = A.length;
		for(int i = 0; i < length - 1; i++){
			double avg = (A[i] + A[i+1])/2.0;
			if(avg < minAverage){
				minAverage = avg;
				indexOfMinAVg = i;
			}
			if(i < length - 2){
				double threeSliceAvg = (A[i] + A[i+1] + A[i+2])/3.0;
				if(threeSliceAvg < minAverage){
					minAverage = threeSliceAvg;
					indexOfMinAVg = i;
				}
			}
		}
		return indexOfMinAVg;
		
		
//		final int N = input.length;
//
//	      int minIndex = 0;
//	      double minAvg = Double.MAX_VALUE;
//
//	      for ( int i = 0; i < N - 1; i++ ) {
//	        double average = ( input[ i ] + input[ i + 1 ] ) / 2.0;
//
//	        if ( i < N - 2 ) {
//	          double threeSliceAvg = ( input[ i ] + input[ i + 1 ] + input[ i + 2 ] ) / 3.0;
//	          average = Math.min( average, threeSliceAvg );
//	        }
//
//	        if ( average < minAvg ) {
//	          minAvg = average;
//	          minIndex = i;
//	        }
//	      }
//
//	      return minIndex;
	}
}
