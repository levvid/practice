/*
James is a businessman. He is on a tight schedule this week. The week starts on Monday at 00:00 and ends on Sunday at 24:00. His schedule consists of M meetings he needs to take part in. Each of them will take place in a period of time, beginning and ending on the same day (there are no two ongoing meetings at the same time). James is very tired, thus he needs to find the longest possible time slot to sleep. In other words, he wants to find the longest period of time when there are no ongoing meetings. The sleeping break can begin and end on different days and should begin and end in the same week. You are given a string containing M lines. Each line is a substring representing one meeting in the schedule, in the format "Ddd hh:mm-hh: mm". "Ddd" is a three-letter abbreviation for the day of the week when the meeting takes place: "Mon" (Monday), "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" hh :mm-hh: mm" means the beginning time and the ending time of the meeting (from 00:00 to 24:00 inclusive) The given times represent exact moments of time. So, there are exactly five minutes between 13:40 and 13:45.

*/

// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {
    // function to sort hashmap by values
    public static HashMap<Integer, Integer> sortByValue(HashMap<Integer, Integer> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<Integer, Integer> > list =
               new LinkedList<Map.Entry<Integer, Integer> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer> >() {
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }


    public int timeInMinutes(String time, int day) {
        String[] hrMins = time.split(":");
        int hour = Integer.parseInt(hrMins[0]), mins = Integer.parseInt(hrMins[1]);
        return hour * 60 + mins + 24 * 60 * day ;
    }


    public int solution(String S) {
        // write your code in Java SE 8
        String[] meetings = S.split("\n");
        HashMap<Integer, Integer> startTimes = new HashMap<>();
        HashMap<Integer, Integer> endTimes = new HashMap<>();
        HashMap<String, Integer> days = new HashMap<>();

        days.put("Mon", 0);
        days.put("Tue", 1);
        days.put("Wed", 2);
        days.put("Thu", 3);
        days.put("Fri", 4);
        days.put("Sat", 5);
        days.put("Sun", 6);
        for (int i=0; i < meetings.length; i++) {
            String[] meeting = meetings[i].split(" ");
            String day = meeting[0];
            String[] times = meeting[1].split("-");

            int startTime = timeInMinutes(times[0], days.get(day)), endTime = timeInMinutes(times[1], days.get(day));

            startTimes.put(i, startTime);
            endTimes.put(i, endTime);



        }

        HashMap<Integer, Integer> sortedStartTimes = sortByValue(startTimes);
        int maxTime = 0;
        int prevKey = -1;
        int count = 0;
        // print the sorted hashmap
        for (Map.Entry<Integer, Integer> tmp : sortedStartTimes.entrySet()) {
            count += 1;
            if (prevKey == -1) {
                prevKey = tmp.getKey();
            }
            else {
                // M[i+1].start - M.end
                // curr.start - prev.end
                int currKey = tmp.getKey();

                int timeDiff = startTimes.get(currKey) - endTimes.get(prevKey);
                maxTime = Math.max(maxTime, timeDiff);

                if (count == startTimes.size()) {
                    timeDiff = 10080 - endTimes.get(currKey);
                }
                maxTime = Math.max(maxTime, timeDiff);
                prevKey = currKey;
            }

        }

        return maxTime;
    }
}
