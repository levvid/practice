import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class GroupingDishes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[][] dishes = {{"Salad", "Tomato", "Cucumber", "Salad", "Sauce"},
			{"Pizza", "Tomato", "Sausage", "Sauce", "Dough"},
			{"Quesadilla", "Chicken", "Cheese", "Sauce"},
			{"Sandwich", "Salad", "Bread", "Tomato", "Cheese"}};
		groupingDishes(dishes);

	}
	
	static String[][] groupingDishes(String[][] dishes) {
	    Map<String, ArrayList<String>> ingredients = new HashMap<String,  ArrayList<String>>();
	    
	    for(int i = 0; i < dishes.length; i++){
	        for(int j = 0; j < dishes[i].length; j++){
	            if(j>0){
	                if(!ingredients.containsKey(dishes[i][j])){
	                    ingredients.put(dishes[i][j], new ArrayList<String>(Arrays.asList(dishes[i][0])));
	                }
	                else{
	                    addToList(ingredients, dishes[i][j], dishes[i][0]);
	                }
	            }
	        }
	    }
	    
	    
	    
	    return groupDishes(ingredients);
	    
	}


	private static String[][] groupDishes(Map<String, ArrayList<String>> ingredients){
	    SortedSet<String> keys = new TreeSet<String>(ingredients.keySet());
	    List<String[]> tmp = new ArrayList<String[]>();
	    for (String key : keys) { 
	        List<String> dishes = ingredients.get(key);
	        Collections.sort(dishes);
	        if(dishes.size() > 1){
	            System.out.println(key+ ": " + dishes);
	            dishes.add(0, key);
	            tmp.add(dishes.toArray(new String[dishes.size()]));
	        }
	    }
	    String[][] returnValue = new String[tmp.size()][];
	    for(int i = 0; i < tmp.size(); i++){
	    	returnValue[i] = new String[tmp.get(i).length];
	    	for(int j = 0; j < tmp.get(i).length; j++){
	    		returnValue[i][j] = tmp.get(i)[j];
	    	}
	    }
	    return returnValue;
	}


	private static synchronized void addToList(Map<String, ArrayList<String>> ingredients, String mapKey, String newDish) {
	    ArrayList<String> dishes = ingredients.get(mapKey);

	    // if list does not exist create it
	    if(dishes == null) {
	         dishes = new ArrayList<String>();
	         dishes.add(newDish);
	         ingredients.put(mapKey, dishes);
	    } else {
	        // add if item is not already in list
	        if(!dishes.contains(newDish)) dishes.add(newDish);
	    }
	}


}
