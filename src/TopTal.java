
public class TopTal {
	class Tree {
		public int x;
		public Tree l;
		public Tree r;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//return findLongestZigZag(T, 0) - 1;
		
		
	}

	/****
	 * ****
	 * https://gist.github.com/gandarez/67ce18820ed895638704c4163d9cb2e1*
	 * http://codingrecipies.blogspot.com/2013/12/longest-zig-zag-path.html**/
	private int findLongestZigZag(Tree T, int max){

		if (T == null){
			return 0;
		}

		int left = countHelper(T, true, 0);
		int right = countHelper(T, false, 0);

		max = Math.max(left, right);
		max = Math.max(max, findLongestZigZag(T.l, max));
		max = Math.max(max, findLongestZigZag(T.r, max));

		return max;
	}

	private int countHelper(Tree T, boolean moveLeft , int count){
		if(T==null)
			return count;

		if(moveLeft)
			count = countHelper(T.l,!moveLeft, T.l == null? count: count+1);
		else
			count = countHelper(T.r,!moveLeft,T.r == null? count: count+1);

		return count;
	}
	
	
	
	
	 public int shortestWinter(int[] T) {
	     // write your code in Java SE 8
		 int prevIndex = 0, lengthOfWinter= 1, maxWinterTemp = T[0];
		 
		 
		 for(int i = 1; i < T.length; i++){
			 if(T[i] <= maxWinterTemp){
				 lengthOfWinter = lengthOfWinter + i - prevIndex;
				 prevIndex = i;
			 }
		 }
		 
		 return lengthOfWinter;
	 }

}
