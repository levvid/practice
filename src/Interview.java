import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Interview {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String S = "00:01:07,400-234-090\n"
				+ "00:05:01,701-080-080\n"
				  + "00:05:00,400-234-090";
		//moneyPaid(S);
		List l = new ArrayList();
		l.add("1");
		l.add("2");
		l.add(1,"3");
		System.out.println(l);
		char a = 064770, b = '\uface';
		
		
		
	}
	
	public static int moneyPaid(String S) {
        // write your code in Java SE 8
        HashMap<String, Integer> map = new HashMap<String, Integer>(); //stores total time for each phonenumber to get one with longest total duration
        HashMap<String, List<Integer>> callDetailsMap = new HashMap<String, List<Integer>>(); //stores call durations for each call
        BufferedReader bufferedReader = new BufferedReader(new StringReader(S));
        int moneyPaid = 0;
        String phoneNumberWithMaxDuration = "";
        String line = null;
        try {
			while((line = bufferedReader.readLine()) != null ){
				String[] timeAndNumber = line.split(",");
				String[] tokens = timeAndNumber[0].split(":");
				int hours = Integer.parseInt(tokens[0]);
				int minutes = Integer.parseInt(tokens[1]);
				int seconds = Integer.parseInt(tokens[2]);
				String phoneNumber = timeAndNumber[1];
				int duration = 3600 * hours + 60 * minutes + seconds;
				
				moneyPaid += getMoneyPaid(duration);
				
				int currentDuration = map.containsKey(phoneNumber) ? map.get(phoneNumber) : 0;
				map.put(phoneNumber, currentDuration + duration); 
				
				if (callDetailsMap.get(phoneNumber) == null) {
				    callDetailsMap.put(phoneNumber, new ArrayList<Integer>());
				}
				callDetailsMap.get(phoneNumber).add(duration);
				
				
				//System.out.println("Hours: " + hours + " Mins: " + minutes + " Secs: " + seconds + " Phone: " + phoneNumber);
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        int maxValueInMap = (Collections.max(map.values()));  //max value in the Hashmap
        for (Entry<String, Integer> entry : map.entrySet()) {  // find phone number with max time
            if (entry.getValue() == maxValueInMap) {
                //key with max value
            	phoneNumberWithMaxDuration = entry.getKey();
            	System.out.println("Ph: " + phoneNumberWithMaxDuration + " max V: " + maxValueInMap);
            }
        }
        
        //retrieve details of phone number with max duration and subtract from the money paid
        
        List<Integer> values = callDetailsMap.get(phoneNumberWithMaxDuration);
        for(int i = 0; i < values.size(); i++){
        	moneyPaid -= getMoneyPaid(values.get(i));
        }
        
        return moneyPaid;
    }
	
	
	private static int getMoneyPaid(int duration){
		int moneyPaid = 0;
		if(duration < (5 * 60)){//call less than 5 minutes
			moneyPaid = 3 * duration;
		}
		else if(duration >= (5 * 60)){ //call at least 5 minutes long
			//convert duration to minutes, if there are more than 0 seconds add 1 to the number of minutes
			duration = duration % 60 == 0 ? duration/60:duration/60+1; 
			moneyPaid =  duration * 150;
		}
		return moneyPaid;
	}
}
