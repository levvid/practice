import java.io.*;
import java.util.*;

public class BalancedDelimiters {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Stack<Character> stack = new Stack<>();
        Scanner sc = new Scanner(System.in);
        int j = 0;
        

        String s = sc.nextLine();
        for(int i=0; i< s.length(); i++){
        	if(s.charAt(i) == '{' || s.charAt(i) == '(' || s.charAt(i) == '['){
        		stack.push(s.charAt(i));
            }else if(!stack.isEmpty()){
            	char c = stack.pop();
                if((s.charAt(i) == '}' && c != '{' || s.charAt(i) == ')' && c != '(' 
                    || s.charAt(i) == ']' && c != '[' )){
                	j++;
                	break ;
                }
            }else if(stack.isEmpty()){
                j++;
                break;
            }
        }
       
        if(!stack.isEmpty() || j>0){
        	System.out.println("False");
        }
        else{
        	System.out.println("True");
        }
    }
}