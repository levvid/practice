import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Sorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	/******************************************
	 * 
	 * @param A
	 * @param N
	 * @return
	 * 
	 * Write a function

		int solution(int A[], int N);
		that, given a zero-indexed array A consisting of N integers, returns the number of distinct values in array A.
		
		Assume that:
		
		N is an integer within the range [0..100,000];
		each element of array A is an integer within the range [−1,000,000..1,000,000].
		For example, given array A consisting of six elements such that:
		
		 A[0] = 2    A[1] = 1    A[2] = 1
		 A[3] = 2    A[4] = 3    A[5] = 1
		the function should return 3, because there are 3 distinct values appearing in array A, namely 1, 2 and 3.
		
		Complexity:
		
		expected worst-case time complexity is O(N*log(N));
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	private static int distinct(int[] A, int N){
		Set<Integer> set = new TreeSet<Integer>();
		for(int i = 0; i < N; i++){
			set.add(A[i]);
		}
		
		return set.size();
	}
	
	
	/**************************************************
	 * 
	 * @param A
	 * @return
	 * 
	 * 
	 * A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:

		A[P] + A[Q] > A[R],
		A[Q] + A[R] > A[P],
		A[R] + A[P] > A[Q].
		For example, consider array A such that:
		
		  A[0] = 10    A[1] = 2    A[2] = 5
		  A[3] = 1     A[4] = 8    A[5] = 20
		Triplet (0, 2, 4) is triangular.
		
		Write a function:
		
		class Solution { public int solution(int[] A); }
		that, given a zero-indexed array A consisting of N integers, returns 1 if there exists a triangular triplet for this array and returns 0 otherwise.
		
		For example, given array A such that:
		
		  A[0] = 10    A[1] = 2    A[2] = 5
		  A[3] = 1     A[4] = 8    A[5] = 20
		the function should return 1, as explained above. Given array A such that:
		
		  A[0] = 10    A[1] = 50    A[2] = 5
		  A[3] = 1
		the function should return 0.
		
		Assume that:
		
		N is an integer within the range [0..100,000];
		each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
		Complexity:
		
		expected worst-case time complexity is O(N*log(N));
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
		
		*
		*
		*
		*Solution
		*
		*Update: we need two parts to prove our solution.

		On one hand, there is no false triangular. 
		Since the array is sorted, we already know A[index] < = A[index+1] <= A[index+2], and all values are positive. 
		A[index] <= A[index+2], so it must be true that A[index] < A[index+1] + A[index+2]. Similarly, A[index+1] < A[index] + A[index+2]. 
		Finally, we ONLY need to check A[index]+A[index+1] > A[index+2] to confirm the existence of triangular.
	 */
	private static int solution(int[] A){
		if(A== null || A.length < 3)
			return 0;
		Arrays.sort(A);
		
		for(int i = 0; i < A.length - 2; i++){
			//To prevent overflow, instead of:
			//if(A[i] + A[i+1] > A[i+2]) 
			//we use
			if (A[i] >= 0 && A[i] > A[i+2] - A[i+1])
				return 1;
			
			/*
             * We already know A[i+1] <= A[i+2]. If A[i] < 0,
             * A[i] + A[i+1] < A[i+2]
             */
		}
		return 0;
	}
	
	/********************************************************
	 * 
	 * @param A
	 * @return
	 * 
	 * A non-empty zero-indexed array A consisting of N integers is given. The product of triplet (P, Q, R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).

		For example, array A such that:
		
		  A[0] = -3
		  A[1] = 1
		  A[2] = 2
		  A[3] = -2
		  A[4] = 5
		  A[5] = 6
		contains the following example triplets:
		
		(0, 1, 2), product is −3 * 1 * 2 = −6
		(1, 2, 4), product is 1 * 2 * 5 = 10
		(2, 4, 5), product is 2 * 5 * 6 = 60
		Your goal is to find the maximal product of any triplet.
		
		Write a function:
		
		class Solution { public int solution(int[] A); }
		
		that, given a non-empty zero-indexed array A, returns the value of the maximal product of any triplet.
		
		For example, given array A such that:
		
		  A[0] = -3
		  A[1] = 1
		  A[2] = 2
		  A[3] = -2
		  A[4] = 5
		  A[5] = 6
		the function should return 60, as the product of triplet (2, 4, 5) is maximal.
		
		Assume that:
		
		N is an integer within the range [3..100,000];
		each element of array A is an integer within the range [−1,000..1,000].
		Complexity:
		
		expected worst-case time complexity is O(N*log(N));
		expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	private static int maxProductOfThree(int[] A){
		Arrays.sort(A);
		int N = A.length;
		// Return the maximum of product of last three
	    // elements and product of first two elements
	    // and last element
		return Math.max(A[N-1] * A[N-2] * A[N-3], A[0] * A[1] * A[N-1]);
	}
	
	/*******************************************************************************************
	 * 
	 * @param A
	 * @return 
	 * 
	 * We draw N discs on a plane. The discs are numbered from 0 to N − 1. A zero-indexed array A of N non-negative integers, specifying the radiuses of the discs, is given. The J-th disc is drawn with its center at (J, 0) and radius A[J].

		We say that the J-th disc and K-th disc intersect if J ≠ K and the J-th and K-th discs have at least one common point (assuming that the discs contain their borders).
		
		The figure below shows discs drawn for N = 6 and A as follows:
		
		  A[0] = 1
		  A[1] = 5
		  A[2] = 2
		  A[3] = 1
		  A[4] = 4
		  A[5] = 0
		
		There are eleven (unordered) pairs of discs that intersect, namely:
		
		discs 1 and 4 intersect, and both intersect with all the other discs;
		disc 2 also intersects with discs 0 and 3.
		Write a function:
		
		class Solution { public int solution(int[] A); }
		that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs. The function should return −1 if the number of intersecting pairs exceeds 10,000,000.
		
		Given array A shown above, the function should return 11, as explained above.
		
		Assume that:
		
		N is an integer within the range [0..100,000];
		each element of array A is an integer within the range [0..2,147,483,647].
		Complexity:
		
		expected worst-case time complexity is O(N*log(N));
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
		
	*******************************************************************************************
	*Borrowed from: 
	*https://rafal.io/posts/codility-intersecting-discs.html
	*
	 */
	private static int numberOfDiscIntersections(int[] A){
		long cnt = 0;
		
		int l = A.length;
		long[] A1 = new long[l];
		long[] A2 = new long[l];
		
		for(int i = 0; i < l; i++){
			A1[i] = (long)A[i] + i;
			A2[i] = -((long)A[i] - i);
		}
		
		Arrays.sort(A1);
		Arrays.sort(A2);
		for(int i = A.length - 1; i >= 0; i--){
			int pos = Arrays.binarySearch(A2, A1[i]);
			if(pos >= 0){
				while(pos < A.length && A2[pos] == A1[i]){
					pos++;
				}
				cnt += pos;
			} else{ // element not there
				int insertionPoint = -(pos+1);
				cnt += insertionPoint;
			}

		}

		long sub = (long)l*((long)l+1)/2;
		cnt = cnt - sub;

		if(cnt > 1e7) return -1;
		
		return (int)cnt;
	}
}
