
public class RotateArray {
	
	
	private int[][] rotateImage(int[][] a) {
	    final int M = a.length;
	    final int N = a[0].length;
	    int[][] ret = new int[N][M];
	    for (int r = 0; r < M; r++) {
	        for (int c = 0; c < N; c++) {
	            ret[c][M-1-r] = a[r][c];
	        }
	    }
	    return ret;
	}	

}
