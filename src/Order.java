import java.util.Comparator;


public class Order implements Comparable<Order>{
    private int orderNumber;
    private String type;
    private String side;
    private int value1;
    private double value2;
    
    public Order(int orderNumber, String type, String side, int value1, double value2){
        super();
        this.orderNumber = orderNumber;
        this.type = type;
        this.side = side;
        this.value1 = value1;
        this.value2 = value2;
    }
    
    
    public int getOrderNumber() {
        return orderNumber;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getSide(){
        return side;
    }
    
    public void setSide(String side) {
        this.side = side;
    }
    
    public int getValue1(){
        return value1;
    }
    
    public void setValue1(int value1) {
        this.value1 = value1;
    }
    
    public double getValue2() {
        return value2;
    }
    
    public void setValue2(double value2) {
        this.value2 = value2;
    }
    
    public static class Comparators {
        public static final Comparator<Order> ORDER_NUMBER = (Order o1, Order o2) -> Integer.compare(o1.getOrderNumber(), o2.getOrderNumber()); //ascending
        public static final Comparator<Order> PRICE = (Order o1, Order o2) -> Double.compare(o1.getValue2(), o2.getValue2()); //ascending order
    }

	@Override
	public int compareTo(Order arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}