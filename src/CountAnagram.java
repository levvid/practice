import java.util.Arrays;
import java.util.Scanner;

public class CountAnagram {

	public static void main(String[] args) {Scanner scanner =new Scanner(System.in);  
	int n = Integer.parseInt(scanner.nextLine()); //get the number of test cases
	for(int i = 0; i < n; i++){ //loop through test cases
		String input = scanner.nextLine();
		if(input.length() % 2 != 0){ //if length is not even, then cannot be anagram
			System.out.println("-1");
		}
		else{
			String word1 = input.substring(0, input.length()/2);
			String word2 = input.substring(input.length()/2);
			char[] word1array = word1.toCharArray();
			
			int count = 0;
			for(int j = 0; j < word1array.length; j++){
				StringBuilder stringBuilder = new StringBuilder(word2);
				int index = word2.indexOf(word1array[j]);
				if(index != -1){ //character is in word2
					stringBuilder.deleteCharAt(index); //replace the character in word2 to avoid repetition
					word2 = stringBuilder.toString();
				}else{
					count++; //if char not present in word2 then increment count
				}
			}
			System.out.println(count);
		}
	}

	}

}
