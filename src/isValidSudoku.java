import java.util.Arrays;

public class isValidSudoku {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char[][] grid =  {{'.', '.', '.', '1', '4', '.', '.', '2', '.'},
		                    {'.', '.', '6', '.', '.', '.', '.', '.', '.'},
		                    {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
		                    {'.', '.', '1', '.', '.', '.', '.', '.', '.'},
		                    {'.', '6', '7', '.', '.', '.', '.', '.', '9'},
		                    {'.', '.', '.', '.', '.', '.', '8', '1', '.'},
		                    {'.', '3', '.', '.', '.', '.', '.', '.', '6'},
		                    {'.', '.', '.', '.', '.', '7', '.', '.', '.'},
		                    {'.', '.', '.', '5', '.', '.', '.', '7', '.'}};
		
		System.out.println(sudoku2(grid));
	}
	
	static boolean sudoku2(char[][] grid) {
	    //check all the rows
		//check all columns
		for(int i = 0; i < 9; i++){
			boolean[] tmp2 = new boolean[9];
            boolean[] tmp = new boolean[9];
			for(int j = 0; j < 9; j++){
                             
				char c = grid[i][j];
				char c2 = grid[j][i];
				//System.out.println("i: " + i + " j: " + j + " c: " + c);
				//rows
				int index = (int)c - '1', index2 = (int)c2 - '1';
				if(c != '.'){
                    if(tmp[index]){
                        return false;
                    }
                    else{
                        tmp[index] = true;
                    }
                }
                if(c2 != '.'){
                    if(tmp2[index2]){
                        return false;
                    }
                    else{
                        tmp2[index2] = true;
                    }
                }
			}
		}
		
		
		
		//check all 3x3 matrices
		for(int x = 0; x < 9; x += 3){
			for(int y = 0; y < 9; y += 3){
				boolean[] tmp = new boolean[9];
				for(int i = x; i < x + 3; i++){
                    
					for(int j = y; j < y + 3; j ++){
                       
						char c = grid[i][j];
						if(c != '.' && tmp[(int)c - '1']){
                            System.out.println("h2222");
							return false;
						}else if(c != '.'){
							tmp[(int)c - '1'] = true;
						}
					}
				}
			}
		}
		
		return true;
	}


}
