/**http://www.geeksforgeeks.org/sum-numbers-formed-root-leaf-paths/**/
public class TreePathsSum {
	// Definition for binary tree:
		 class Tree<T> {
		   Tree(T x) {
		     value = x;
		   }
		   T value;
		   Tree<T> left;
		   Tree<T> right;
		 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	//
	
	long digitTreeSum(Tree<Integer> t) {
	    return calculateSum(t, 0);
	}
	
	
	/**Returns sum of all root to leaf paths. The first parameter is 
	 *root of current subtree, the second parameter is value of the
	 *number formed by nodes from root to this node**/
	long calculateSum(Tree<Integer> node, long workingSum){
	    if(node == null) return 0;
	    workingSum = workingSum * 10 + node.value;
	    
	    if(node.left == null && node.right == null) return workingSum;
	    
	    return calculateSum(node.left, workingSum) + calculateSum(node.right, workingSum);
	}
	
	
	/**With BigInteger**/
	
//	long digitTreeSum(Tree<Integer> t) {
//	    return calculateSum(t, BigInteger.ZERO);
//	}
//
//	long calculateSum(Tree<Integer> node, BigInteger workingSum){
//	    if(node == null) return workingSum.longValue();
//	    workingSum = workingSum.multiply(BigInteger.valueOf((long)10)).add(BigInteger.valueOf((long)node.value));
//	    
//	    if(node.left == null && node.right == null) return workingSum.longValue();
//	    
//	    return calculateSum(node.left, workingSum) + calculateSum(node.right, workingSum);
//	}
	
	/**Short solution with strings**/
//	long r;
//
//	long digitTreeSum(Tree<Integer> t) {
//	    if (t != null) f("", t);
//	    return r;
//	}
//
//	void f(String s, Tree<Integer> t) {
//	    s += t.value;
//	    if (t.left == null && t.right == null) r += new Long(s);
//	    if (t.left != null) f(s, t.left);
//	    if (t.right != null) f(s, t.right);
//	}


}
