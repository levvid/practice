import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Solution {
	public static void main(String args[] ) throws Exception {
		/* Enter your code here. Read input from STDIN. Print output to STDOUT */
		 Scanner scanner = new Scanner(new BufferedInputStream(System.in));
		 	int orderNumber = 1;
		 	Order newOrder = null;
		 	ArrayList<Order> orders = new ArrayList<Order>();
	        while (scanner.hasNext()) {
	            String line = scanner.nextLine();
	            String[] orderDetails = line.split(" ");
	            if(orderDetails.length == 4){
		            newOrder = new Order(orderNumber, orderDetails[0], orderDetails[1], Integer.parseInt(orderDetails[2]), Double.parseDouble(orderDetails[3]) );
		            
		            orders.add(newOrder);
		            if(newOrder != null && newOrder.getSide().equals("sell")){
		            	orders = new ArrayList<Order>(sell(orders, newOrder));
		            }else{         	
		            	orders = new ArrayList<Order>(buy(orders, newOrder));
		            }    
		            
		            orderNumber++;
	            }
	        }
	        //scanner.close();
	}



	/*prints a match*/
	public static void printMatch(Order taker, Order maker, int volume, double price){
		System.out.println("match " + taker.getOrderNumber() + " " + maker.getOrderNumber() + " " + volume + " " + price);
		
	}

	public static ArrayList<Order> performBuy(Order taker, Order maker, double price){
		int volume = 0;
		ArrayList<Order> takerMaker = new ArrayList<Order>();
		if(taker.getValue1() > maker.getValue1()){ //buying more than they are selling
			volume = taker.getValue1() - maker.getValue1();
			taker.setValue1(taker.getValue1() - volume);
			maker.setValue1(0);
		}
		else if(taker.getValue1() < maker.getValue1()){ //buying less than they are selling
			volume = taker.getValue1();
			maker.setValue1(maker.getValue1() - volume);
			taker.setValue1(0);
			
		}else{ //buying same number
			volume = maker.getValue1();
			maker.setValue1(0);
			taker.setValue1(0); 			
		}
		
		takerMaker.add(taker);
		takerMaker.add(maker);
		
		printMatch(taker, maker, volume, price);
		
		return takerMaker;
	}


	/*processes a buy*/
	public static ArrayList<Order> buy(ArrayList<Order> orders, Order order) {
		
		ArrayList<Order> ordersTmp = new ArrayList<Order>();
		ordersTmp = orders;
		ArrayList<Order> ordersResult = new ArrayList<Order>();
		ordersResult = orders;
		Order orderTmp = order;
		if(order.getType().equals("market")){ //market orders			
			ordersResult = transactionHelper("market", "sell", ordersResult, ordersTmp, orderTmp, Order.Comparators.PRICE, 2);
		}
		else if(order.getType().equals( "limit")){
			ordersResult = transactionHelper("limit", "sell", ordersResult, ordersTmp, orderTmp, Order.Comparators.PRICE, 0);
		}
		else if(order.getType() == "stop"){
			ordersResult = transactionHelper("stop", "buy", ordersResult, ordersTmp, orderTmp, Order.Comparators.ORDER_NUMBER, 1);
		}
		
		return ordersResult;
	
	}
	/*processes a sell*/
	public static ArrayList<Order> sell(ArrayList<Order> orders, Order order){
		ArrayList<Order> ordersTmp = new ArrayList<Order>();
		ordersTmp = orders;
		ArrayList<Order> ordersResult = new ArrayList<Order>();
		ordersResult = orders;
		Order orderTmp = order;
		if(order.getType().equals("market")) { //market orders
			ordersResult = transactionHelper("market", "buy", ordersResult, ordersTmp, orderTmp, Order.Comparators.PRICE, 2);
		}
		else if(order.getType().equals("limit")){
			ordersResult = transactionHelper("limit", "buy", ordersResult, ordersTmp, orderTmp, Order.Comparators.PRICE, 1);
		}
		else if(order.getType().equals("stop")) {
			ordersResult = transactionHelper("stop", "sell", ordersResult, ordersTmp, orderTmp, Order.Comparators.ORDER_NUMBER, 0);
		}
		else if(orderTmp.getType().equals("cancel")) {
			//ordersResult = new ArrayList<Order>(cancel(ordersResult, orderTmp));
		}

		return ordersResult;

	
	}
	
	public static ArrayList<Order> cancel(ArrayList<Order> ordersResult, Order orderTmp){
		int orderNumber = orderTmp.getValue1();
		if((ordersResult.get(orderNumber)).getValue1() > 0){
			Order tmp = ordersResult.get(orderNumber);
			printMatch(orderTmp, tmp, tmp.getValue1(), tmp.getValue2());
			tmp.setValue1(0);
			ordersResult.set(orderNumber, tmp);
		}
		return ordersResult;
	}
	
	public static ArrayList<Order> transactionHelper(String type, String side, ArrayList<Order> ordersResult, ArrayList<Order> ordersTmp, Order orderTmp, Comparator<Order> comparator, int conditionNumber) {
		int index = 0;
		
		
		ArrayList<Order> marketSellOrders = (ArrayList<Order>) ordersTmp.stream()
				.filter(o -> o.getSide().equals(side)).collect(Collectors.toList());
		marketSellOrders = (ArrayList<Order>) marketSellOrders.stream()
				.filter(o -> o.getType().equals("limit")).collect(Collectors.toList());
		
		ArrayList<Order> marketSellOrders2 = (ArrayList<Order>) ordersTmp.stream()
				.filter(o -> o.getType().equals("market")).collect(Collectors.toList());
		marketSellOrders2 = (ArrayList<Order>) marketSellOrders2.stream()
				.filter(o -> o.getSide().equals(side)).collect(Collectors.toList());
		marketSellOrders.addAll(marketSellOrders2);
		
		Collections.sort(marketSellOrders, comparator); //ascending order price
		if (marketSellOrders != null && !marketSellOrders.isEmpty()) {
			int i = 0;
			boolean[] conditions = new boolean[3];
			conditions[0] = ((Order)(marketSellOrders.get(i))).getValue2() <= orderTmp.getValue2(); //at or below limit
			conditions[1] = ((Order)(marketSellOrders.get(i))).getValue2() >= orderTmp.getValue2(); //at or above limit
			conditions[2] = true;

			while(i < marketSellOrders.size() && orderTmp.getValue1() > 0  && conditions[conditionNumber]){//start with smallest price
				
				Order opposingOrder = ((Order)(marketSellOrders.get(i)));
				if(opposingOrder.getValue1() > 0 && ((Order)(marketSellOrders.get(i))).getOrderNumber() != orderTmp.getOrderNumber()){ //not same order
			    	ArrayList<Order> takerMaker = new ArrayList<Order>();
					takerMaker = performBuy(orderTmp, opposingOrder, opposingOrder.getValue2());
					ordersResult.set(index, (Order) takerMaker.get(1));
					ordersResult.set(ordersResult.indexOf(orderTmp), (Order) takerMaker.get(0));
					orderTmp = (Order) takerMaker.get(0);
					ArrayList<Order> cancelOrders = new ArrayList<Order>();
					cancelOrders = (ArrayList<Order>) ordersTmp.stream()
					.filter(o -> o.getType().equals("cancel")).collect(Collectors.toList());
					cancelOrders = (ArrayList<Order>) cancelOrders.stream()
					.filter(o -> o.getSide().equals("none")).collect(Collectors.toList());
					for(int k = 0; k < cancelOrders.size(); k++){
						if(((Order)takerMaker.get(0)).getOrderNumber() == cancelOrders.get(k).getValue1() && ((Order)(takerMaker.get(0))).getValue1() > 0) {
							cancel(ordersResult, orderTmp);
							cancelOrders.get(k).setSide("done");
							ordersResult.set(cancelOrders.get(k).getOrderNumber(), cancelOrders.get(k));
						}
					}
							
							
				}
					
				i++;
			}
		}else{
			return ordersResult;
		}
			
		return ordersTmp;
	}
	

	
}