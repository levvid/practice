import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Test {
	
	public ArrayList<Order> sell2(ArrayList<Order> orders, Order order) {
		ArrayList<Order> ordersTmp = new ArrayList<Order>();
		ordersTmp = orders;
		ArrayList<Order> ordersResult = new ArrayList<Order>();
		ordersResult = orders;
		Order orderTmp = order;
		
		if(order.getType() == "market") { //market orders
			int index = 0;
//			ArrayList<Order> marketSellOrders = ordersTmp.stream()
//					.filter( (o -> o.getSide() == "buy" ) && (o -> o.getType() == "market" || o -> o.getType() == "limit")).collect(Collectors.toList());
			ArrayList<Order> marketSellOrders = new ArrayList<Order>();
			Collections.sort(marketSellOrders, Order.Comparators.PRICE);
			if (marketSellOrders != null && !marketSellOrders.isEmpty()) {
				int i = 0;
				while(i < marketSellOrders.size() && orderTmp.getValue1() > 0){
					index = ((Order)(marketSellOrders.get(i))).getOrderNumber();
					Order opposingOrder = ordersResult.get(index);
					ArrayList<Order> takerMaker = new ArrayList<Order>();
					//takerMaker = performBuy(orderTmp, opposingOrder, opposingOrder.getValue2());
					ordersResult.set(index, takerMaker.get(1));
					ordersResult.set(ordersResult.indexOf(orderTmp), takerMaker.get(0));
					i++;
					orderTmp = takerMaker.get(0);
				}


			}else{
				return ordersResult;
			}
		}
		else if(order.getType() == "limit"){
			int index = 0;
			ArrayList<Order> marketSellOrders = (ArrayList<Order>) ordersTmp.stream()
					.filter(o -> o.getSide() == "sell").collect(Collectors.toList());
			marketSellOrders = (ArrayList<Order>) ordersTmp.stream()
					.filter(o -> o.getSide() == "limit").collect(Collectors.toList());
			ArrayList<Order> marketSellOrders2 = (ArrayList<Order>) ordersTmp.stream()
					.filter(o -> o.getSide() == "market").collect(Collectors.toList());
			marketSellOrders.addAll(marketSellOrders2);
			Collections.sort(marketSellOrders, Order.Comparators.PRICE);
			if (marketSellOrders != null && !marketSellOrders.isEmpty()) {
				int i = 0;
				while(i < marketSellOrders.size() && orderTmp.getValue1() > 0 && ((Order)(marketSellOrders.get(i))).getValue2() <= orderTmp.getValue2()) {
					index = ((Order)(marketSellOrders.get(i))).getOrderNumber();
					Order opposingOrder = ordersResult.get(index);
					ArrayList takerMaker = new ArrayList<Order>();
					takerMaker = performBuy(orderTmp, opposingOrder, opposingOrder.getValue2());
					ordersResult.set(index, ((Order) takerMaker.get(1)));
					ordersResult.set(ordersResult.indexOf(orderTmp), (Order) takerMaker.get(0));
					i++;
					orderTmp = (Order) takerMaker.get(0);
				}


			}else{
				return ordersResult;
			}
		}
		
		return ordersResult;
		
	}
	
	
	/*prints a match*/
	public void printMatch(Order taker, Order maker, int volume, double price){
		System.out.println("match " + taker.getOrderNumber() + " " + maker.getOrderNumber() + " " + volume + " " + price);
	}

	public ArrayList<Order> performBuy(Order taker, Order maker, double price){
		int volume = 0;
		if(taker.getValue1() > maker.getValue1()){
			volume = taker.getValue1() - maker.getValue1();
			taker.setValue1(volume);
			maker.setValue1(0);
		}
		else if(taker.getValue1() < maker.getValue1()){
			volume = maker.getValue1() - taker.getValue1();
			maker.setValue1(volume);
			taker.setValue1(0);
		}else{ //are equal
			volume = maker.getValue1();
			maker.setValue1(0);
			taker.setValue1(0); 
		}
		ArrayList<Order> takerMaker = new ArrayList<Order>();
		takerMaker.add(taker);
		takerMaker.add(maker);
		printMatch(taker, maker, volume, price);
		return takerMaker;
	}

}
