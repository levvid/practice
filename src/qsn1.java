
class Solution {
    public String solution(String S, String C) {
        // write your code in Java SE 8
        String ret = "";
        String[] names = S.split(";");
        HashMap<String, Integer> emailAddresses = new HashMap<String, Integer>();
        C = C.toLowerCase();

        for (int i=0; i < names.length; i++) {
            String name = names[i];
            name = name.trim().toLowerCase();
            String[] splitNames = name.split(" ");

            String firstName = "", lastName = "";
            String emailAddress = "";

            if (splitNames.length == 2) {
                firstName = splitNames[0];
                lastName = splitNames[1].replace("-", "");
            }
            else { // has middle name
                firstName = splitNames[0];
                lastName = splitNames[2].replace("-", "");
            }

            if (lastName.length() > 8) {
                lastName = lastName.substring(0, 8);
            }

            emailAddress = firstName + "." +  lastName + "@" + C + ".com";

            if (emailAddresses.containsKey(emailAddress)) {
                int num_emails = emailAddresses.get(emailAddress);
                num_emails += 1;
                emailAddresses.put(emailAddress, num_emails);
                emailAddress = firstName + "." +  lastName + num_emails + "@" + C + ".com";
            }
            else {
                emailAddresses.put(emailAddress, 1);
            }

            if (i == names.length - 1){
                ret += emailAddress;
            }
            else {
                ret += emailAddress + "; ";
            }
        }

        return ret;

    }

    public static void main(String... args) {
        String S = "John Doe; Peter Benjamin Parker; Mary Jane Watson-Parker; John Elvis Doe; John Evan Doe; Jane Doe; Peter Brian Parker";
        String C = "Example";
        Solution test = new Solution();
        test.solution(S, C);

    }
}
