/*package whatever //do not write package name here

Given the first 2 terms A and B of an Arithmetic Series, tell the Nth term of the series.

Input:
First line contains an integer, the number of test cases 'T'. T testcases follow. Each test case in its first line should contain two positive integer A and B(First 2 terms of AP). In the second line of every test case it contains of an integer N.

Output:
For each testcase, in a new line, print the Nth term of the Arithmetic Progression.

Constraints:
1 <= T <= 30
-100 <= A <= 100
-100 <= B <= 100
1 <= N <= 100

Example:
Input:
2
2 3
4
1 2
10
Output:
5
10


*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	public static void main (String[] args) {
		//code
		int a, b, n, nth, numtests, d;
		Scanner input = new Scanner(System.in);
        numtests = input.nextInt();

        for(int i=0; i<numtests; i++){
            a = input.nextInt();
            b = input.nextInt();
            n = input.nextInt();

            d = b - a;
            nth = a + (n - 1)*d;
            // System.out.println("a: " + a + " b: "+ b+" n: "+n+" nth: "+nth);
            System.out.println(nth);
        }
	}
}
