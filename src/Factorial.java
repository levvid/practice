import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class Factorial {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    	Scanner sc = new Scanner(System.in);
    	BigInteger n = BigInteger.valueOf(Long.parseLong(sc.next()));
    	System.out.println(factorial(n));
    }
    
    
    /***Imperative approach*****/
    public static BigInteger factorial(BigInteger n) {
        BigInteger result = BigInteger.ONE;

        while (!n.equals(BigInteger.ZERO)) {
            result = result.multiply(n);
            n = n.subtract(BigInteger.ONE);
        }

        return result;
    }
    
    
    /**Recursive approach***/
    public static BigInteger fac(BigInteger n){
    	if(n.equals(BigInteger.ZERO)){
    		return BigInteger.ONE;
    	}
    	return n.multiply(factorial(n.subtract(BigInteger.ONE)));
    }
}