import java.io.*;
import java.util.*;
import java.math.*;

public class FibonacciWithBigInteger {
    private static Map<Integer, BigInteger> memo = new HashMap<>();
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sCurrentLine = null;
        
        try { 
            while ((sCurrentLine = reader.readLine()) != null) {
                int n = Integer.parseInt(sCurrentLine);
                System.out.println(fibonacci(n));
                
            } 
        }catch(IOException e){
              e.printStackTrace();
        }
        
    }
    
    public static BigInteger fibonacci(int n){
        
        if (n == 0 || n == 1) {
            return BigInteger.valueOf(n);
        }
        if (memo.containsKey(n)) {
            return memo.get(n);
        }
        BigInteger v = fibonacci(n - 2).add(fibonacci(n - 1));
        memo.put(n, v);
        return v;
        
        
    }
}