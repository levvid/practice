import java.math.BigInteger;

public class AddTwoHugeNumbersLinkedList {
	// Definition for singly-linked list:
		 class ListNode<T> {
		   ListNode(T x) {
		     value = x;
		   }
		   T value;
		   ListNode<T> next;
		 }

	public static void main(String[] args) {
		// TODO Auto-generated method stub


	}


	ListNode<Integer> addTwoHugeNumbers(ListNode<Integer> a, ListNode<Integer> b) {
	    String strA = "", strB = "";

	    while(a != null){
	        String tmp = String.valueOf(a.value);
	        while(tmp.length() < 4){
	            tmp = "0" + tmp;
	        }
	        strA += tmp;
	        a = a.next;
	    }


	    while(b != null){
	        String tmp = String.valueOf(b.value);
	        while(tmp.length() < 4){
	            tmp = "0" + tmp;
	        }
	        strB += tmp;
	        b = b.next;
	    }

	    BigInteger result = new BigInteger(strA).add(new BigInteger(strB));
	    //System.out.println("A: " + strA + " B: " + strB);

	    String tmp = result.toString();
	    int tabs = tmp.length()%4!=0?tmp.length()/4+1 : tmp.length()/4;
	    int tabTracker = tabs - 1;
	    String[] resultArray = new String[tabs];
	    String tmp2 = "";
	    for(int i = tmp.length()-1; i >= 0; i--){
	        tmp2 = tmp.charAt(i) + tmp2;
	        if(tmp2.length() == 4){
	            resultArray[tabTracker] = tmp2;
	            //System.out.println("tabT: " + tabTracker);
	            tabTracker--;

	            tmp2 = "";
	        }
	        if(tabTracker == 0 && tmp2.length() < 4){
	            resultArray[tabTracker] = tmp2;
							


	        }

	    }

	    //System.out.println("Arr: " + Arrays.toString(resultArray));

	    ListNode<Integer> r = new ListNode<Integer>(Integer.parseInt(resultArray[0]));
	    ListNode<Integer> helper = r;
	    if(resultArray.length > 1){
	        for(int i = 1; i < resultArray.length; i++){

	            ListNode<Integer> node = new ListNode<Integer>(Integer.parseInt(resultArray[i]));
	            helper.next = node;
	            helper = helper.next;
	        }
	    }

	    return r;

	}


}
