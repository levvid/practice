
public class TreeSymmetric {
	//
	// Definition for binary tree:
	 class Tree<T> {
	   Tree(T x) {
	     value = x;
	   }
	   T value;
	   Tree<T> left;
	   Tree<T> right;
	 }
	boolean isTreeSymmetric(Tree<Integer> t) {
	    return isMirror(t, t);
	}

	boolean isMirror(Tree<Integer> tree1, Tree<Integer> tree2){
	    if(tree1 == null && tree2 == null)
	        return true;
	    // For two trees to be mirror images, the following three
	    // conditions must be true
	    // 1 - Their root node's key must be same
	    // 2 - left subtree of left tree and right subtree
	    //      of right tree have to be mirror images
	    // 3 - right subtree of left tree and left subtree
	    //      of right tree have to be mirror images
	    if(tree1 != null && tree2 != null && tree1.value == tree2.value){
	        return isMirror(tree1.left, tree2.right) && isMirror(tree1.right, tree2.left);
	    }
	    
	    return false;
	}

}
