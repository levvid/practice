import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LargestValueinTreeRows {
	//
	// Definition for binary tree:
	 class Tree<T> {
	   Tree(T x) {
	     value = x;
	   }
	   T value;
	   Tree<T> left;
	   Tree<T> right;
	 }
	int[] largestValuesInTreeRows(Tree<Integer> t) {
	    Queue<Tree<Integer>> queue = new LinkedList<Tree<Integer>>();
		    List<Integer> result = new ArrayList<Integer>();
		    queue.clear();
		    if(t == null) return new int[0];
		    queue.add(t);
		    while (!queue.isEmpty()) {
	            int largestValue = Integer.MIN_VALUE;
	            int size = queue.size();
	            for(int i = 0; i < size; i ++){
	                Tree tmp = queue.remove();
	                largestValue = Math.max(largestValue, (int)tmp.value);
	                System.out.println(tmp.value + " ");
	                if (tmp.left != null)
	                    queue.add(tmp.left);
	                if (tmp.right != null)
	                    queue.add(tmp.right);
	            }
	            result.add(largestValue);
		        
		    }
		    
		    return result.stream().mapToInt(i -> i).toArray();
	}


}
