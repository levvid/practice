
public class TreeHasPathWithGivenSum {
	//
	// Definition for binary tree:
	 class Tree<T> {
	   Tree(T x) {
	     value = x;
	   }
	   T value;
	   Tree<T> left;
	   Tree<T> right;
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	boolean hasPathWithGivenSum(Tree<Integer> t, int s) {
	    if(t == null)
	        return s == 0;
	    else{
	        boolean sumCheck = false;
	        int subSum = s - t.value;
	        if(t.left == null && t.right == null){
	            System.out.println("leaf");
	            return subSum == 0;
	        }
	        if(t.left != null){
	            sumCheck = sumCheck || hasPathWithGivenSum(t.left, subSum);
	        }
	        if(t.right !=null){
	            sumCheck = sumCheck || hasPathWithGivenSum(t.right, subSum);
	        }
	        return sumCheck;
	    }
	    
	    
	   
	}


}
