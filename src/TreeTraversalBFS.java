import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeTraversalBFS {
	//
	 //Definition for binary tree:
	 class Tree<T> {
	   Tree(T x) {
	     value = x;
	   }
	   T value;
	   Tree<T> left;
	   Tree<T> right;
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	int[] traverseTree(Tree<Integer> t) {
	    Queue<Tree<Integer>> queue = new LinkedList<Tree<Integer>>();
	    List<Integer> result = new ArrayList<Integer>();
	    queue.clear();
	    if(t == null) return new int[0];
	    queue.add(t);
	    while (!queue.isEmpty()) {
	        Tree tmp = queue.remove();
	        result.add((int)tmp.value);
	        System.out.println(tmp.value + " ");
	        if (tmp.left != null)
	            queue.add(tmp.left);
	        if (tmp.right != null)
	            queue.add(tmp.right);
	    }
	    
	    return result.stream().mapToInt(i -> i).toArray();
	}


}
