import java.util.ArrayList;
import java.util.List;

public class TreeBottom {
	
	
	public static void main(String[] args) {
		
  	String tree = "(2 (7 (2 () ()) (6 (5 () ()) (11 () ()))) (5 () (9 (4 () ()) ())))";
  	treeBottom(tree);
  	
  	
  }
	
	public static int[] treeBottom(String tree) {
		tree = tree.replaceAll(" ", "");
		int level = 0, maxLevel = 0;
		List<Integer> resultArrayList = new ArrayList<Integer>();
		for(char c: tree.toCharArray()){
			if(c == '('){
				level += 1;
			}
			else if(c == ')'){
				level -= 1;
			}else{
				maxLevel = Math.max(maxLevel, level);
			}
		}
		level = 0;
		String tmp = "";
		for(char c: tree.toCharArray()){
			
			if(c == '('){
				if(tmp != ""){
					resultArrayList.add(Integer.parseInt(tmp));
				}
				tmp = "";
				level += 1;
			}
			else if(c == ')'){
				level -= 1;
			}else if(level == maxLevel){
				tmp += c;
			}
		}
		for(int i = 0; i<resultArrayList.size(); i++){
			System.out.println(resultArrayList.get(i));
		}
		int[] resultArray = new int[resultArrayList.size()];
		for(int i = 0; i<resultArrayList.size(); i++){
			resultArray[i] = resultArrayList.get(i);
		}
		return resultArray;

	}
	


}
