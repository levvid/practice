import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CrypticSolution {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] crypt = {"SEND", "MORE", "DIAGNOSTIC"};
		char[][] solution = {{'O', '0'},
				            {'M', '1'},
				            {'Y', '2'},
				            {'E', '5'},
				            {'N', '6'},
				            {'D', '7'},
				            {'R', '8'},
				            {'S', '9'}};
		System.out.println(isCryptSolution(crypt, solution));
	}

	static boolean isCryptSolution(String[] crypt, char[][] solution) {
		Map<Character, Character> map = new HashMap<Character, Character>();
		String[] words = new String[3];
		Arrays.fill(words, "");
		for(int i = 0; i < solution.length; i++){
			map.put(solution[i][0], solution[i][1]);
		}
		for(int i = 0; i < 3; i++){//loop thru crypt
			for(int j = 0; j < crypt[i].length(); j++){
                char tmp = map.get(crypt[i].charAt(j));
                if(tmp == '0' && j == 0 && crypt[i].length() > 1) return false;
				words[i] += tmp;
			}
		}

        BigInteger sum = new BigInteger(words[0])
            .add(new BigInteger(words[1]));
        BigInteger result = new BigInteger(words[2]);

		if(!sum.equals(result))
			return false;

		return true;

	}
}
