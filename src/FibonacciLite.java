import java.io.*;
import java.util.*;

public class FibonacciLite {
    public static HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sCurrentLine = null;
        
        int n =0;
        try { 
            while ((sCurrentLine = reader.readLine()) != null) {
                n = Integer.parseInt(sCurrentLine);
                System.out.println(fibonacci(n));
                
            } 
        }catch(IOException e){
            e.printStackTrace();
        }
        
        /***********Alternatively****************/
//        Scanner sc = new Scanner(System.in);
//        while(sc.hasNextLine()){
//            int n = Integer.parseInt(sc.nextLine());
//            System.out.println(fibonacci(n));
//        }
    }
    
    public static int fibonacci(int number){
        
        if (number == 0 || number == 1) {
          return number;
        }
        else if (map.containsKey(number)) {
          return map.get(number);
        }
        else {
          int fibonacciValue = fibonacci(number - 2) + fibonacci(number - 1);
          map.put(number, fibonacciValue);
          return fibonacciValue;
        }
    }
}