
public class Leader {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//TEST FUNC 1
		int[] A = {4,3,4,4,4,2};
		System.out.println("Eq leaders num: " + equileaderNumber(A));
		
		//TEST FUNC 2
		int[] B = {3,4,3,2,3,-1,3,3}, C = {1,2,3,4,5,6};
		System.out.println("Dominator index: " + dominatorIndex(C));
		
		
		//TEST 
		
	}
	/***********************************************************************
	 * 
	 * @param A
	 * @return
	 * 
	 * from: https://rafal.io/posts/codility-equi-leader.html
	 * 
	 * A non-empty zero-indexed array A consisting of N integers is given.

		The leader of this array is the value that occurs in more than half of the elements of A.
		
		An equi leader is an index S such that 0 ≤ S < N − 1 and two sequences A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N − 1] have leaders of the same value.
		
		For example, given array A such that:
		
		    A[0] = 4
		    A[1] = 3
		    A[2] = 4
		    A[3] = 4
		    A[4] = 4
		    A[5] = 2
		we can find two equi leaders:
		
		0, because sequences: (4) and (3, 4, 4, 4, 2) have the same leader, whose value is 4.
		2, because sequences: (4, 3, 4) and (4, 4, 2) have the same leader, whose value is 4.
		The goal is to count the number of equi leaders.
		
		Write a function:
		
		class Solution { public int solution(int[] A); }
		that, given a non-empty zero-indexed array A consisting of N integers, returns the number of equi leaders.
		
		For example, given:
		
		    A[0] = 4
		    A[1] = 3
		    A[2] = 4
		    A[3] = 4
		    A[4] = 4
		    A[5] = 2
		the function should return 2, as explained above.
		
		Assume that:
		
		N is an integer within the range [1..100,000];
		each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].
		Complexity:
		
		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	private static int equileaderNumber(int[] A) {
		int numEquileaders = 0, counter = 1, leader = A[0];
		
		
		//find the leader
		for(int i = 1; i < A.length; i++) {
			if(leader == A[i]) {
				counter++;
			}else {
				counter--;
			}
			if(counter == 0) {
				counter = 1;
				leader = A[i];
			}
		}

		//make sure the leader covers more than half the elements
		int total = 0;
		for(int i = 0; i < A.length; i++) {
			if(A[i] == leader) {
				total++;
			}
		}
		if(total <= A.length/2) { //if less than  half, return 0. Impossible to have equileaders
			return 0;
		}
		
		//count the number of equileaders
		int leaderCount = 0;
		for(int i = 0; i < A.length; i++) {
			if(A[i] == leader) {
				leaderCount++;
			}
			int leadersInRightPart = total - leaderCount;
			//if leader in more than half the left and right subsections of array, then i an equileader
			if(leaderCount > (i+1)/2 && leadersInRightPart > (A.length - i - 1)/2) { //-1 bc A is zero indexed
				numEquileaders++;
			}
		}
		return numEquileaders;
	}
	
	/***********************************************************
	 * 
	 * from: https://rafal.io/posts/codility-dominator.html
	 * 
	 * @param A
	 * @return
	 * 
	 * A zero-indexed array A consisting of N integers is given. The dominator of array A is the value that occurs in more than half of the elements of A.

		For example, consider array A such that
		
		 A[0] = 3    A[1] = 4    A[2] =  3
		 A[3] = 2    A[4] = 3    A[5] = -1
		 A[6] = 3    A[7] = 3
		The dominator of A is 3 because it occurs in 5 out of 8 elements of A (namely in those with indices 0, 2, 4, 6 and 7) and 5 is more than a half of 8.
		
		Write a function
		
		class Solution { public int solution(int[] A); }
		that, given a zero-indexed array A consisting of N integers, returns index of any element of array A in which the dominator of A occurs. The function should return −1 if array A does not have a dominator.
		
		Assume that:
		
		N is an integer within the range [0..100,000];
		each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
		For example, given array A such that
		
		 A[0] = 3    A[1] = 4    A[2] =  3
		 A[3] = 2    A[4] = 3    A[5] = -1
		 A[6] = 3    A[7] = 3
		the function may return 0, 2, 4, 6 or 7, as explained above.
		
		Complexity:
		
		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	private static int dominatorIndex(int[] A) {
		if(A.length == 0) return -1;
		int dominatorInd = 0, counter = 0;
		
		for(int i = 0; i < A.length; i++) {
			if(A[i] == A[dominatorInd]) {
				counter++;
			}
			else {
				counter--;
			}
			if(counter == 0) {
				dominatorInd = i;
				counter = 1;
			}
		}
		
		int total = 0;
		for(int i = 0; i < A.length; i++) {
			if(A[i] == A[dominatorInd]) {
				total++;
			}
		}
		
		if(total <= A.length/2) return -1;
		return dominatorInd;
		
		/************Alternative solution***************************/
//		if(A.length == 0) return -1;
//	      int count     = 0;
//	      int elem      = A[0];
//	      
//	      for(int i : A){
//	          if(i == elem){    
//	              count++;
//	          } else {
//	              if(count == 0){
//	                  count++;
//	                  elem = i;
//	              }
//	              else count--;
//	          }
//	      }
//	      
//	      int ct = 0;
//	      int ind = -1;
//	      
//	      for(int i = 0; i < A.length; i++){
//	          if(A[i] == elem){
//	              ct++;
//	              ind = i;
//	          }
//	          
//	      }
//	      
//	      if(ct > A.length/2) return ind;
//	      else return -1;
	}
	
	

}
