import java.util.ArrayList;
import java.util.Stack;

public class StacksAndQueues {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//TEST FUNC 1
		//int[] A = new int[]{8,8,5,7,9,8,7,4,8};
		//System.out.println(stoneWall(A));
		
		//TEST FUNC 2
		//String S1 = "{[()()]}", S2 =  "([)()]";
		//System.out.println(bracketsNesting(S2));
		
		
		//TEST FUNC 3
//		String S1 = "(()(())())", S2 = "())", S3 = ")" , S4 = "((()())())";
//		System.out.println(nestingBrackets(S4));
		
		//TEST FUNC 4
		int[] A = new int[] {4, 3, 2, 1, 5}, B = new int[] {0, 1, 0, 0, 0};
		//int[] A = new int[] {4}, B = new int[] {0};
		System.out.println("Fish remaining: " + fishRemaining(A, B));
		
	}


	/*********************************************************************************
	 * 
	 * @param H
	 * @return
	 * 
	 * from: https://github.com/jmornar/codility-java-solutions/blob/master/src/stackandqueue/StoneWall.java
	 * 
	 * You are going to build a stone wall. The wall should be straight and N meters long, and 
	 * its thickness should be constant; however, it should have different heights in different places. 
	 * The height of the wall is specified by a zero-indexed array H of N positive integers. H[I] is the 
	 * height of the wall from I to I+1 meters to the right of its left end. In particular, H[0] is the height 
	 * of the wall's left end and H[N−1] is the height of the wall's right end.

		The wall should be built of cuboid stone blocks (that is, all sides of such blocks are rectangular). Your task is to compute the minimum number of blocks needed to build the wall.

		Write a function:

		class Solution { public int solution(int[] H); }
		that, given a zero-indexed array H of N positive integers specifying the height of the wall, returns the minimum number of blocks needed to build it.

		For example, given array H containing N = 9 integers:

		  H[0] = 8    H[1] = 8    H[2] = 5
		  H[3] = 7    H[4] = 9    H[5] = 8
		  H[6] = 7    H[7] = 4    H[8] = 8
		the function should return 7. The figure shows one possible arrangement of seven blocks.



		Assume that:

		N is an integer within the range [1..100,000];
		each element of array H is an integer within the range [1..1,000,000,000].
		Complexity:

		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	public static int stoneWall(int[] H){
		Stack<Integer> stack = new Stack<Integer>();
		int blocks = 1;
		stack.push(H[0]);
		for (int i = 1; i < H.length; i++) {
			if (stack.peek() ==  H[i])
				continue;
			else if (H[i] > stack.peek()) {
				stack.push(H[i]);
				blocks++;
			} else {
				while(stack.size()>0 && stack.peek()>H[i])
					stack.pop();
				if (stack.size() == 0 || stack.peek() != H[i]) {
					stack.push(H[i]);
					blocks++;
				}
			}
		}
		return blocks;

	}

	/***********************************STONEWALL 2*********************************
	 * 
	 * from : https://gist.githubusercontent.com/Ceva24/8493120/raw/11f78c8b17066e66a6d23687e95aad3f87154859/StoneWall.java
	 * 
	 * so when we find two indices that are of the same height, we can use the same block, providing all the values in-between are higher.
	 *
	 * if we find a lower value, end the block.

	 * idea - store a list of currently-active blocks. next interations can build upon top of these.
	 * once the height of an index is lower than an active block, remove that block from activity, and add a new block equaling the index's height
	 * to the active list.

	 * are we able to accurately track the number of blocks used by doing this?

	 * Must be O(N) - no inner loops.
	 * no addition or multiplication - should be safe using ints rather than longs.



	 * extra - can we store blocks individual heights in the list rather than total height? an int tracking total height?
	 */
	public static int stoneWall2(int[] H) {
		int numberOfBlocks = 0;
		int totalHeight = 0;

		final ArrayList<Integer> activeBlocks = new ArrayList<Integer>(); // depending on this being insertion-order.

		for (int i = 0; i < H.length; i++)
		{
			int height = H[i];

			if (totalHeight > height)
			{
				// remove block(s) - add another if necessary.

				for (int j = activeBlocks.size() - 1; j >= 0; j--) // first trouble - how do we scan and remove lesser blocks in O(N) time... is it still O(N)?
				{
					int latestBlock = activeBlocks.get(j);
					activeBlocks.remove(j);
					totalHeight -= latestBlock;

					if ((totalHeight) <= height) // removed the latest block and now we're not too high.
					{
						// we're done.
						break;
					}
				}

				// adding the other if necessary.
				if (totalHeight < height)
				{
					int newBlock = height - totalHeight;
					totalHeight += newBlock;
					activeBlocks.add(newBlock);
					numberOfBlocks++;
				}

			}
			else if (totalHeight == height)
			{
				continue;
			}
			else
			{
				int newBlock = height - totalHeight;
				activeBlocks.add(newBlock);
				totalHeight += newBlock;

				// add a block. increase total height.
				numberOfBlocks++;
			}
		}

		return numberOfBlocks;

	}



	/****************************************************************
	 * 
	 * @param S
	 * @return
	 * 
	 * A string S consisting of N characters is considered to be properly nested if any of the following conditions is true:

		S is empty;
		S has the form "(U)" or "[U]" or "{U}" where U is a properly nested string;
		S has the form "VW" where V and W are properly nested strings.
		For example, the string "{[()()]}" is properly nested but "([)()]" is not.

		Write a function:

		class Solution { public int solution(String S); }
		that, given a string S consisting of N characters, returns 1 if S is properly nested and 0 otherwise.

		For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.

		Assume that:

		N is an integer within the range [0..200,000];
		string S consists only of the following characters: "(", "{", "[", "]", "}" and/or ")".
		Complexity:

		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(N) (not counting the storage required for input arguments).
	 */
	public static int bracketsNesting(String S){
		Stack<Character> stack = new Stack<Character>();
		String openingBrackets = "({[", closingBrackets = ")}]";
		
		for(int i = 0; i < S.length(); i++) {
			char c = S.charAt(i);
			if(openingBrackets.contains(String.valueOf(c))) {
				System.out.println("Pushed: " + c) ;
				stack.push(c);
			}
			else {
				if(!stack.isEmpty()) {
					char openingBracket = stack.pop();
					if(getClosingBracket(openingBracket) != c){
						return 0;
					}
				}else {
					return 0;
				}
			}
		}
		
		return stack.size() == 0? 1: 0;

	}
	
	public static char getClosingBracket(char bracket) {
		if(bracket == '(')
			return ')';
		else if(bracket == '{')
			return '}';
		return ']';
					
	}
	/***********************
	 * 
	 * @param S
	 * @return
	 * 
	 * A string S consisting of N characters is called properly nested if:

		S is empty;
		S has the form "(U)" where U is a properly nested string;
		S has the form "VW" where V and W are properly nested strings.
		For example, string "(()(())())" is properly nested but string "())" isn't.
		
		Write a function:
		
		class Solution { public int solution(String S); }
		that, given a string S consisting of N characters, returns 1 if string S is properly nested and 0 otherwise.
		
		For example, given S = "(()(())())", the function should return 1 and given S = "())", the function should return 0, as explained above.
		
		Assume that:
		
		N is an integer within the range [0..1,000,000];
		string S consists only of the characters "(" and/or ")".
		Complexity:
		
		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(1) (not counting the storage required for input arguments).
	 */
	public static int nestingBrackets(String S) {
		Stack<Character> stack = new Stack<Character>();
		
		for(int i = 0; i < S.length(); i++) {
			char c = S.charAt(i);
			if(c == '(') {
				stack.push(c);
			}
			else {
				if(!stack.isEmpty()) {
					char openingBracket = stack.pop();
					if(openingBracket != '(') {
						return 0;
					}
				}
				else {
					return 0;
				}
			}
		}
		return stack.size() == 0?1:0;
	}
	
	
	/******************************************************************
	 * 
	 * @param A
	 * @param B
	 * @return
	 * 
	 * 
	 * from: https://rafal.io/posts/codility-fish.html
	 * 
	 * You are given two non-empty zero-indexed arrays A and B consisting of N integers. Arrays A and B represent N voracious fish in a river, ordered downstream along the flow of the river.

		The fish are numbered from 0 to N − 1. If P and Q are two fish and P < Q, then fish P is initially upstream of fish Q. Initially, each fish has a unique position.
		
		Fish number P is represented by A[P] and B[P]. Array A contains the sizes of the fish. All its elements are unique. Array B contains the directions of the fish. It contains only 0s and/or 1s, where:
		
		0 represents a fish flowing upstream,
		1 represents a fish flowing downstream.
		If two fish move in opposite directions and there are no other (living) fish between them, they will eventually meet each other. Then only one fish can stay alive − the larger fish eats the smaller one. More precisely, we say that two fish P and Q meet each other when P < Q, B[P] = 1 and B[Q] = 0, and there are no living fish between them. After they meet:
		
		If A[P] > A[Q] then P eats Q, and P will still be flowing downstream,
		If A[Q] > A[P] then Q eats P, and Q will still be flowing upstream.
		We assume that all the fish are flowing at the same speed. That is, fish moving in the same direction never meet. The goal is to calculate the number of fish that will stay alive.
		
		For example, consider arrays A and B such that:
		
		  A[0] = 4    B[0] = 0
		  A[1] = 3    B[1] = 1
		  A[2] = 2    B[2] = 0
		  A[3] = 1    B[3] = 0
		  A[4] = 5    B[4] = 0
		Initially all the fish are alive and all except fish number 1 are moving upstream. Fish number 1 meets fish number 2 and eats it, then it meets fish number 3 and eats it too. Finally, it meets fish number 4 and is eaten by it. The remaining two fish, number 0 and 4, never meet and therefore stay alive.
		
		Write a function:
		
		class Solution { public int solution(int[] A, int[] B); }
		that, given two non-empty zero-indexed arrays A and B consisting of N integers, returns the number of fish that will stay alive.
		
		For example, given the arrays shown above, the function should return 2, as explained above.
		
		Assume that:
		
		N is an integer within the range [1..100,000];
		each element of array A is an integer within the range [0..1,000,000,000];
		each element of array B is an integer that can have one of the following values: 0, 1;
		the elements of A are all distinct.
		Complexity:
		
		expected worst-case time complexity is O(N);
		expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
		Elements of input arrays can be modified.
	 */
	private static int fishRemaining(int[] A, int[] B) {
		Stack<Integer> stack = new Stack<Integer>();
	
		for(int i = 0; i < A.length; i++) {
			int size = A[i], dir = B[i];
			if(stack.isEmpty()) {//add to stack if no fish in the stack currently == no fish processed
				stack.push(i);
			}
			else { //fish have been processed
				//stack is not empty
				//previous fish was travelling downstream and the current one is travelling upstream dir - B[stack.peek()] == -1 ==> 0 - 1 == -1
				//previous fish is smaller than the current one, then eat it (stack.pop())
				while(!stack.isEmpty() && (dir - B[stack.peek()] == -1) && A[stack.peek()] < size) {
					stack.pop();
				}
				if(!stack.isEmpty()) {
					if(dir - B[stack.peek()] != -1) {//previous fish travelling upstream OR both prev and current travelling downstream --> add to stack
						stack.push(i);
					}
				}
				else {
					stack.push(i); //stack is empty, just add the fish
				}
			}
		}
		return stack.size();
	}
}
