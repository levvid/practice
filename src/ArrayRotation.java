import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.security.KeyStore.Entry;
import java.util.regex.*;
import java.util.stream.Collectors;

public class ArrayRotation {

    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int n = in.nextInt();
//        int k = in.nextInt();
//        int a[] = new int[n];
//        for(int a_i=0; a_i < n; a_i++){
//            a[a_i] = in.nextInt();
//        }
    	//System.out.println("root: " + getRoot(n));
    	int[] a = {10000, 627615, 588225 };
    	digitRootSort(a);
    	
    	
    }
    
    public static int[] digitRootSort(int[] a) {
    	
    	Map<Integer, Integer> unsortedMap = new HashMap<Integer, Integer>();
    	for(int i = 0; i < a.length; i++){
    		System.out.println("key: " + a[i] + " Value: " + getRoot(a[i]));
    		unsortedMap.put(a[i], getRoot(a[i]));
    	}
    
    	
    	ArrayList<Integer> sortedDigits = new ArrayList<Integer>();
    	unsortedMap.entrySet().stream()
        .sorted((k1, k2) -> {return k1.getValue().equals(k2.getValue()) ? k1.getKey().compareTo(k2.getKey()) : 
        	k1.getValue().compareTo(k2.getValue()); }
        //k1.getValue().compareTo(k2.getValue())
        )
        //.forEach(k -> System.out.println(k.getKey() + ": " + k.getValue()))
        .forEach(k -> sortedDigits.add(k.getKey()));
    	System.out.println(" AND ");

    	
    	int[] result = new int[sortedDigits.size()];
    	for (int i = 0; i < sortedDigits.size(); i++) {
    	    result[i] = sortedDigits.get(i);
    	}
    	
    	for(int i=0; i<result.length; i++){
    		if(i==0)
    			System.out.print("[" + result[i] + ", ");
    		else if(i==result.length-1)
    			System.out.print(result[i] + "]");
    		else
    			System.out.print(result[i] + ", ");
    	}
    	
    	
    	
		return result;
        
        
    }
    
    public static int getRoot(int n) {
//        int root=n;
//        while ( (root=((root%10) + root/10))>9  );
//        return root;
    	
    	int num = n;
        int sum = 0;
        while (num > 0) {
            sum = sum + num % 10;
            num = num / 10;
        }
        return sum;
    } 
}




/************************************************************************************************************/

/************************************************************************************************
 * A zero-indexed array A consisting of N integers is given. Rotation of the array means that each element is shifted right by one index, and the last element of the array is also moved to the first place.

For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7]. The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.

Write a function:

class Solution { public int[] solution(int[] A, int K); }
that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.

For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].

Assume that:

N and K are integers within the range [0..100];
each element of array A is an integer within the range [-1,000..1,000].
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.

 */
//import java.io.*;
//import java.util.*;
//import java.text.*;
//import java.math.*;
//import java.util.regex.*;
//
//public class Solution {
//
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int n = in.nextInt();
//        int k = in.nextInt();
//        int a[] = new int[n];
//        for(int a_i=0; a_i < n; a_i++){
//            a[a_i] = in.nextInt();
//        }
//        
//        int i;
//        for(i = 0; i < k; i++){
//            leftRotateByOne(a, n);
//        }
//        
//        printArray(a, n);
//    }
//    
//    private static void printArray(int arr[], int size){
//        int i;
//        for (i = 0; i < size; i++)
//            System.out.print(arr[i] + " ");
//    }
//    
//    
//    public static void leftRotateByOne(int[] a, int n){
//        int i, temp;
//        temp = a[0];
//        for(i = 0; i < n - 1; i++){
//            a[i] = a[i+1];
//        }
//        a[i] = temp;
//    }

//private void rightRotateByOne(int[] A){
//    int tmp = A[A.length - 1];
//    for(int i = A.length - 1; i > 0; i--){
//        A[i] = A[i-1];
//    }
//    A[0] = tmp;
//}
//}
