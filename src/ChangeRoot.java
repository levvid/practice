import java.util.Arrays;

public class ChangeRoot {
	public static void main(String[] args) {
  	int[] a = {};
  	
  	
  	
  }
	
	public static int[] changeRoot(int[] parent, int newRoot) {
		int[] result = new int[parent.length];
		boolean[] resultTracker = new boolean[parent.length];
		
		int newRootTmp = newRoot;
		int parentOfNewRootTmp = parent[newRootTmp];
		result[newRootTmp] = newRootTmp;
		resultTracker[newRootTmp] = true;
		while(newRootTmp != parentOfNewRootTmp){
			
			result[parentOfNewRootTmp] = newRootTmp;
			resultTracker[parentOfNewRootTmp] = true;
			newRootTmp = parentOfNewRootTmp;
			parentOfNewRootTmp = parent[newRootTmp];
		}
		
		for(int i=0; i<result.length; i++){
			if(!resultTracker[i]){
				result[i] = parent[i];
			}
		}
		
		return result;
	}
}
