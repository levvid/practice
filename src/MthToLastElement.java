import java.io.*;
import java.util.*;


//bufferedreader example
public class MthToLastElement {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] numbers = null;
        String sCurrentLine = null;
        LinkedList<String> list = new LinkedList<String>();
        int i = 0, n =0;
        try { 
            while ((sCurrentLine = reader.readLine()) != null) {
                if(i ==0)
                    n = Integer.parseInt(sCurrentLine);
                
                if(i==1)
                    numbers = sCurrentLine.split(" ");  
                i++;
            } 
        }catch(IOException e){
            e.printStackTrace();
        }
        if(n > numbers.length){
            System.out.println("NIL");
            return;
        }
           
        for(int j=0; j<numbers.length; j++){
            list.addFirst(numbers[j]);
        }
        System.out.println(list.get(n-1));
        
        
        //using Scanner times out
//        Scanner sc=new Scanner(System.in);  
//        int n = Integer.parseInt(sc.nextLine());
//        String[] numbers = sc.nextLine().split(" ");
//        LinkedList<String> list = new LinkedList<String>();
//       
//        if(n > numbers.length){
//            System.out.println("NIL");
//            return;
//        }
//        for(int i=0; i< numbers.length; i++){
//            list.addFirst(numbers[i]);
//        }
//       
//        System.out.println(list.get(n-1));
    }
}