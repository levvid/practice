"""
Maze of size n with 0's being navigable and 1's being walls.

"""


def solution(maze, n):
    # Type your solution here
    def is_safe(i, j):
        return i < n and j < n and maze[i][j] != 1

    def solve(i, j, visited):
        if is_safe(i, j) and not visited[i][j]:
            if i == n - 1 == j:
                return True
            visited[i][j] = True
            left = solve(i, j + 1, visited)
            if left:
                return True

            down = solve(i + 1, j, visited)
            if down:
                return True

        return False

    visited = [[False for i in range(n)] for i in range(n)]

    tmp = solve(0, 0, visited)

    return tmp


