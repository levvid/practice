from collections import defaultdict


def divisorGame(self, N: int) -> bool:
    def game(num, player):
        if num == 1:
            if player == 'A':  # no more moves for A
                return False
            else:  # no more moves for B
                return True

        if num in self.game_dict:
            return

        if player == 'A':
            player = 'B'
        else:
            player = 'A'

        divisors = []

        for i in range(1, (num // 2) + 1):
            if num % i == 0:
                divisors.append(i)

        print('Divisors for num {} are {}'.format(num, divisors))
        for divisor in divisors:
            self.game_dict[num] = game(num - divisor, player)

    self.game_dict = defaultdict(bool)
    game(N, 'A')
    print('Game dict {}'.format(any(self.game_dict.values())))
    return any(self.game_dict.values())








