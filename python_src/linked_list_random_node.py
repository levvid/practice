"""
Given a singly linked list, return a random node's value from the linked list. Each node must have the same probability of being chosen.

Follow up:
What if the linked list is extremely large and its length is unknown to you? Could you solve this efficiently without using extra space?

Example:

// Init a singly linked list [1,2,3].
ListNode head = new ListNode(1);
head.next = new ListNode(2);
head.next.next = new ListNode(3);
Solution solution = new Solution(head);

// getRandom() should return either 1, 2, or 3 randomly. Each element should have equal probability of returning.
solution.getRandom();
"""
import random
from collections import defaultdict

from add_two_numbers_linked_list import ListNode


class Solution:

    def __init__(self, head: ListNode):
        """
        @param head The linked list's head.
        Note that the head is guaranteed to be not null, so it contains at least one node.
        """
        self.head = head
        self.num_elements = 0
        curr = head

        self.node_dict = defaultdict(ListNode)
        while curr:
            self.num_elements += 1
            self.node_dict[self.num_elements] = curr
            curr = curr.next

    def getRandom(self) -> int:
        """
        Returns a random node's value.
        """
        rand_index = random.randint(1, self.num_elements)

        return self.node_dict[rand_index].val
