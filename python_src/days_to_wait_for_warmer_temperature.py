"""
Given a list of daily temperatures T, return a list such that, for each day in the input,
tells you how many days you would have to wait until a warmer temperature. If there is no future day for which this is
possible, put 0 instead.

For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 73], your output should be
[1, 1, 4, 2, 1, 1, 0, 0].

Note: The length of temperatures will be in the range [1, 30000]. Each temperature will be an integer in the
range [30, 100].
"""

from typing import List


def dailyTemperatures(self, T: List[int]) -> List[int]:
    #         t_dict = defaultdict(int)

    #         for i in range(len(T)):
    #             t_dict[i] = T[i]


    #         sorted_dict = sorted(t_dict.items(), key=itemgetter(1), reverse=True)

    #         print('T_dict {}'.format(t_dict.items()))
    #         print('Sorted dict {}'.format(sorted_dict))

    #         i = 0
    #         for key, value in sorted_dict:
    #             if key
    #             print('Key {} value {}'.format(key, value))







    #         temp_list = [0]*len(T)
    #         print(temp_list)

    #         for i in range(len(T)):
    #             count = 1
    #             for j in range(i+1, len(T)):
    #                 if T[i] < T[j]:
    #                     temp_list[i] = count
    #                     break
    #                 else:
    #                     count += 1

    #         return temp_list
    temperatures = T
    n = len(temperatures)
    wait = [0] * n
    closest_gt = [ n -1]
    for i in range( n -2, -1, -1):
        if temperatures[ i +1] > temperatures[i]:
            wait[i] = 1
        else:
            while closest_gt:
                j = closest_gt[-1]
                if temperatures[j] > temperatures[i]:
                    wait[i] = j - i
                    break
                else:
                    closest_gt.pop()
        closest_gt.append(i)
    return wait