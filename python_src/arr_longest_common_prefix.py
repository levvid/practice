"""
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
Note:

All given inputs are in lowercase letters a-z.
"""
from typing import List


def longestCommonPrefix(self, strs: List[str]) -> str:
    if not strs:
        return ''

    if len(strs) == 1:
        return strs[0]

    min_word = min(strs, key=len)

    is_prefix = [True for c in range(len(min_word))]

    for word in strs:
        for i in range(len(word)):
            if i > len(is_prefix) - 1:
                break

            if word[i] != min_word[i]:
                is_prefix[i] = False
                break

    ret = ''
    for i in range(len(min_word)):
        if is_prefix[i]:
            ret += min_word[i]
        else:
            break
    return ret
