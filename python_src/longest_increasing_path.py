from typing import List


def longestIncreasingPath(self, matrix: List[List[int]]) -> int:
    def increasing_path(curr, path, i, j):
        if i < len(matrix) and j < len(matrix[0]):
            if matrix[i][j] > curr:
                increasing_path(matrix[i][j], path.append(matrix[i][j]), i + 1, j)
                increasing_path(matrix[i][j], path.append(matrix[i][j]), i - 1, j)
                increasing_path(matrix[i][j], path.append(matrix[i][j]), i, j + 1)
                increasing_path(matrix[i][j], path.append(matrix[i][j]), i, j - 1)
        else:
            return self.paths.append(path)

    self.paths = []
    increasing_path(matrix[0][0], [], 0, 0)
    print(self.paths)
