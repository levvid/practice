"""
Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along
the path equals the given sum.

Note: A leaf is a node with no children.

Example:

Given the below binary tree and sum = 22,

      5
     / \
    4   8
   /   / \
  11  13  4
 /  \      \
7    2      1
return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
"""
from bin_tree_inorder_traversal import TreeNode


def hasPathSum(self, root: TreeNode, sum: int) -> bool:
    def dfs(node, curr):
        if node:
            curr += node.val
            if not node.left and not node.right:
                if curr == sum:
                    self.check = True
            else:
                dfs(node.left, curr)
                dfs(node.right, curr)

    self.check = False
    dfs(root, 0)

    return self.check