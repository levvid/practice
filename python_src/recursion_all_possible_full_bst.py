"""
A full binary tree is a binary tree where each node has exactly 0 or 2 children.
Return a list of all possible full binary trees with N nodes.  Each element of the answer is the root node of one possible tree.
Each node of each tree in the answer must have node.val = 0.
You may return the final list of trees in any order.

Example 1:

Input: 7
Output: [[0,0,0,null,null,0,0,null,null,0,0],[0,0,0,null,null,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,null,null,null,null,0,0],[0,0,0,0,0,null,null,0,0]]
Explanation:

Note:

1 <= N <= 20
"""
from typing import List
import binary_tree_print


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def allPossibleFBT(N: int) -> List[TreeNode]:
    count = N - 1
    root = TreeNode(0)


def generate_trees(node, count, lst):
    while count > 0:
        left = TreeNode(0)
        right = TreeNode(0)
        if count >= 2:
            node.left = left
            node.right = right
            count -= 2
        generate_trees(node.left, count)
        generate_trees(node.right, count)


def test():
    root = TreeNode(1)
    left = TreeNode(2)
    right = TreeNode(3)
    root.left = left
    root.right = right
    tmp = TreeNode(4)
    left.left = tmp
    binary_tree_print.printTree(root)
