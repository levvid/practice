"""
Given an integer (signed 32 bits), write a function to check whether it is a power of 4.

Example 1:

Input: 16
Output: true
Example 2:

Input: 5
Output: false
Follow up: Could you solve it without loops/recursion?
"""
import math

num = 15
num = -2147483648
def is_power_of_four(n: int) -> bool:
    if n <= 0:
        return False

    log_n = math.log(n, 4)

    if log_n.is_integer():
        return True
    else:
        return False