"""
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.

Example 1:

Input: "()"
Output: true
Example 2:

Input: "()[]{}"
Output: true
Example 3:

Input: "(]"
Output: false
Example 4:

Input: "([)]"
Output: false
Example 5:

Input: "{[]}"
Output: true
"""

test = '(([]))]'
def isValid(s: str) -> bool:
    if len(s) == 0:
        return True

    stack = []

    for c in s:
        if c == '(' or c == '[' or c == '{':
            stack.append(c)
        else:
            if len(stack) == 0:
                print('False 1')
                return False
            p = stack.pop()
            if (c == ')' and p != '(') or (c == ']' and p != '[') or (c == '}' and p != '{'):
                print('False 2')
                return False

    if len(stack) > 0:
        print('False 3')
        return False
    return True


def is_valid(S):
    stack = []
    brackets = {
        '(': ')',
        '[': ']',
        '{': '}'
    }

    if len(S) <= 1:
        return 1

    for s in S:
        if s in brackets.keys():
            stack.append(s)
        else:
            if len(stack) == 0:
                return 0
            else:
                opening = stack.pop()

                if brackets[opening] != s:
                    return 0

    if len(stack) != 0:
        return 0

    return 1
