"""
Given an 2D board, count how many battleships are in it. The battleships are represented with 'X's, empty slots are represented with '.'s. You may assume the following rules:
You receive a valid board, made of only battleships or empty slots.
Battleships can only be placed horizontally or vertically. In other words, they can only be made of the shape 1xN (1 row, N columns) or Nx1 (N rows, 1 column), where N can be of any size.
At least one horizontal or vertical cell separates between two battleships - there are no adjacent battleships.
Example:
X..X
...X
...X
In the above board there are 2 battleships.
Invalid Example:
...X
XXXX
...X
This is an invalid board that you will not receive - as battleships will always have a cell separating between them.
Follow up:
Could you do it in one-pass, using only O(1) extra memory and without modifying the value of the board?
"""
from typing import List


def countBattleships(board: List[List[str]]) -> int:
    """
    row +-1, col
    row, col+-1
    """

    rows = len(board)
    cols = len(board[0])
    ship_cells = []
    num_ships = 0

    for i in range(rows):
        for j in range(cols):
            if board[i][j] == 'X':
                checks = [(i - 1, j), (i + 1, j), (i, j + 1), (i, j - 1)]
                if not any(x in checks for x in ship_cells):
                    num_ships += 1
                ship_cells.append((i, j))

    return num_ships
