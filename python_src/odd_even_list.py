"""
Given a singly linked list, group all odd nodes together followed by the even nodes.
Please note here we are talking about the node number and not the value in the nodes.

You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time complexity.

Example 1:

Input: 1->2->3->4->5->NULL
Output: 1->3->5->2->4->NULL
Example 2:

Input: 2->1->3->5->6->4->7->NULL
Output: 2->3->6->7->1->5->4->NULL
Note:

The relative order inside both the even and odd groups should remain as it was in the input.
The first node is considered odd, the second node even and so on ...
"""

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def oddEvenList(self, head: ListNode) -> ListNode:
    dummy = ListNode(0)
    even_head = ListNode(0)
    even_nodes_tmp = ListNode(0)
    odd_tail = ListNode(0)
    curr = head
    i = 1
    if not head or not head.next:
        return head

    while curr:
        if i % 2 == 0:
            even_nodes_tmp.next = curr
            even_nodes_tmp = curr
            if not even_head.next:
                even_head.next = curr
        else:
            odd_tail.next = curr
            odd_tail = curr
            if not dummy.next:
                dummy.next = curr

        curr = curr.next
        i += 1

    if even_nodes_tmp:
        even_nodes_tmp.next = None
    if not dummy.next:
        dummy.next = even_head.next
    else:
        odd_tail.next = even_head.next
    return dummy.next
