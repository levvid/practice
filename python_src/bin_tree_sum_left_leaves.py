"""
Find the sum of all left leaves in a given binary tree.

Example:

    3
   / \
  9  20
    /  \
   15   7

There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.
"""
from bin_tree_delete_nodes import TreeNode


def sumOfLeftLeaves(self, root: TreeNode) -> int:
    def dfs(node):
        if node:
            if node.left:
                if not node.left.left and not node.left.right:
                    self.sum += node.left.val
            dfs(node.left)
            dfs(node.right)

    self.sum = 0
    dfs(root)

    return self.sum
