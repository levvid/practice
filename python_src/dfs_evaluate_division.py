"""
Equations are given in the format A / B = k, where A and B are variables represented as strings, and k is a real number
(floating point number). Given some queries, return the answers. If the answer does not exist, return -1.0.

Example:
Given a / b = 2.0, b / c = 3.0.
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? .
return [6.0, 0.5, -1.0, 1.0, -1.0 ].

The input is: vector<pair<string, string>> equations, vector<double>& values, vector<pair<string, string>> queries ,
where equations.size() == values.size(), and the values are positive. This represents the equations.
Return vector<double>.

According to the example above:

equations = [ ["a", "b"], ["b", "c"] ],
values = [2.0, 3.0],
queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ].


The input is always valid. You may assume that evaluating the queries will result in no division by zero and there is
no contradiction.
"""

from collections import defaultdict
from typing import List


def dfs(self, graph, path, a, target, visited):
    if a in visited:
        return 0

    visited.add(a)
    for v in graph[a]:
        if v == target:
            return path * graph[a][v]
        ret = self.dfs(graph, path * graph[a][v], v, target, visited)
        if ret:
            return ret


def calcEquation(self, equations: List[List[str]], values: List[float], queries: List[List[str]]) -> List[float]:
    graph = defaultdict(dict)
    for eq, ratio in zip(equations, values):
        u, v = eq
        graph[u][v] = ratio
        graph[v][u] = 1 / ratio

    result = []
    for a, b in queries:
        if a not in graph or b not in graph:
            result.append(-1.0)
        else:
            path = self.dfs(graph, 1, a, b, set())
            if path:
                result.append(path)
            else:
                result.append(-1.0)
    return result
