"""
Given a non-empty array of digits representing a non-negative integer, plus one to the integer.

The digits are stored such that the most significant digit is at the head of the list,
and each element in the array contain a single digit.

You may assume the integer does not contain any leading zero, except the number 0 itself.

Example 1:

Input: [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Example 2:

Input: [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
"""
from typing import List

digs = [1,9,9,9,9]


def plusOne(digits: List[int]) -> List[int]:
    if len(digits) < 1:
        return [1]

    ret_digits = digits[::-1]
    tmp = ret_digits[0] + 1

    if tmp > 9:
        rem = tmp % 10
        carry = tmp // 10
        ret_digits[0] = rem
        i = 1
        while i < len(ret_digits):
            if carry == 0:
                break
            else:
                tmp = carry + ret_digits[i]
                rem = tmp % 10
                carry = tmp // 10
                ret_digits[i] = rem
            i += 1
        if carry != 0:
            ret_digits.append(carry)
    else:
        ret_digits[0] = tmp

    ret = ret_digits[::-1]

    return ret

