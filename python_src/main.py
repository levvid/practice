"""
Main
"""
import invalid_transactions
import is_power_of_two
import find_disappeared_numbers
import largest_24_hour_time
import reorder_logs
import generate_parenthesis
import two_sum
import prison_after_n_days
import bin_search_search_insert
import count_characters
import longest_palindromic_substring
import k_closest_points
import dp_max_stock_profit
import array_product_except_self
import is_one_bit_character
import permute_numbers
import search_sorted_array_with_pivot
import find_common_restaurant
import num_pairs_divisible_by_60
import subdomain_visits
import is_power_of_four
import bulb_switch
import duplicate_zeros
import reverse_only_letters
import reverser_words
import find_words_in_matrix
import is_valid_bracket
import fraction_to_decimal
import array_mountain_array
import to_goat_latin
import fraction_to_decimal
import arr_plus_one
import find_replace_string
import dp_climb_stairs
import arr_pivot_index
import count_sequences
import arr_mat_flood_fill
import arr_k_most_frequent
import recursion_count_of_atoms
import recursion_all_possible_full_bst
import recursion_tribonacci
import recursion_kth_grammar
import dp_house_robber
import dp_max_subarray
import dp_min_cost_climbing_stairs
import remove_outer_parenthesis
import dp_can_win_nim_game
import arr_count_characters
import int_to_roman
import self_dividing_numbers
import matrix_cherry_pickup


def driver():
    print('Running...')
    ret = matrix_cherry_pickup.cherry_pickup(matrix_cherry_pickup.TEST)
    # ret = self_dividing_numbers.Solution().selfDividingNumbers(self_dividing_numbers.TEST['left'],
    #                                                          self_dividing_numbers.TEST['right'])
    # ret = int_to_roman.int_to_roman(int_to_roman.TEST)

    # ret = arr_count_characters.Solution().countCharacters(arr_count_characters.WORDS, arr_count_characters.CHARS)
    # ret = dp_can_win_nim_game.can_win_nim(dp_can_win_nim_game.TEST)
    # ret = remove_outer_parenthesis.remove_outer_parenthesis(remove_outer_parenthesis.TEST)
    # ret = dp_min_cost_climbing_stairs.minCostClimbingStairs(dp_min_cost_climbing_stairs.COST)
    # ret = dp_max_subarray.max_subarray(dp_max_subarray.test)
    # ret = dp_house_robber.rob(dp_house_robber.test)
    # for i in range(1, 20):
    #     ret = recursion_kth_grammar.kth_grammar(i, 1)
    # ret = recursion_tribonacci.tribonacci_two(29)
    # ret = recursion_all_possible_full_bst.test()
    # ret = recursion_count_of_atoms.countOfAtoms(recursion_count_of_atoms.test)
    # ret = arr_k_most_frequent.topKFrequent(arr_k_most_frequent.arr, arr_k_most_frequent.K)
    # ret = arr_mat_flood_fill.floodFill(arr_mat_flood_fill.IMAGE, 0, 0, 2)
    # ret = arr_mat_flood_fill.neighbours(arr_mat_flood_fill.IMAGE, 2, 0, 1)
    # ret = arr_pivot_index.pivotIndex2(arr_pivot_index.nums)
    # ret = count_sequences.knightDialer((31045))
    # ret = find_replace_string.findReplaceString(find_replace_string.S, find_replace_string.indexes, find_replace_string.sources, find_replace_string.targets)
    # ret = climb_stairs.climbStairs(climb_stairs.num)
    # ret = arr_plus_one.plusOne(arr_plus_one.digs)
    # ret = fraction_to_decimal.fractionToDecimal(fraction_to_decimal.numerator, fraction_to_decimal.denominator)
    # ret = to_goat_latin.toGoatLatin(to_goat_latin.string)

    # ret = reverse_only_letters.reverseOnlyLetters(reverse_only_letters.s)
    # ret = reverser_words.reverseWords(reverser_words.input)
    # ret = find_words_in_matrix.findWords(find_words_in_matrix.b, find_words_in_matrix.words)
    # ret = is_valid_bracket.isValid(is_valid_bracket.test)
    # ret = fraction_to_decimal.fractionToDecimal(fraction_to_decimal.numerator, fraction_to_decimal.denominator)
    # ret = array_mountain_array.validMountainArray(array_mountain_array.arr)
    print(ret)

    # ret = is_power_of_two.is_power(-16)
    # ret = find_disappeared_numbers.findDisappearedNumbers(find_disappeared_numbers.nums)

    # ret = largest_24_hour_time.largestTimeFromDigits(largest_24_hour_time.tym)

    # ret = reorder_logs.reorderLogFiles(reorder_logs.logs)

    # ret = generate_parenthesis.generateParenthesis(2)
    # ret = two_sum.twoSum2(two_sum.nums, two_sum.target)
    # ret = prison_after_n_days.prisonAfterNDays(prison_after_n_days.cells, 7)

    # ret = search_insert.searchInsert(search_insert.nums, search_insert.target)
    # ret = count_characters.countCharacters(count_characters.words, count_characters.chars)
    # ret = invalid_transactions.invalidTransactions(invalid_transactions.transaction_list)
    # ret = longest_palindromic_substring.longestPalindrome(longest_palindromic_substring.test)
    # ret = k_closest_points.kClosest(k_closest_points.points, k_closest_points.K)
    # ret = max_stock_profit.maxProfit(max_stock_profit.prices)

    # ret = array_product_except_self.productExceptSelf2(array_product_except_self.nums)
    # ret = is_one_bit_character.isOneBitCharacter(is_one_bit_character.bits)
    # ret = permute_numbers.permute(permute_numbers.nums)
    # ret = search_sorted_array_with_pivot.search(search_sorted_array_with_pivot.nums, search_sorted_array_with_pivot.target)
    # ret = find_common_restaurant.findRestaurant(find_common_restaurant.list1, find_common_restaurant.list2)
    # ret = num_pairs_divisible_by_60.numPairsDivisibleBy60(num_pairs_divisible_by_60.time)
    # ret = num_pairs_divisible_by_60.numPairsTest(num_pairs_divisible_by_60.time)
    # ret = subdomain_visits.subdomainVisits(subdomain_visits.domains)
    # ret = is_power_of_four.is_power_of_four(is_power_of_four.num)

    # ret = bulb_switch.bulb_switch(bulb_switch.num)
    # ret = duplicate_zeros.duplicateZeros(duplicate_zeros.nums)


if __name__ == '__main__':
    driver()


