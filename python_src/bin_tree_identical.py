"""
Given two binary trees, write a function to check if they are the same or not.

Two binary trees are considered the same if they are structurally identical and the nodes have the same value.

Example 1:

Input:     1         1
          / \       / \
         2   3     2   3

        [1,2,3],   [1,2,3]

Output: true
Example 2:

Input:     1         1
          /           \
         2             2

        [1,2],     [1,null,2]

Output: false
Example 3:

Input:     1         1
          / \       / \
         2   1     1   2

        [1,2,1],   [1,1,2]

Output: false
"""
from bin_tree_inorder_traversal import TreeNode


def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
    def same(n1, n2):
        if not n1 and not n2:
            return True
        elif n1 and n2:
            return n1.val == n2.val and same(n1.left, n2.left) and same(n1.right, n2.right)
        else:
            return False

    return same(p, q)