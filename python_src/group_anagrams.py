"""
Given an array of strings, group anagrams together.

Example:

Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
Output:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
Note:

All inputs will be in lowercase.
The order of your output does not matter.
"""
from typing import List


def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
    ret = []

    for i in range(len(strs)):
        if strs[i] not in ret:
            tmp = [strs[i]]
        else:
            continue
        for j in range(i + 1, len(strs)):
            if strs[j] not in ret:
                if is_anagram(strs[i], strs[j]):
                    tmp.append(strs[j])
            else:
                continue

    return ret


def is_anagram(self, str1: str, str2: str) -> bool:
    return sorted(str1) == sorted(str2)
