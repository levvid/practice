"""
We have a sequence of books: the i-th book has thickness books[i][0] and height books[i][1].

We want to place these books in order onto bookcase shelves that have total width shelf_width.

We choose some of the books to place on this shelf (such that the sum of their thickness is <= shelf_width), then
build another level of shelf of the bookcase so that the total height of the bookcase has increased by the maximum
height of the books we just put down.  We repeat this process until there are no more books to place.

Note again that at each step of the above process, the order of the books we place is the same order as the given
sequence of books.  For example, if we have an ordered list of 5 books, we might place the first and second book onto
the first shelf, the third book on the second shelf, and the fourth and fifth book on the last shelf.

Return the minimum possible height that the total bookshelf can be after placing shelves in this manner.

Example 1:

Input: books = [[1,1],[2,3],[2,3],[1,1],[1,1],[1,1],[1,2]], shelf_width = 4
Output: 6
Explanation:
The sum of the heights of the 3 shelves are 1 + 3 + 2 = 6.
Notice that book number 2 does not have to be on the first shelf.


Constraints:

1 <= books.length <= 1000
1 <= books[i][0] <= shelf_width <= 1000
1 <= books[i][1] <= 1000

Ref: https://leetcode.com/problems/filling-bookcase-shelves/discuss/360525/python-dp-solution-with-explanation
-similar-to-the-text-justification-problem
"""


from typing import List


def aux(self, books, i, dp, shelfWidth):
    if i == len(books) - 1:
        dp[i] = books[i][1]
        return books[i][1]
    if dp[i] != 2147483647:
        return dp[i]
    height, current_height, current_width = 2147483647, books[i][1], books[i][0]
    for j in range(i + 1, len(books)):
        height = min(height, current_height + self.aux(books, j, dp, shelfWidth))
        current_width += books[j][0]
        current_height = max(current_height, books[j][1])
        if current_width > shelfWidth:
            break
    if current_width <= shelfWidth:
        height = min(height, current_height)
    dp[i] = height
    return height


def minHeightShelves(self, books: List[List[int]], shelf_width: int) -> int:
    if len(books) == 0:
        return 0
    dp = [2147483647 for _ in range(len(books))]
    self.aux(books, 0, dp, shelf_width)
    return dp[0]