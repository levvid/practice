"""
You need to find the largest value in each row of a binary tree.

Example:
Input:

          1
         / \
        3   2
       / \   \
      5   3   9

Output: [1, 3, 9]
"""

from collections import defaultdict
from typing import List

from bin_tree_inorder_traversal import TreeNode


def largestValues(self, root: TreeNode) -> List[int]:
    def dfs(node, level):
        if node:
            self.node_dict[level].append(node.val)
            dfs(node.left, level +1)
            dfs(node.right, level +1)

    self.node_dict = defaultdict(list)

    dfs(root, 0)

    ret = []
    for key in self.node_dict.keys():
        ret.append(max(self.node_dict[key]))

    return ret