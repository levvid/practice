"""
Given a string containing only digits, restore it by returning all possible valid IP address combinations.

A valid IP address consists of exactly four integers (each integer is between 0 and 255) separated by single points.

Example:

Input: "25525511135"
Output: ["255.255.11.135", "255.255.111.35"]
"""
from typing import List


def restoreIpAddresses(self, s: str) -> List[str]:
    def is_valid(ip):
        # Splitting by "."
        ip = ip.split(".")

        # Checking for the corner cases
        for i in ip:
            if len(i) > 3 or int(i) < 0 or int(i) > 255:
                return False
            if len(i) > 1 and int(i) == 0:
                return False
            if len(i) > 1 and int(i) != 0 and i[0] == '0':
                return False
        return True

    # Function converts string to IP address
    def convert(s):
        sz = len(s)

        # Check for string size
        if sz > 12:
            return []
        snew = s
        l = []

        # Generating different combinations.
        for i in range(1, sz - 2):
            for j in range(i + 1, sz - 1):
                for k in range(j + 1, sz):
                    snew = snew[:k] + "." + snew[k:]
                    snew = snew[:j] + "." + snew[j:]
                    snew = snew[:i] + "." + snew[i:]

                    # Check for the validity of combination
                    if is_valid(snew):
                        l.append(snew)
                    snew = s
        return l

    return convert(s)