"""
Given N, consider a convex N-sided polygon with vertices labelled A[0], A[i], ..., A[N-1] in clockwise order.

Suppose you triangulate the polygon into N-2 triangles.  For each triangle, the value of that triangle is the product
of the labels of the vertices, and the total score of the triangulation is the sum of these values over all N-2 triangles in the triangulation.
Return the smallest possible total score that you can achieve with some triangulation of the polygon.

Example 1:

Input: [1,2,3]
Output: 6
Explanation: The polygon is already triangulated, and the score of the only triangle is 6.
Example 2:

Input: [3,7,4,5]
Output: 144
Explanation: There are two triangulations, with possible scores: 3*7*5 + 4*5*7 = 245, or 3*4*5 + 3*4*7 = 144.
The minimum score is 144.
Example 3:

Input: [1,3,1,4,1,5]
Output: 13
Explanation: The minimum score triangulation has score 1*1*3 + 1*1*4 + 1*1*5 + 1*1*1 = 13.


Note:

3 <= A.length <= 50
1 <= A[i] <= 100

"""

# DP - Top Down - With Recursion (Slower): (seven lines)
import math
from typing import List


class Solution:
    def minScoreTriangulation(self, A: List[int]) -> int:
        SP, LA = [[0]*50 for i in range(50)], len(A)
        def MinPoly(a,b):
            L, m = b - a + 1, math.inf;
            if SP[a][b] != 0 or L < 3: return SP[a][b]
            for i in range(a+1,b): m = min(m, A[a]*A[i]*A[b] + MinPoly(a,i) + MinPoly(i,b))
            SP[a][b] = m; return SP[a][b]
        return MinPoly(0, LA-1)


# DP - Bottom Up - Without Recursion (Faster): (six lines)

class Solution:
    def minScoreTriangulation(self, A: List[int]) -> int:
        SP, L = [[0]*50 for _ in range(50)], len(A)
        for i in range(2,L):
            for j in range(L-i):
                s, e, SP[s][e] = j, j + i, math.inf
                for k in range(s+1,e): SP[s][e] = min(SP[s][e], A[s]*A[k]*A[e] + SP[s][k] + SP[k][e])
        return SP[0][L-1]