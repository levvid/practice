"""
The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

Given two integers x and y, calculate the Hamming distance.

Note:
0 ≤ x, y < 231.

Example:

Input: x = 1, y = 4

Output: 2

Explanation:
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑

The above arrows point to positions where the corresponding bits are different.
"""


def hammingDistance(self, x: int, y: int) -> int:
    tmp = '{0:b}'.format(x ^ y)
    set_bits = [x for x in tmp if x == '1']

    return len(set_bits)
