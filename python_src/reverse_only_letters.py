"""
Given a string S, return the "reversed" string where all characters that are not a letter stay in the same place,
and all letters reverse their positions.



Example 1:

Input: "ab-cd"
Output: "dc-ba"
Example 2:

Input: "a-bC-dEf-ghIj"
Output: "j-Ih-gfE-dCba"
Example 3:

Input: "Test1ng-Leet=code-Q!"
Output: "Qedo1ct-eeLg=ntse-T!"


Note:

S.length <= 100
33 <= S[i].ASCIIcode <= 122
S doesn't contain \ or "
"""
from collections import defaultdict

s = "ab-acd"


def reverseOnlyLetters(S: str) -> str:
    i = 0
    j = len(S) - 1
    ret = [x for x in S]
    while i < j:
        if is_letter(S[i]):
            if is_letter(S[j]):
                ret[i] = S[j]
                ret[j] = S[i]
                i += 1
                j -= 1
            else:
                j -= 1
        else:
            i += 1
            if not is_letter(S[j]):
                j -= 1

    return ''.join(ret)


def is_letter(s: str):
    return 97 <= ord(s) <= 122
