"""
Given a set of distinct integers, nums, return all possible subsets (the power set).

Note: The solution set must not contain duplicate subsets.

Example:

Input: nums = [1,2,3]
Output:
[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]

Soln: https://www.geeksforgeeks.org/power-set/
"""
from typing import List


def subsets(nums: List[int]) -> List[List[int]]:
    x = len(nums)
    ret = []
    for i in range(1 << x):
        ret.append([nums[j] for j in range(x) if (i & (1 << j))])

    return ret


def printPowerSet(set, set_size):
    # set_size of power set of a set
    # with set_size n is (2**n -1)
    pow_set_size = int(pow(2, set_size))
    # Run from counter 000..0 to 111..1
    for counter in range(0, pow_set_size):
        for j in range(0, set_size):

            # Check if jth bit in the
            # counter is set If set then
            # print jth element from set
            if (counter & (1 << j)) > 0:
                print(set[j], end="")
        print("")
