# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation
# """
class NestedInteger:
   def isInteger(self) -> bool:
       """
       @return True if this NestedInteger holds a single integer, rather than a nested list.
       """
       pass

   def getInteger(self) -> int:
       """
       @return the single integer that this NestedInteger holds, if it holds a single integer
       Return None if this NestedInteger holds a nested list
       """
       pass

   def getList(self) -> [NestedInteger]:
       """
       @return the nested list that this NestedInteger holds, if it holds a nested list
       Return None if this NestedInteger holds a single integer
       """
       pass

class NestedIterator:
    def __init__(self, nestedList: [NestedInteger]):
        tmp = []

        for i in range(len(nestedList)):
            tmp.append(nestedList[i])

        self.nested_list = []
        for i in range(len(tmp)):
            self.append(self.nested_list, tmp[i])

        self.current_index = 0

    def append(self, lst, item):
        if item.isInteger():
            lst.append(item)
        else:
            tmp = item.getList()
            for i in range(len(tmp)):
                self.append(lst, tmp[i])

    def next(self) -> int:
        tmp = self.nested_list[self.current_index]
        self.current_index += 1
        return tmp

    def hasNext(self) -> bool:
        return self.current_index < len(self.nested_list)

# Your NestedIterator object will be instantiated and called as such:
# i, v = NestedIterator(nestedList), []
# while i.hasNext(): v.append(i.next())