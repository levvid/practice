"""Under a grammar given below, strings can represent a set of lowercase words.  Let's use R(expr) to denote the set
of words the expression represents.

Grammar can best be understood through simple examples:

Single letters represent a singleton set containing that word. R("a") = {"a"} R("w") = {"w"} When we take a comma
delimited list of 2 or more expressions, we take the union of possibilities. R("{a,b,c}") = {"a","b","c"} R("{{a,b},
{b,c}}") = {"a","b","c"} (notice the final set only contains each word at most once) When we concatenate two
expressions, we take the set of possible concatenations between two words where the first word comes from the first
expression and the second word comes from the second expression. R("{a,b}{c,d}") = {"ac","ad","bc","bd"} R("a{b,c}{d,
e}f{g,h}") = {"abdfg", "abdfh", "abefg", "abefh", "acdfg", "acdfh", "acefg", "acefh"} Formally, the 3 rules for our
grammar:

For every lowercase letter x, we have R(x) = {x} For expressions e_1, e_2, ... , e_k with k >= 2, we have R({e_1,e_2,
...}) = R(e_1) ∪ R(e_2) ∪ ... For expressions e_1 and e_2, we have R(e_1 + e_2) = {a + b for (a, b) in R(e_1) × R(
e_2)}, where + denotes concatenation, and × denotes the cartesian product. Given an expression representing a set of
words under the given grammar, return the sorted list of words that the expression represents.



Example 1:

Input: "{a,b}{c,{d,e}}"
Output: ["ac","ad","ae","bc","bd","be"]
Example 2:

Input: "{{a,z},a{b,c},{ab,z}}"
Output: ["a","ab","ac","z"]
Explanation: Each distinct word is written only once in the final answer.


Constraints:

1 <= expression.length <= 60
expression[i] consists of '{', '}', ','or lowercase English letters.
The given expression represents a set of words based on the grammar given in the description.
"""
from typing import List


def braceExpansionII(expression: str) -> List[str]:
    """
    Using DFS
    :param self:
    :param expression:
    :return:
    """
    def dfs(i):
        curr_lis, res, curr = [], [], ""
        while i < len(expression):
            if expression[i] == '{':
                i, lis = dfs(i + 1)
                curr_lis = [ch1 + ch2 for ch1 in curr_lis for ch2 in lis] or lis
            elif expression[i] == '}':
                return i, res + curr_lis
            elif expression[i] == ',':
                res.extend(curr_lis)
                curr_lis = []
            elif expression[i].isalpha():
                curr += expression[i]
                if i + 1 == len(expression) or not expression[i + 1].isalpha():
                    curr_lis = [ch + curr for ch in curr_lis] or [curr]
                    curr = ""
            i += 1
        return len(expression), res + curr_lis

    return sorted(set(dfs(0)[1]))


def braceExpansionII2(exp):
    """
    Using stacks
    :param self:
    :param exp:
    :return:
    """
    stk, curr_lis, res, curr = [], [], [], ""
    for i, ch in enumerate(exp):
        if ch == '{':
            stk.append(res)
            stk.append(curr_lis)
            res, curr_lis = [], []
        elif ch == '}':  # combine R(xy)=>prev_curr_lis and res+curr_lis
            curr_lis = res + curr_lis
            prev_lis = stk.pop()
            res = stk.pop()
            curr_lis = [ch1 + ch2 for ch1 in prev_lis for ch2 in curr_lis] or curr_lis
        elif ch == ',':
            res += curr_lis
            curr_lis = []
        elif ch.isalpha():
            curr += ch
            if i + 1 == len(exp) or not exp[i + 1].isalpha():
                curr_lis = [ch + curr for ch in curr_lis] or [curr]
                curr = ""
    return sorted(set(res + curr_lis))

