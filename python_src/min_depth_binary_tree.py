"""
Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its minimum depth = 2.


[1,2,null,3,null,4,null,5]
"""


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def minDepth(self, root: TreeNode) -> int:
    if not root:
        return 0
    a = self.depth(root, 1)

    print(a)
    return a


def depth(self, node: TreeNode, min_level: int) -> int:
    if node:
        if not node.left and not node.right:  # left node
            return min_level

        if not node.left and node.right:
            return self.depth(node.right, min_level + 1)
        if not node.right and node.left:
            return self.depth(node.left, min_level + 1)

        return min(self.depth(node.left, min_level + 1), self.depth(node.right, min_level + 1))