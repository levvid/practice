"""
Remove all elements from a linked list of integers that have value val.

Example:

Input:  1->2->6->3->4->5->6, val = 6
Output: 1->2->3->4->5
"""


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def removeElements(self, head: ListNode, val: int) -> ListNode:
    curr = head
    dummy = ListNode(0)
    prev = dummy
    while curr:
        if curr.val == val:
            prev.next = curr.next
            curr = curr.next
        else:
            if not dummy.next:
                dummy.next = curr
            prev = curr
            curr = curr.next

    return dummy.next


def removeElements2(self, head: ListNode, val: int) -> ListNode:
    dummy = ListNode(0)
    curr = head
    prev = dummy

    while curr:
        if curr.val == val:
            prev.next = curr.next
        else:
            prev.next = curr
            prev = curr
        curr = curr.next

    return dummy.next
