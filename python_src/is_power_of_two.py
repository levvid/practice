import math


def is_power(n: int) -> int:
    if n <= 0:
        return False
    log_2 = math.log2(n)
    return log_2.is_integer()