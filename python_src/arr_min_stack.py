"""
Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
getMin() -- Retrieve the minimum element in the stack.


Example 1:

Input
["MinStack","push","push","push","getMin","pop","top","getMin"]
[[],[-2],[0],[-3],[],[],[],[]]

Output
[null,null,null,null,-3,null,0,-2]

Explanation
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin(); // return -3
minStack.pop();
minStack.top();    // return 0
minStack.getMin(); // return -2


Constraints:

Methods pop, top and getMin operations will always be called on non-empty stacks.
"""


class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []
        self.min = float('inf')

    def push(self, x: int) -> None:
        if x < self.min:
            y = 2 * x - self.min
            self.stack.append(y)
            self.min = x
        else:
            self.stack.append(x)

    def pop(self) -> None:
        y = self.stack[-1]

        if y < self.min:
            ret = self.min
            self.min = 2 * self.min - y

        else:
            ret = y

        self.stack.pop()
        return y

    def top(self) -> int:
        y = self.stack[-1]

        if y < self.min:
            return self.min
        return y


    def getMin(self) -> int:
        return self.min


"""
2 stack implementation
"""
class MinStack_II:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []
        self.min = float('inf')
        self.min_stack = []


    def push(self, x: int) -> None:
        if x <= self.min:
            self.min_stack.append(self.min)
            self.min = x
        self.stack.append(x)

    def pop(self) -> None:
        x = self.stack[-1]

        if x == self.min:
            self.min = self.min_stack.pop()
        self.stack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        return self.min
        # return min(self.stack)




# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()