"""
Given an arbitrary ransom note string and another string containing letters from all the magazines,
write a function that will return true if the ransom note can be constructed from the magazines ; otherwise, it will return false.

Each letter in the magazine string can only be used once in your ransom note.

Note:
You may assume that both strings contain only lowercase letters.

canConstruct("a", "b") -> false
canConstruct("aa", "ab") -> false
canConstruct("aa", "aab") -> true
"""
from collections import defaultdict

ran = 'a'
mag = 'aa'


def canConstruct(self, ransomNote: str, magazine: str) -> bool:
    mag_dict = defaultdict(int)

    for s in magazine:
        mag_dict[s] += 1

    for r in ransomNote:
        if mag_dict[r] == 0:
            return False
        mag_dict[r] -= 1

    return True