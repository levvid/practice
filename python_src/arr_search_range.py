"""
Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
"""
from typing import List


def searchRange(self, nums: List[int], target: int) -> List[int]:
    ret = [-1, -1]
    found_target = False
    for i in range(len(nums)):
        if nums[i] == target:
            if not found_target:
                ret[0] = i
                if len(nums) == 1 or i == len(nums) - 1:
                    ret[1] = i
                found_target = True
            else:
                ret[1] = i
        else:
            if found_target and nums[ i -1] == target:
                ret[1] = i - 1
    return ret