from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)

    def sort_aphabet_helper(self, curr_letter, stack, visited):
        visited[curr_letter] = True

        for letter in self.graph[curr_letter]:
            if not visited[letter]:
                self.sort_aphabet_helper(letter, stack, visited)

        stack.append(curr_letter)

    def sort_alphabet(self):
        stack = []
        visited = defaultdict(bool)

        keys = list(self.graph.keys())
        for letter in keys:
            if not visited[letter]:
                self.sort_aphabet_helper(letter, stack, visited)

        return stack


def reconstruct_alphabet(words):
    result = []
    # Put your code here
    graph = Graph()

    for i in range(len(words) - 1):
        word1 = words[i]
        word2 = words[i+1]

        for j in range(min(len(word1), len(word2))):
            if word1[j] != word2[j]:
                graph.add_edge(word1[j], word2[j])
                break

    result = graph.sort_alphabet()[::-1]

    # Return the result, do not change the structure
    return result


if __name__ == '__main__':
    words = ['ccda',
             'ccb',
             'cd',
             'a',
             'ab',
             'd',
             ]
    a = reconstruct_alphabet(words)
    print(a)
