"""
Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.

Example:
Given nums = [-2, 0, 3, -5, 2, -1]

sumRange(0, 2) -> 1
sumRange(2, 5) -> -1
sumRange(0, 5) -> -3
Note:
You may assume that the array does not change.
There are many calls to sumRange function.

sum_acc = [-2, -2, 1, -4, -2, -3]


"""

from copy import copy
from typing import List

"""
Soln using DP
"""


class NumArray:
    def __init__(self, nums: List[int]):
        self.nums = nums
        self.sum_arr = copy(nums)

        for i in range(1, self.nums):
            self.sum_arr[i] += self.sum_arr[i - 1]

    def sumRange(self, i: int, j: int) -> int:
        if i == 0:
            return self.sum_arr[j]
        return self.sum_arr[j] - self.sum_arr[i-1]


"""
Less efficient soln
"""


class NumArray:
    def __init__(self, nums: List[int]):
        self.nums = nums

    def sumRange(self, i: int, j: int) -> int:
        tmp = 0
        for k in range(i, j + 1):
            tmp += self.nums[k]
        return tmp
