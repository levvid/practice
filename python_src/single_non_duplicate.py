"""
You are given a sorted array consisting of only integers where every element appears exactly twice,
except for one element which appears exactly once. Find this single element that appears only once.



Example 1:

Input: [1,1,2,3,3,4,4,8,8]
Output: 2
Example 2:

Input: [3,3,7,7,10,11,11]
Output: 10


Note: Your solution should run in O(log n) time and O(1) space.
"""
from collections import defaultdict
from typing import List

nums = [1, 1, 2, 3, 3, 4, 4, 8, 8]


def singleNonDuplicate(nums: List[int]) -> int:
    num_dict = defaultdict(int)
    for num in nums:
        num_dict[num] += 1

    for key in num_dict:
        if num_dict[key] != 2:
            return key

    # using XOR
    ret = nums[0]

    for i in range(1, len(nums)):
        ret ^= nums[i]

    return ret
