"""
Implement a basic calculator to evaluate a simple expression string.

The expression string contains only non-negative integers, +, -, *, / operators and empty spaces .
The integer division should truncate toward zero.

Example 1:

Input: "3/2+2"
Output: 7
Example 2:

Input: " 3/2 "
Output: 1
Example 3:

Input: " 3+5 / 2 "
Output: 5
Note:

You may assume that the given expression is always valid.
Do not use the eval built-in library function.
"""

TEST = "3+2*2"


def calculate(s: str) -> int:
    def add(a, b):
        return int(a) + int(b)

    def subtract(a, b):
        return int(a) - int(b)

    def multiply(a, b):
        return int(a) * int(b)

    def divide(a, b):
        return int(a) // int(b)

    num_stack = []
    oper_stack = []
    curr = 0

    i = 0

    while i < len(s):
        if s[i].isnumeric():
            num_stack.append(s[i])
        else:
            oper_stack.append(s[i])
            i += 1
            while s[i].isnumeric():
                pass