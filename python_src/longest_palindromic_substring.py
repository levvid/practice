"""

Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
"""

test = "ab"


def longestPalindrome(s: str) -> str:
    long_pal = ''
    max_len = 0

    if len(s) == 1:
        return s

    for i in range(len(s)):
        for j in range(i, len(s)):
            # print('i is {} j is {}'.format(i, j))
            curr = s[i:j + 1]
            print(curr)
            if is_palindrome(curr):
                if len(curr) > max_len:
                    max_len = len(curr)
                    long_pal = curr

    print('Long pal is: {}'.format(long_pal))
    return long_pal


def is_palindrome(s: str):
    return s[::-1] == s
