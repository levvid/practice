"""
S and T are strings composed of lowercase letters. In S, no letter occurs more than once.

S was sorted in some custom order previously. We want to permute the characters of T so that they match the order that
S was sorted. More specifically, if x occurs before y in S, then x should occur before y in the returned string.

Return any permutation of T (as a string) that satisfies this property.

Example :
Input:
S = "cba"
T = "abcd"
Output: "cbad"
Explanation:
"a", "b", "c" appear in S, so the order of "a", "b", "c" should be "c", "b", and "a".
Since "d" does not appear in S, it can be at any position in T. "dcba", "cdba", "cbda" are also valid outputs.


Note:

S has length at most 26, and no character is repeated in S.
T has length at most 200.
S and T consist of lowercase letters only.
"""


def customSortString(S: str, T: str) -> str:
    s = [c for c in S]
    t_sort = [c for c in T if c in S]
    t_not_sort = [c for c in T if c not in S]

    tmp = sorted(t_sort, key=lambda word: [s.index(c) for c in word])

    return ''.join(tmp) + ''.join(t_not_sort)
