"""

Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].

Example:

Input:  [1,2,3,4]
Output: [24,12,8,6]
Constraint: It's guaranteed that the product of the elements of any prefix or suffix of the array (including the whole array) fits in a 32 bit integer.

Note: Please solve it without division and in O(n).

Follow up:
Could you solve it with constant space complexity? (The output array does not count as extra space for the purpose of space complexity analysis.)
"""
from typing import List

nums = [1,0]
def productExceptSelf(nums: List[int]) -> List[int]:
    product = 1
    ret = []
    for num in nums:
        product *= num

    for num in nums:
        try:
            ret.append(int(product/num))
        except ZeroDivisionError:
            ret.append(0)

    return ret


def productExceptSelf2(nums: List[int]) -> List[int]:
    if len(nums) <= 1:
        return nums

    products = [None]*len(nums)
    print(products)
    for i in range(len(nums)):
        products[i] = multiply(nums[:i]) * multiply(nums[i + 1:])
    # print(products)
    return products


def multiply(nums: List[int]):
    prod = 1
    for num in nums:
        prod *= num

    return prod

"""
prod = nums[0...i-1] * nums[i+1...n]

prod[i] 
"""