"""There are 2N people a company is planning to interview. The cost of flying the i-th person to city A is costs[i][
0], and the cost of flying the i-th person to city B is costs[i][1].

Return the minimum cost to fly every person to a city such that exactly N people arrive in each city.



Example 1:

Input: [[10,20],[30,200],[400,50],[30,20]]
Output: 110
Explanation:
The first person goes to city A for a cost of 10.
The second person goes to city A for a cost of 30.
The third person goes to city B for a cost of 50.
The fourth person goes to city B for a cost of 20.

The total minimum cost is 10 + 30 + 50 + 20 = 110 to have half the people interviewing in each city.


Note:

1 <= costs.length <= 100
It is guaranteed that costs.length is even.
1 <= costs[i][0], costs[i][1] <= 1000
"""
from collections import defaultdict
from typing import List


def twoCitySchedCost(costs: List[List[int]]) -> int:
    diff_dict = defaultdict(int)

    for i in range(len(costs)):
        diff_dict[i] = abs(costs[i][1] - costs[i][0])

    count_a = 0
    count_b = 0
    tot_cost = 0

    for i in sorted(diff_dict, key=diff_dict.get, reverse=True):
        min_cost = min(costs[i])
        if costs[i].index(min_cost) == 0:
            count_a += 1
        else:
            count_b += 1

        if count_a > len(costs) / 2:
            tot_cost += costs[i][1]
        elif count_b > len(costs) / 2:
            tot_cost += costs[i][0]
        else:
            tot_cost += min_cost

    return tot_cost
