"""
Given the following quote by Alan Perlis

“Dealing with failure is easy: Work hard to improve. Success is also easy to
handle: You’ve solved the wrong problem. Work hard to improve.”

Considering only the alphabetical characters, consonants having the value of
their ASCII codes, and vowels having the inverse value of their ASCII codes,
what is the sum of the sentence?

Example:
Taking the word “iffy”, the ASCII code of “i” is 105, it’s inverse is -105.
The ASCII value of ‘f’ is 102. The ASCII y of “y” is 121. The sum of “iffy” =
220
"""


def epigram(sentence: str):
    sum_ = 0
    vowels = {'A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u'}

    for c in sentence:
        if c.isalpha():
            if c in vowels:
                sum_ += (-1 * ord(c))
            else:
                sum_ += ord(c)

    return sum_


if __name__ == '__main__':
    sentence = 'Dealing with failure is easy: Work hard to improve. Success is also easy to handle: You’ve solved ' \
               'the wrong problem. Work hard to improve.'

    print(epigram(sentence))
