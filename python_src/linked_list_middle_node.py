"""
Given a non-empty, singly linked list with head node head, return a middle node of linked list.

If there are two middle nodes, return the second middle node.



Example 1:
[1,2]
Input: [1,2,3,4,5]
Output: Node 3 from this list (Serialization: [3,4,5])
The returned node has value 3.  (The judge's serialization of this node is [3,4,5]).
Note that we returned a ListNode object ans, such that:
ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
Example 2:

Input: [1,2,3,4,5,6]
Output: Node 4 from this list (Serialization: [4,5,6])
Since the list has two middle nodes with values 3 and 4, we return the second one.


Note:

The number of nodes in the given list will be between 1 and 100.
"""
from collections import defaultdict

from add_two_numbers_linked_list import ListNode


def middleNode(head: ListNode) -> ListNode:
    curr = head
    if not curr:
        return curr

    i = 0
    node_dict = defaultdict(ListNode)

    while curr:
        node_dict[i] = curr
        curr = curr.next
        i += 1

    return node_dict[i // 2]

