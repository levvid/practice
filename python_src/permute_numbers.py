"""

Given a collection of distinct integers, return all possible permutations.

Example:

Input: [1,2,3]
Output:
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
"""

from typing import List

nums = [1,2]

def permute(nums: List[int]) -> List[List[int]]:
    permutations(nums)

    pass


def permutations(nums, step = 0):

    # if we've gotten to the end, print the permutation
    if step == len(nums):
        print(nums)

    # everything to the right of step has not been swapped yet
    for i in range(step, len(nums)):

        # copy the string (store as array)
        num_copy = [num for num in nums]
        print('Num copy is {}'.format(num_copy))
        # swap the current index with the step
        num_copy[step], num_copy[i] = num_copy[i], num_copy[step]

        # recurse on the portion of the string that has not been swapped yet (now it's index will begin with step + 1)
        permutations(num_copy, step + 1)