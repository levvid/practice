"""
You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water.

Grid cells are connected horizontally/vertically (not diagonally). The grid is completely surrounded by water,
and there is exactly one island (i.e., one or more connected land cells).

The island doesn't have "lakes" (water inside that isn't connected to the water around the island).
One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100.
Determine the perimeter of the island.

Example:

Input:
[[0,1,0,0],
 [1,1,1,0],
 [0,1,0,0],
 [1,1,0,0]]

Output: 16
Explanation: The perimeter is the 16 yellow stripes in the image below:
"""

from typing import List


def islandPerimeter(self, grid: List[List[int]]) -> int:
    perimeter = 0
    if len(grid) < 2 and len(grid[0]) < 2:
        if grid[0][0] == 1:
            return 4
        else:
            return 0

    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == 1:
                perimeter += self.get_num_sides(i, j, grid)

    return perimeter


def get_num_sides(self, i, j, grid):
    sides = 4
    length = len(grid)
    length_j = len(grid[0])
    if i == 0:
        if j == 0:
            if length > 1 and grid[i + 1][j] == 1:
                sides -= 1
            if length_j > 1 and grid[i][j + 1] == 1:
                sides -= 1
        elif j == length_j - 1:
            if grid[i][j - 1] == 1:
                sides -= 1
            if length > 1 and grid[i + 1][j] == 1:
                sides -= 1
        else:  # in btwn on 1st row
            if grid[i][j - 1] == 1:
                sides -= 1
            if length > 1 and grid[i + 1][j] == 1:
                sides -= 1
            if length_j > 1 and grid[i][j + 1] == 1:
                sides -= 1
    elif i == length - 1:
        if j == 0:
            if grid[i - 1][j] == 1:
                sides -= 1
            if length_j > 1 and grid[i][j + 1] == 1:
                sides -= 1
        elif j == length_j - 1:
            if grid[i - 1][j] == 1:
                sides -= 1
            if grid[i][j - 1] == 1:
                sides -= 1
        else:
            if grid[i - 1][j] == 1:
                sides -= 1
            if grid[i][j - 1] == 1:
                sides -= 1
            if grid[i][j + 1] == 1:
                sides -= 1
    elif 0 < i < length - 1:
        if j == 0:
            if grid[i - 1][j] == 1:
                sides -= 1
            if grid[i + 1][j] == 1:
                sides -= 1
            if length_j > 1 and grid[i][j + 1] == 1:
                sides -= 1
        elif j == length_j - 1:
            if grid[i - 1][j] == 1:
                sides -= 1
            if grid[i][j - 1] == 1:
                sides -= 1
            if grid[i + 1][j] == 1:
                sides -= 1
        else:
            if grid[i - 1][j] == 1:
                sides -= 1
            if grid[i][j - 1] == 1:
                sides -= 1
            if grid[i + 1][j] == 1:
                sides -= 1
            if length_j > 1 and grid[i][j + 1] == 1:
                sides -= 1
    print('Grid ({},{}) sides: {}'.format(i, j, sides))
    return sides


