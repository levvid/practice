"""
Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3


But the following [1,2,2,null,3,null,3] is not:
    1
   / \
  2   2
   \   \
   3    3


Follow up: Solve it both recursively and iteratively.
"""
from copy import deepcopy

from bin_tree_delete_nodes import TreeNode


def isSymmetric(self, root: TreeNode) -> bool:
    def flip_tree(node):
        if node:
            tmp = node.left
            node.left = node.right
            node.right = tmp
            flip_tree(node.left)
            flip_tree(node.right)

    def same(n1, n2):
        if not n1 and not n2:
            return True
        elif n1 and n2:
            return n1.val == n2.val and same(n1.left, n2.left) and same(n1.right, n2.right)
        else:
            return False

    dummy = deepcopy(root)
    flip_tree(root)

    return same(dummy, root)
