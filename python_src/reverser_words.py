"""
Given a string, you need to reverse the order of characters in each word within a sentence while still
preserving whitespace and initial word order.

Example 1:
Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"
Note: In the string, each word is separated by single space and there will not be any extra space in the string.
"""

input = "Let's take LeetCode contest"

def reverseWords(s: str) -> str:
    s_list = s.split(' ')
    ret = [''] * len(s_list)
    for i in range(len(s_list)):
        ret[i] = s_list[i][::-1]

    print(ret)
    return ' '.join(ret)

