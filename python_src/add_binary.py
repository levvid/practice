"""
Given two binary strings, return their sum (also a binary string).

The input strings are both non-empty and contains only characters 1 or 0.

Example 1:

Input: a = "11", b = "1"
Output: "100"
Example 2:

Input: a = "1010", b = "1011"
Output: "10101"


Constraints:

Each string consists only of '0' or '1' characters.
1 <= a.length, b.length <= 10^4
Each string is either "0" or doesn't contain any leading zero.
1, 1, 1
1, 1, 0
0,
"""


def addBinary(a: str, b: str) -> str:
    def add_helper(f: str, s: str, c: str) -> (str, str):  # sum, carry
        lst = [f, s, c]
        one_count = lst.count('1')

        if one_count == 1:
            return '1', '0'
        elif one_count == 2:
            return '0', '1'
        elif one_count == 3:
            return '1', '1'
        else:
            return '0', '0'

    carry = '0'
    ret = ''

    a = a[::-1]
    b = b[::-1]

    if len(a) > len(b):
        first = a
        second = b
    else:
        first = b
        second = a

    for i in range(len(first)):
        try:
            res, carry = add_helper(first[i], second[i], carry)
            ret += res
        except IndexError:
            res, carry = add_helper(first[i], '0', carry)
            ret += res

    if carry != '0':
        ret += carry

    return ret[::-1]




