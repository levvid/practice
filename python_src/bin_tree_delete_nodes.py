"""
Given the root of a binary tree, each node in the tree has a distinct value.

After deleting all nodes with a value in to_delete, we are left with a forest (a disjoint union of trees).

Return the roots of the trees in the remaining forest.  You may return the result in any order.



Example 1:



Input: root = [1,2,3,4,5,6,7], to_delete = [3,5]
Output: [[1,2,null,4],[6],[7]]


Constraints:

The number of nodes in the given tree is at most 1000.
Each node has a distinct value between 1 and 1000.
to_delete.length <= 1000
to_delete contains distinct values between 1 and 1000.
"""
# Definition for a binary tree node.
from typing import List


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:
        ret = []

        dummy = TreeNode(0)
        dummy.left = root

        if root.val not in to_delete:
            ret.append(root)

        self.del_helper(root, to_delete, ret, dummy)
        print(ret)
        return ret

    def del_helper(self, node, to_delete, ret, parent):
        if node:
            if node.val in to_delete:
                if parent.left == node:
                    parent.left = None
                else:
                    parent.right = None
                if node.left and node.left.val not in to_delete:
                    ret.append(node.left)
                if node.right and node.right.val not in to_delete:
                    ret.append(node.right)
                # parent = node
            self.del_helper(node.left, to_delete, ret, node)
            self.del_helper(node.right, to_delete, ret, node)


