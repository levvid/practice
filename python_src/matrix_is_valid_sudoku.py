"""
Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.

A partially filled sudoku which is valid.

The Sudoku board could be partially filled, where empty cells are filled with the character '.'.

Example 1:

Input:
[
  ["5","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: true
Example 2:

Input:
[
  ["8","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: false
Explanation: Same as Example 1, except with the 5 in the top left corner being
    modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules.
The given board contain only digits 1-9 and the character '.'.
The given board size is always 9x9.
"""
from typing import List


def is_valid_square(board, i, j):
    i_ = i + 3
    j_ = j + 3
    check = [False for i in range(11)]
    for i in range(i, i_):
        for j in range(j, j_):
            num = board[i][j]
            if num != '.':
                if check[int(num)]:
                    return False
                check[int(num)] = True

    return True


def is_valid_row_cols(board):
    for i in range(len(board)):
        check = [False for i in range(10)]
        for j in range(len(board)):
            num = board[i][j]
            if num != '.':
                if check[int(num)]:
                    return False
                check[int(num)] = True

    for i in range(len(board)):
        check = [False for i in range(10)]
        for j in range(len(board)):
            num = board[j][i]
            if num != '.':
                if check[int(num)]:
                    return False
                check[int(num)] = True

    return True


def isValidSudoku(board: List[List[str]]) -> bool:
    if is_valid_row_cols(board):
        i = 0
        j = 0
        while i <= 6 and j <= 6:
            ret = is_valid_square(board, i, j)
            if not ret:
                return False
            i += 3
            j += 3
    else:
        return False

    return True


def isValidSudoku(self, board: List[List[str]]) -> bool:
    def isValid(x, y, tmp):
        for i in range(9):
            if board[i][y] == tmp:
                return False
        for i in range(9):
            if board[x][i] == tmp:
                return False
        for i in range(3):
            for j in range(3):
                if board[(x // 3) * 3 + i][(y // 3) * 3 + j] == tmp:
                    return False
        return True

    for i in range(9):
        for j in range(9):
            if board[i][j] == '.':
                continue
            tmp = board[i][j]
            board[i][j] = 'D'
            if not isValid(i, j, tmp):
                return False
            else:
                board[i][j] = tmp
    return True
