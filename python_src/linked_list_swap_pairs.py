"""
Given a linked list, swap every two adjacent nodes and return its head.

You may not modify the values in the list's nodes, only nodes itself may be changed.

Example:

Given 1->2->3->4, you should return the list as 2->1->4->3.
"""


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def swapPairs(head: ListNode) -> ListNode:
    dummy = ListNode(0)

    curr = head
    prev = dummy

    if not head or not head.next:
        return head

    while curr:
        tmp = curr.next
        if tmp:
            first = curr
            second = curr.next
            print('Second {}'.format(second.val))
            # 1 2 3
            if second:
                first.next = second.next
                second.next = first
                prev.next = second
                prev = first
                curr = first
                if not dummy.next:
                    dummy.next = second
        curr = curr.next

    return dummy.next
