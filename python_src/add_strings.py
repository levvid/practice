"""
Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

Note:

The length of both num1 and num2 is < 5100.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
"""


class Solution:
    def addStrings(self, num1: str, num2: str) -> str:

        num1 = num1[::-1]
        num2 = num2[::-1]

        if len(num1) >= len(num2):
            tmp1 = num1
            tmp2 = num2
        else:
            tmp1 = num2
            tmp2 = num1

        carry = 0
        ret = ''
        for i in range(len(tmp1)):
            tmp = 0
            if i < len(tmp2):
                tmp += int(tmp2[i])
            tmp += int(tmp1[i]) + carry

            if tmp > 9:
                carry = tmp // 10
                tmp = tmp % 10
            else:
                carry = 0
            ret = str(tmp) + ret
        if carry != 0:
            ret = str(carry) + ret

        return ret