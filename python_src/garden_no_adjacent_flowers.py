"""
You have N gardens, labelled 1 to N.  In each garden, you want to plant one of 4 types of flowers.
paths[i] = [x, y] describes the existence of a bidirectional path from garden x to garden y.
Also, there is no garden that has more than 3 paths coming into or leaving it.
Your task is to choose a flower type for each garden such that, for any two gardens connected by a path,
they have different types of flowers.
Return any such a choice as an array answer, where answer[i] is the type of flower planted in the (i+1)-th garden.
The flower types are denoted 1, 2, 3, or 4.  It is guaranteed an answer exists.

Example 1:

Input: N = 3, paths = [[1,2],[2,3],[3,1]]
Output: [1,2,3]
Example 2:

Input: N = 4, paths = [[1,2],[3,4]]
Output: [1,2,1,2]
Example 3:

Input: N = 4, paths = [[1,2],[2,3],[3,4],[4,1],[1,3],[2,4]]
Output: [1,2,3,4]


Note:

1 <= N <= 10000
0 <= paths.size <= 20000
No garden has 4 or more paths coming into or leaving it.
It is guaranteed an answer exists.


"""
from collections import defaultdict
from typing import List


class Solution:
    def gardenNoAdj(self, N: int, paths: List[List[int]]) -> List[int]:
        dic = defaultdict(list)

        for path in paths:
            x0, y0 =  path[0], path[1]
            dic[x0].append(y0)
            dic[y0].append(x0)

        # print dic
        res = [-1 for _ in range(N + 1)]
        print(len(dic.keys()))
        for i in dic.keys():
            neighbours = dic[i]
            used = [0 for _ in range(5)]
            for neighbour in neighbours:
                if res[neighbour] != -1:
                    used[res[neighbour]] = 1
            if sum(used) == 0:
                res[i] = 1
            else:
                for j in range(1, 5):
                    if used[j] == 0:
                        res[i] = j
                        break
        # print res
        for i, x in enumerate(res):
            if x == -1:
                res[i] = 1
        return res[1:]
