"""
Given two strings str1 and str2, return the shortest string that has both str1 and str2 as subsequences.
If multiple answers exist, you may return any of them.

(A string S is a subsequence of string T if deleting some number of characters from T (possibly 0, and the characters
are chosen anywhere from T) results in the string S.)

Example 1:

Input: str1 = "abac", str2 = "cab"
Output: "cabac"
Explanation:
str1 = "abac" is a subsequence of "cabac" because we can delete the first "c".
str2 = "cab" is a subsequence of "cabac" because we can delete the last "ac".
The answer provided is the shortest such string that satisfies these properties.

Note:

1 <= str1.length, str2.length <= 1000
str1 and str2 consist of lowercase English letters.
"""


def shortest_common_supersequence(str1: str, str2: str) -> str:
    m, n = len(str1), len(str2)
    dp = [list(range(n + 1))] + [[i] + [0] * n for i in range(1, m + 1)]
    for i in range(m):
        for j in range(n):
            dp[i + 1][j + 1] = 1 + (dp[i][j] if str1[i] == str2[j] else min(dp[i + 1][j], dp[i][j + 1]))
    i, j, scs = m, n, ""
    while i * j:
        f1, f2 = str1[i - 1] == str2[j - 1], dp[i - 1][j] < dp[i][j - 1]
        f1, f2 = int(f1 or f2), int(f1 or not f2)
        scs = str1[i - 1] * f1 + str2[j - 1] * (1 - f1) + scs
        i, j = i - f1, j - f2
    return str1[:i] + str2[:j] + scs
