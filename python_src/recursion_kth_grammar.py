"""
On the first row, we write a 0. Now in every subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each occurrence of 1 with 10.

Given row N and index K, return the K-th indexed symbol in row N. (The values of K are 1-indexed.) (1 indexed).

Examples:
Input: N = 1, K = 1
Output: 0

Input: N = 2, K = 1
Output: 0

Input: N = 2, K = 2
Output: 1

Input: N = 4, K = 5
Output: 1

Explanation:
row 1: 0
row 2: 01
row 3: 0110
row 4: 01101001
Note:

N will be an integer in the range [1, 30].
K will be an integer in the range [1, 2^(N-1)].


The patterns of changes are the below.

(n-1)-th      0                    1
            /   \                /   \
n-th       0     1              1     0
        [2p-1]  [2p]         [2p-1]  [2p]
Note

if k is the form of 2p-1 or odd number, the k-th value of the n-th row equals the value of the (n-1)-th row deriving
them if k is the form of 2p or even number, the k-th value of the n-th row equals the inversion of the value of the (
n-1)-th
row deriving them (0 and 1 respectively becomes 1 and 0)

"""


def kth_grammar(self, N: int, K: int) -> int:
    if N == 1:
        return 0
    elif N == 2:
        return 0 if K == 1 else 1

    v = self.kthGrammar(N - 1, (K + 1) // 2)
    if v == 0:
        if K % 2 == 0:
            return 1
        else:
            return 0
    else:
        if K % 2 == 0:
            return 0
        else:
            return 1


def kth_grammar(N: int, K: int) -> int:
    def make_changes(ret: str) -> str:
        tmp = ''
        for s in ret:
            if s == '0':
                tmp += '01'
            else:
                tmp += '10'

        return tmp

    ret = '0'
    for i in range(1, N):
        ret = make_changes(ret)

    print('Row: {} STR: {}'.format(N, ret))
    return ret[K-1]








