"""
Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.

Example:

You may serialize the following tree:

    1
   / \
  2   3
     / \
    4   5

as "[1,2,3,null,null,4,5]"
Clarification: The above format is the same as how LeetCode serializes a binary tree. You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.

Note: Do not use class member/global/static variables to store states. Your serialize and deserialize algorithms should be stateless.
"""


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import collections

from bin_tree_delete_nodes import TreeNode


class Codec:
    def buildNode(self, val):
        return None if val == '#' else TreeNode(int(val))

    def serialize(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """

        queue = [root]
        for node in queue:
            if not node:
                continue
            queue += [node.left, node.right]

        return ':'.join(
            map(lambda item: str(item.val) if item else '#', queue))


    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """
        parts = data.split(':')
        root = self.buildNode(parts[0])
        queue, i = collections.deque([root]), 1

        while queue:
            node = queue.popleft()
            if node:
                node.left, node.right = map(
                    self.buildNode, (parts[i], parts[i + 1]))
                queue.append(node.left)
                queue.append(node.right)
                i += 2
        return root




# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))