#!/usr/bin/env python3
"""
                                        Stable SWE Design Challenge
                                        ---------------------------
Background:​ Stable manages a distributed network of DC fast charging sites. Each charger is
equipped with a robot that can autonomously plug the charger cable into an electric vehicle
parked in front of it. The robot is running proprietary software that must be kept up-to-date to
correct any defects that may have shipped, and add new features.
Objective:​ Build a tool for validating update package set ​update manifests​.
Data:​ We define an ​update manifest​ as a plaintext file containing the set of packages to install
as well as any runtime dependencies each package may have. In this problem, an ​update
manifest will be a JSON file of the form:
{
    “pkgs”: {
        “package-name”: {
            “required”: [
            ],
            “exported”: [
            ]
        },
    ...
    }
}

Where:
    - pkgs is a map of package description objects keyed by the package name (a String)
    - Each package description contains:
        - required (List of String): This package’s runtime dependencies
        - exports (List of String): Runtime variables exported by this package

An ​update manifest​ is considered valid if all of the following constraints are satisfied:
    - Every exported variable is exported by exactly one package
    - Each package’s ​required​ variable set can be satisfied by the ​exports​ of the union of all of the other packages
    in the manifest
    - An order of installation can be determined such that when a package is installed all packages that export its ​
    required​ variables are already installed

Interface:​ Your program should be a command-line tool that takes a JSON ​update manifest​ as a positional argument
such that it can be executed as follows:
    $ update_validater update_manifest.json

Expected Output:​ If the ​update manifest​ is valid your program should print a valid installation
order as a comma-separated list. For example:
    $ update_validater update_manifest.json
    pkgD,pkgB,pkgE,pkgA
If the ​update manifest​ is invalid your program should print ​invalid​ as follows:
    $ update_validater update_manifest.json
    invalid

Deliverable:​ Please provide your full solution code and any test ​update manifest​ JSON files
that you may have produced in a single zip archive or tarball. In addition please include a
README that explains your solution design, your testing strategy, how to install any
dependencies that your code needs to run, any assumptions you have made, and any
comments you would like to share with the team.
You are free to use any language. However, note that you should avoid using libraries or
functions that fully solve the problem as we want to assess your implementation.
We will provide you with two example ​update manifest​ JSONs - one which is valid and one
which is invalid. Note that the invalid example will not necessarily include all failure cases.
"""
import sys
from collections import defaultdict
from typing import List
from parse_json import parse_json_file


class PackageDescription(object):
    def __init__(self):
        self.required_packages = defaultdict(list)  # map from package_name -> [required_packages]
        self.exported_variables = defaultdict(str)  # map from exported_variable -> package_name
        self.order_of_installation = []  # list to hold the order of installation of packages

    def add_required_packages(self, package: str, required_packages: List) -> None:
        """
        Adds required_packages to the list of required packages for package
        :param package:
        :param required_packages: list of required packages for package
        :return: None
        """
        self.required_packages[package] += required_packages

    def add_exported_variables(self, package: str, exported_variables: List) -> bool:
        """
        Maps each exported variable in exported_variables to package and returns false if more than one package export
        the same exported variable and true otherwise
        :param exported_variables: exported variables for this package
        :param package: package name
        :return: false if more than one package export the same exported variable and true otherwise
        """
        # returns
        for exported_variable in exported_variables:
            if exported_variable in self.exported_variables:
                return False
            else:
                self.exported_variables[exported_variable] = package
        return True

    def all_required_packages_exported(self) -> bool:
        """
        Checks whether all the required packages have been exported and returns True if they have been, false otherwise
        :return: True if all required packages are exported
        """
        for package in self.required_packages:
            if not set(self.required_packages[package]).issubset(set(self.exported_variables.keys())):
                return False
        return True

    def all_required_packages_installed(self, package: str) -> bool:
        """
        Checks to make sure that all this package's dependencies have already been installed first.
        Returns true if all required packages have been installed first, false otherwise.
        :param package:
        :return: True if all required packages have been installed, false otherwise
        """
        return set([self.exported_variables[package_name] for package_name in self.required_packages[
            package]]).issubset(set(self.order_of_installation))

    def find_package_order_helper(self, package: str, visited: dict, recursion_stack: dict) -> bool:
        """
        Uses depth first search to check each available package and it's subsequent required packages to find the
        order of installation for all the packages. Also checks to make sure that there are no cyclic dependencies in all the package
        requirements and as a final sanity check, that all a package's dependencies are installed first before the package is installed
        :param package:
        :param visited:
        :param recursion_stack:
        :return: true if there are cyclic dependencies and all package's dependencies are installed first before the
        package is installed, false otherwise
        """
        visited[package] = True
        recursion_stack[package] = True

        # loop through all the packages and recur for all packages that are required by this package
        for required_package in self.required_packages[package]:
            required_package_name = self.exported_variables[required_package]
            if not visited[required_package_name]:  # if required_package_name has not been visited already
                # check whether there are cyclic dependencies
                if self.find_package_order_helper(required_package_name, visited, recursion_stack):
                    return True
            elif recursion_stack[required_package_name]:  # if a visited required package is still in the recursion
                # stack, then a cyclic dependency exists
                return True

        # sanity check to make sure all the packages dependencies have been installed first
        if self.all_required_packages_installed(package):
            # after fulfilling all the packages dependencies, add it to the order_of_installation
            self.order_of_installation.append(package)
        else:
            print('invalid')
            return False
        # remove package from recursion stack
        recursion_stack[package] = False
        return False

    def find_package_order(self) -> List:
        """
        Finds the order of installation for all packages while checking that all the conditions are satisfied. If
        not, prints invalid
        :return: None (prints the order of package installation if all conditions are met or 'invalid' if some
        condition is not satisfied)
        """

        # Mark all the vertices as not visited
        visited = defaultdict(bool)  # boolean dict to hold status of whether package has been checked or not
        recursion_stack = defaultdict(bool)  # boolean dict to hold recursion stack status - will help to check whether
        # there are cyclic dependencies

        # traverse all packages in package graph
        for package in self.required_packages:
            if not visited[package]:
                cyclic_dependencies = self.find_package_order_helper(package, visited, recursion_stack)
                if not cyclic_dependencies:
                    # no cyclic dependencies
                    continue
                else:
                    # there are cyclic dependencies, ergo update manifest if invalid
                    print('invalid')
                    return

        print(','.join(self.order_of_installation))


def validate_output(path):
    """
    Validates a JSON file present at path.
    :param path: file path for JSON file to be validated
    :return:
    """
    package_description = PackageDescription()

    package_details = parse_json_file(path)

    if not package_details:  # errors encountered during parsing of JSON file
        return

    for package, required_packages, exported_variables in package_details:
        package_description.add_required_packages(package, required_packages)
        is_valid = package_description.add_exported_variables(package, exported_variables)
        if not is_valid:  # more than one package exports a package with the same name
            print('invalid')
            return

    # check that all the required packages have been exported
    all_required_packages_exported = package_description.all_required_packages_exported()
    if not all_required_packages_exported:  # not all the required packages have been exported
        print('invalid')
        return

    # find the package installation order
    package_description.find_package_order()


if __name__ == '__main__':
    arguments = sys.argv
    if len(arguments) < 2:
        print('Please enter the file_path using the format below:')
        print('update_validator <path_to_json_file/update_manifest.json>')
    else:
        file_path = sys.argv[1]
        validate_output(file_path)
