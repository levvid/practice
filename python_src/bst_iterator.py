"""
Implement an iterator over a binary search tree (BST). Your iterator will be initialized with the root node of a BST.

Calling next() will return the next smallest number in the BST.



Example:



BSTIterator iterator = new BSTIterator(root);
iterator.next();    // return 3
iterator.next();    // return 7
iterator.hasNext(); // return true
iterator.next();    // return 9
iterator.hasNext(); // return true
iterator.next();    // return 15
iterator.hasNext(); // return true
iterator.next();    // return 20
iterator.hasNext(); // return false


Note:

next() and hasNext() should run in average O(1) time and uses O(h) memory, where h is the height of the tree.
You may assume that next() call will always be valid, that is, there will be at least a next smallest number in the BST
when next() is called.
"""
from bin_tree_delete_nodes import TreeNode


class BSTIterator:

    def __init__(self, root: TreeNode):
        self.stack = []
        curr = root
        while curr:
            self.stack.append(curr)
            curr = curr.left

    def next(self) -> int:
        """
        @return the next smallest number
        """
        next_node = self.stack.pop()

        curr = next_node.right

        while curr:
            self.stack.append(curr)
            curr = curr.left
        return next_node.val

    def hasNext(self) -> bool:
        """
        @return whether we have a next smallest number
        """
        return len(self.stack) > 0


class BSTIterator:

    def __init__(self, root: TreeNode):
        self.tree = []

        def dfs(node):
            if node:
                self.tree.append(node.val)
                dfs(node.left)
                dfs(node.right)

        dfs(root)
        self.tree = sorted(self.tree)

    def next(self) -> int:
        """
        @return the next smallest number
        """
        tmp = self.tree[0]
        self.tree = self.tree[1:]
        return tmp

    def hasNext(self) -> bool:
        """
        @return whether we have a next smallest number
        """
        return len(self.tree) > 0


class BSTIterator:

    def __init__(self, root: TreeNode):
        self.stack = []

        tmp = root

        while tmp:
            self.stack.append(tmp)
            tmp = tmp.left

    def next(self) -> int:
        """
        @return the next smallest number
        """
        next_node = self.stack.pop()
        current = next_node.right
        while current != None:
            self.stack.append(current)
            current = current.left
        return next_node.val

    def hasNext(self) -> bool:
        """
        @return whether we have a next smallest number
        """
        return len(self.stack) > 0
