"""
Given a fixed length array arr of integers, duplicate each occurrence of zero, shifting the remaining elements to the right.

Note that elements beyond the length of the original array are not written.

Do the above modifications to the input array in place, do not return anything from your function.



Example 1:

Input: [1,0,2,3,0,4,5,0]
Output: null
Explanation: After calling your function, the input array is modified to: [1,0,0,2,3,0,0,4]
Example 2:

Input: [1,2,3]
Output: null
Explanation: After calling your function, the input array is modified to: [1,2,3]

Note:

1 <= arr.length <= 10000
0 <= arr[i] <= 9
"""
from collections import defaultdict
from typing import List


nums = [1,0,2,3,0,4,5,0]

def duplicateZeros(arr: List[int]) -> None:
    tmp = arr
    arr_dict = defaultdict(int)
    i = 0
    j = 0
    next = None
    while i < len(arr) - 1:
        print('This is tmp {}'.format(tmp))
        if tmp[i] == 0:
            next = 0
        else:
            next = tmp[i+1]
        arr[i] = next
        i += 1

    print(arr)




