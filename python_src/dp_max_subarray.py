"""
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest
sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach,
which is more subtle.

[1, 2, 3, 4]

max(sum+nums[i], max_sub(nums[i:])
"""

from typing import List

test = [1, 2, 3, 4]
test = [-2, 1, -3, 4, -1, 2, 1, -5, 4]


def max_subarray(nums: List[int]) -> int:
    current = nums[0]
    global_current = nums[0]

    for i in range(1, len(nums)):
        current = max(nums[i], nums[i] + current)

        if current > global_current:
            global_current = current

    return global_current




