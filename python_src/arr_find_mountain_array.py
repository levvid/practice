"""
(This problem is an interactive problem.)

You may recall that an array A is a mountain array if and only if:

A.length >= 3
There exists some i with 0 < i < A.length - 1 such that:
A[0] < A[1] < ... A[i-1] < A[i]
A[i] > A[i+1] > ... > A[A.length - 1]
Given a mountain array mountainArr, return the minimum index such that mountainArr.get(index) == target.
If such an index doesn't exist, return -1.

You can't access the mountain array directly.  You may only access the array using a MountainArray interface:

MountainArray.get(k) returns the element of the array at index k (0-indexed).
MountainArray.length() returns the length of the array.
Submissions making more than 100 calls to MountainArray.get will be judged Wrong Answer.
Also, any solutions that attempt to circumvent the judge will result in disqualification.



Example 1:

Input: array = [1,2,3,4,5,3,1], target = 3
Output: 2
Explanation: 3 exists in the array, at index=2 and index=5. Return the minimum index, which is 2.
Example 2:

Input: array = [0,1,2,4,2,1], target = 3
Output: -1
Explanation: 3 does not exist in the array, so we return -1.

[1,2,3,4,3,2,1]

Constraints:

3 <= mountain_arr.length() <= 10000
0 <= target <= 10^9
0 <= mountain_arr.get(index) <= 10^9
"""


# This is MountainArray's API interface.
# You should not implement it, or speculate about its implementation
# """
class MountainArray:
    def __init__(self, arr):
        self.arr = arr
        self.count = 0

    def get(self, index: int) -> int:
        self.count += 1
        return self.arr[index]

    def length(self) -> int:
        return len(self.arr)


def findInMountainArray(target: int, mountain_arr: MountainArray) -> int:
    def find_peak_index(low, high):
        mid = low + (high-low)//2
        before = mountain_arr.get(mid-1)
        curr = mountain_arr.get(mid)
        after = mountain_arr.get(mid+1)
        if before < curr and curr > after:
            return mid
        elif before < curr < after:
            return find_peak_index(mid, high)
        elif before > curr > after:
            return find_peak_index(low, mid)

    def find_left(low, high):
        mid = low + (high-low)//2
        if low <= high:
            curr = mountain_arr.get(mid)

            if curr == target:
                return mid
            elif curr > target:
                return find_left(low, mid-1)
            else:
                return find_left(mid+1, high)
        else:
            return -1

    def find_right(low, high):
        if low <= high:
            mid = low + (high - low) // 2
            
            curr = mountain_arr.get(mid)

            if curr == target:
                return mid
            elif curr > target:
                return find_right(mid+1, high)
            else:
                return find_right(low, mid-1)
        else:
            return -1

    peak_idx = find_peak_index(0, mountain_arr.length() - 1)

    curr = mountain_arr.get(peak_idx)

    if curr != target:
        curr = find_left(0, peak_idx-1)
        if curr == -1:
            curr = find_right(peak_idx+1, mountain_arr.length())


    return curr






TEST = [1, 2, 3, 4, 5, 3, 1]
# TEST = [0, 1, 2, 4, 2, 1]
# TEST = [10, 5, 3, 2, 0]
# TEST = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
#         31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
#         59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86,
#         87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89,
#         88, 87, 86, 85, 84, 83, 82]

KEY = 3

if __name__ == '__main__':
    arr = MountainArray(TEST)
    r = findInMountainArray(KEY, arr)
    print('Made {} calls to get'.format(arr.count))

    print('Found target {}'.format(r))
