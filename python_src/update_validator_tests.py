import unittest
from io import StringIO
from unittest.mock import patch

from update_validator import validate_output

JSON_FILES_ROOT_DIRECTORY = '/home/gibson/Documents/docs/stable_auto/json/'

TEST_FILES = {
    'valid_json': 'VALID-sample.json',
    'empty_file': 'empty.json',
    'cyclic_dependency_1': 'cyclic_1.json',
    'cyclic_dependency_2': 'cyclic_2.json',
    'multiple_exported_variables': 'multiple_exported_variables.json',
    'unsatisfied_variable_set': 'unsatisfied_variable_set.json',
    'invalid_json': 'invalid_json.json'
}


def get_file_path(file_name: str):
    """
    Helper function to get the full file path
    :param file_name:
    :return:
    """
    return JSON_FILES_ROOT_DIRECTORY + TEST_FILES[file_name]


class TestUpdateValidator(unittest.TestCase):
    def test_valid_json_file(self):
        """
        Tests a valid json file
        :return:
        """
        expected_output = 'package-3,package-2,package-1'
        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('valid_json'))
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)

    def test_empty_file(self):
        """
        Tests an empty json file
        :return:
        """
        expected_output = 'invalid'
        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('empty_file'))
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)

    def test_cyclic_dependencies(self):
        """
        Tests for cyclic dependencies to avoid a dead lock
        :return:
        """
        expected_output = 'invalid'
        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('cyclic_dependency_1'))  # 1 -> 2, 2 -> 1
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)

        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('cyclic_dependency_2'))  # 1 -> 2, 2 -> 3, 3 -> 4, 4-> 1
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)

    def test_multiple_exported_variables(self):
        """
        Tests for when multiple packages have the same exported variable names
        :return:
        """
        expected_output = 'invalid'
        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('multiple_exported_variables'))
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)

    def test_unsatisfied_variable_set(self):
        """
        Tests for when the union of exported variables does not satisfy the required variable set
        :return:
        """
        expected_output = 'invalid'
        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('unsatisfied_variable_set'))
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)

    def test_invalid_json_file(self):
        """
        Tests json file that has json with invalid formatting
        :return:
        """
        expected_output = 'invalid'
        with patch('sys.stdout', new=StringIO()) as fake_out:
            validate_output(get_file_path('invalid_json'))
            self.assertEqual(fake_out.getvalue().replace('\n', ''), expected_output)


if __name__ == '__main__':
    unittest.main()
