"""
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).

You are given a target value to search. If found in the array return true, otherwise return false.

Example 1:

Input: nums = [2,5,6,0,0,1,2], target = 0
Output: true
Example 2:

Input: nums = [2,5,6,0,0,1,2], target = 3
Output: false
Follow up:

This is a follow up problem to Search in Rotated Sorted Array, where nums may contain duplicates.
Would this affect the run-time complexity? How and why?
"""
from typing import List
nums = [2,5,6,0,0,1,2]
target = -2

def search(nums: List[int], target: int) -> bool:
    nums = sorted(nums)
    return bfs(nums, 0, len(nums), target)


def bfs(nums: List[int], low: int, high: int, target: int) -> bool:
    if low <= high:
        mid = low + (high-low)//2
        if mid >= len(nums):
            return  False

        if nums[mid] == target:
            return True

        if target < nums[mid]:
            return bfs(nums, low, mid-1, target)
        else:
            return bfs(nums, mid+1, high, target)
    else:
        return False