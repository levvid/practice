from math import inf
from typing import List

"""
Given an array A of integers, return the length of the longest arithmetic subsequence in A.

Recall that a subsequence of A is a list A[i_1], A[i_2], ..., A[i_k] with 0 <= i_1 < i_2 < ... < i_k <= A.length - 1, and that a sequence B is arithmetic if B[i+1] - B[i] are all the same value (for 0 <= i < B.length - 1).

 

Example 1:

Input: [3,6,9,12]
Output: 4
Explanation: 
The whole array is an arithmetic sequence with steps of length = 3.
Example 2:

Input: [9,4,7,2,10]
Output: 3
Explanation: 
The longest arithmetic subsequence is [4,7,10].
Example 3:

Input: [20,1,15,3,10,5,8]
Output: 4
Explanation: 
The longest arithmetic subsequence is [20,15,10,5].
 

Note:

2 <= A.length <= 2000
0 <= A[i] <= 10000
"""


def longestArithSeqLength(A: List[int]) -> int:
    ans = 2
    n = len(A)
    if n <= 2:
        return n
    llap = [2] * n
    # A.sort()

    for j in range(n - 2, -1, -1):
        i = j - 1
        k = j + 1
        while i >= 0 and k < n:
            if A[i] + A[k] == 2 * A[j]:
                llap[j] = max(llap[k] + 1, llap[j])
                ans = max(ans, llap[j])
                i -= 1
                k += 1
            elif A[i] + A[k] < 2 * A[j]:
                k += 1
            else:
                i -= 1

    return ans
