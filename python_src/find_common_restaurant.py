"""
Suppose Andy and Doris want to choose a restaurant for dinner, and they both have a list of favorite restaurants represented by strings.

You need to help them find out their common interest with the least list index sum. If there is a choice tie between answers, output all of them with no order requirement. You could assume there always exists an answer.

Example 1:
Input:
["Shogun", "Tapioca Express", "Burger King", "KFC"]
["Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"]
Output: ["Shogun"]
Explanation: The only restaurant they both like is "Shogun".
Example 2:
Input:
["Shogun", "Tapioca Express", "Burger King", "KFC"]
["KFC", "Shogun", "Burger King"]
Output: ["Shogun"]
Explanation: The restaurant they both like and have the least index sum is "Shogun" with index sum 1 (0+1).
Note:
The length of both lists will be in the range of [1, 1000].
The length of strings in both lists will be in the range of [1, 30].
The index is starting from 0 to the list length minus 1.
No duplicates in both lists.
"""
from collections import defaultdict
from math import inf
from typing import List

list1 = ["Shogun", "Tapioca Express", "Burger King", "KFC"]
list2 = ["KFC", "Shogun", "Burger King"]


def findRestaurant(list1: List[str], list2: List[str]) -> List[str]:
    set1 = set(list1)
    set2 = set(list2)
    common_restaurants = set1 & set2
    restaurant_index = defaultdict()
    min_index = inf

    if common_restaurants:
        print(common_restaurants)
        for restaurant in common_restaurants:
            index_sum = list1.index(restaurant) + list2.index(restaurant)
            min_index = min(min_index, index_sum)
            restaurant_index[restaurant] = index_sum

    return_list = []
    for key in restaurant_index.keys():
        if restaurant_index[key] == min_index:
            print(type(key))
            return_list.append(key)

    return return_list

