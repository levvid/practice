"""

Implement a trie with insert, search, and startsWith methods.

Example:

Trie trie = new Trie();

trie.insert("apple");
trie.search("apple");   // returns true
trie.search("app");     // returns false
trie.startsWith("app"); // returns true
trie.insert("app");
trie.search("app");     // returns true
Note:

You may assume that all inputs are consist of lowercase letters a-z.
All inputs are guaranteed to be non-empty strings."""
class TrieNode:
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.isEndOfWord = False
        self.children = [None] * 26


class Trie:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.root = self.getNode()


    def getNode(self):
        return TrieNode()

    def get_index(self, ch: str) -> int:
        return ord(ch) - ord('a')

    def insert(self, word: str) -> None:
        """
        Inserts a word into the trie.
        """
        tmp = self.root
        length = len(word)
        for level in range(length):
            if not tmp.children[self.get_index(word[level])]:
                tmp.children[self.get_index(word[level])] = self.getNode()
            tmp = tmp.children[self.get_index(word[level])]

        tmp.isEndOfWord = True


    def search(self, word: str) -> bool:
        """
        Returns if the word is in the trie.
        """
        pCrawl = self.root
        length = len(word)
        for level in range(length):
            index = self.get_index(word[level])
            if not pCrawl.children[index]:
                return False
            pCrawl = pCrawl.children[index]

        return pCrawl != None and pCrawl.isEndOfWord


    def startsWith(self, prefix: str) -> bool:
        """
        Returns if there is any word in the trie that starts with the given prefix.
        """
        pCrawl = self.root
        length = len(prefix)
        for level in range(length):
            index = self.get_index(prefix[level])
            if not pCrawl.children[index]:
                return False
            pCrawl = pCrawl.children[index]

        return pCrawl != None


# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)