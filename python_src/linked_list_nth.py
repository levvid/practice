"""
Given a linked list, remove the n-th node from the end of list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?
"""

from add_two_numbers_linked_list import ListNode


def remove_nth_from_end(head: ListNode, n: int) -> ListNode:
    dummy = ListNode(0)
    dummy.next = head

    first = dummy
    last = head
    count = 0

    while last:
        last = last.next

        if count >= n:
            first = first.next
        count += 1

    first.next = first.next.next

    return dummy.next