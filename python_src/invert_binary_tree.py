"""
Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1
Trivia:
This problem was inspired by this original tweet by Max Howell:

Google: 90% of our engineers use the software you wrote (Homebrew), but you can’t invert a binary tree on a
whiteboard so f*** off.
"""


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def invertTree(self, root: TreeNode) -> TreeNode:
    if not root:
        return root

    self.helper(root)

    return root


def helper(self, root: TreeNode) -> None:
    if root:
        tmp = root.right
        root.right = root.left
        root.left = tmp

        self.helper(root.left)
        self.helper(root.right)
    else:
        return


def invertTree2(self, root: TreeNode) -> TreeNode:
    if not root:
        return root

    def dfs(node):
        if node:
            tmp = node.left
            node.left = node.right
            node.right = tmp
            dfs(node.left)
            dfs(node.right)

    dfs(root)

    return root
