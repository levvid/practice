"""
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.

Your algorithm's runtime complexity must be in the order of O(log n).

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1
"""
from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        if len(nums) == 1:
            if nums[0] == target:
                return 0
            else:
                return -1

        dict = {}
        for i in range(len(nums)):
            dict[nums[i]] = i

        nums.sort()
        print(nums)
        ret = self.bfs(nums, 0, len(nums)-1, target)
        print(ret)
        if ret == -1: return ret;
        return dict[nums[ret]]

    def bfs(self, nums: List[int], low: int, high: int, target: int) -> int:
        # print(len(nums))
        if high >= low:
            mid = low + (high-low)//2
            # print(mid)
            if nums[mid] == target:
                return mid

            elif target < nums[mid]:  # target on the left
                return self.bfs(nums, low, mid-1, target)
            else:  # target on right
                return self.bfs(nums, mid+1, high, target)
        else:
            return -1
