"""
Given an encoded string, return its decoded string.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly
k times. Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those
repeat numbers, k. For example, there won't be input like 3a or 2[4].

Examples:

s = "3[a]2[bc]", return "aaabcbc".
s = "3[a2[c]]", return "accaccacc".
s = "2[abc]3[cd]ef", return "abcabccdcdcdef".
"""

from collections import deque


def decodeString(self, s: str) -> str:
    if not s:
        return ""
    st, end = [], 0
    while end < len(s):
        if s[end].isdigit():
            num = 0
            while end < len(s) and s[end].isdigit():
                num = num * 10 + int(s[end])
                end += 1
            st.append(num)
        elif s[end] == "[":
            st.append("[")
            end += 1
        elif s[end].isalpha():
            temp = []
            while end < len(s) and s[end].isalpha():
                temp.append(s[end])
                end += 1
            st.append("".join(temp))
        elif s[end] == "]":
            x = deque()
            while st[-1] != "[":
                x.appendleft(st[-1])
                st.pop()
            st.pop()
            if st and type(st[-1]) is int:
                temp_str = "".join(x) * st[-1]
                st.pop()
                st.append(temp_str)
            end += 1
    return "".join(st)
