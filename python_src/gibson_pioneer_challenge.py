"""
Pioneer is a "search engine" for ambitious people, so our engineering challenge involves search as well...
but more of the traditional kind.

It's centered around a company called Enron. As you probably know, Enron was an American organization that did some
good but mostly bad things. Especially towards the end. They committed massive fraud. As a result, their entire email
corpus was released to the public.

The legal team suing Enron needs our help. They need to search through the email corpus from their mobile
phone to reference material. A few interesting restrictions:

This will be running in a mobile environment, so we don't have a lot of memory. We can't just load everything into RAM.
We need to be as efficient as possible about organizing the data on disk, keeping as little data as possible in memory.

We don't have access to the Internet. The entire corpus will live on the device.

We do have access to a server to pre-process the data before placing on the device. But once it's on their phone,
it's there forever.

To accommodate these restrictions, here’s what you need to do:

Write a Trie* class. Do not look up any existing Trie implementations. See below for a brief description.

Write a method index_emails(path_to_directory) that returns a populated Trie mapping words to a list of email
file paths containing that word. This would (theoretically) run on the server.

Write a method test(word, trie) that returns the list of file paths that contain the given word. This would
(theoretically) run on the mobile device.

"""
import os
import re
from collections import defaultdict
from typing import List


class TrieNode:
    def __init__(self):
        self.children = defaultdict(str)  # children of this node will be lowercase letters a - z
        self.is_last = False  # true if this is the last letter in a given word
        self.paths = []  # paths to find a word ending in this letter is this is the last letter in the word


class Trie:
    def __init__(self):
        self.root = self.get_trie_node()

    def get_trie_node(self):
        return TrieNode()

    def insert(self, word: str, path: str):
        """
        insert word into the trie and add the file path where the word can be found
        :param word:
        :param path:
        :return:
        """
        curr_node = self.root
        lower_case_word = word.lower()  # store words to lowercase to save space and allow case-insensitive search
        for i in range(len(lower_case_word)):
            if lower_case_word[i] not in curr_node.children:
                curr_node.children[lower_case_word[i]] = self.get_trie_node()  # initialize a trienode if not in
                # children
            curr_node = curr_node.children[lower_case_word[i]]

        curr_node.is_last = True
        curr_node.paths.append(path)  # add to paths if this is last letter of word

    def search_trie(self, word: str) -> List:
        """

        :param word: word to be found
        :return: list of file paths where this word can be found
        """
        curr_node = self.root
        lower_case_word = word.lower()  # search only lower case - words were stored in lower case to allow for case
        # insensitivity

        for i in range(len(lower_case_word)):
            if not curr_node:  # not in trie
                return []
            curr_node = curr_node.children[lower_case_word[i]]

        return curr_node.paths if curr_node and curr_node.is_last else []


def index_emails(path_to_directory: str) -> 'Trie':
    """

    :param path_to_directory:
    :return: a populated Trie mapping words to a list of email file paths containing that word.
    """
    found_files = []
    trie = Trie()  # trie mapping words to file paths
    for root, directory, files in os.walk(path_to_directory):  # recursively find all files
        for file in files:
            found_files.append(os.path.join(root, file))

    for file in found_files:
        file_contents = open(file, 'r')
        try:  # there are some files that generate a unicode decode error due to bad start char, can address this one
            # later
            for line in file_contents:  # traverse file line by line
                word_list = line.split(' ')
                for part in word_list:
                    part_minus_special = re.sub('[^0-9a-zA-Z]+', ' ', part)  # remove special chars - my
                    # implementation splits words that have special characters into separate parts - could be
                    # improved in the case of some special characters like apostrophe
                    word_list = part_minus_special.split(' ')
                    for word in word_list:
                        trie.insert(word, file)
        except UnicodeDecodeError:
            continue

    return trie


def test(word: str, trie: 'Trie'):
    """

    :param word: word to be found
    :param trie:  trie containing words mapped to file paths containing the words
    :return: list of file paths that contain the given word.
    """
    return trie.search_trie(word)


if __name__ == '__main__':
    """
    For testing purposes
    """

    path = '/home/gibson/Downloads/skilling-j/'
    trie = index_emails(path)
    word = 'enron'  # added this to a file as a test case, expect empty list before adding and n
    # results after adding to n number of files. Tried files in different folders as well
    paths = test(word, trie)
    print('These are the file paths for word {} - {}'.format(word, paths))
    # import pdb; pdb.set_trace()
