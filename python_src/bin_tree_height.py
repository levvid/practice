"""
Tree is serialized as so:
-1 == null

[1,-1,-1]

All nodes are given or -1 for null


"""
import math


def bin_tree_height(tree):
    if len(tree) == 3:
        if tree[1] == tree[2] == -1:
            return 1
    if len(tree) == 2:
        if tree[1] == -1:
            return 1

    tmp = math.ceil(math.log2(len(tree) + 1))

    return tmp