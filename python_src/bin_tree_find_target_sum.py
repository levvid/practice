"""
Given a Binary Search Tree and a target number, return true if there exist two elements in the BST such that their sum is equal to the given target.

Example 1:

Input:
    5
   / \
  3   6
 / \   \
2   4   7

Target = 9

Output: True


Example 2:

Input:
    5
   / \
  3   6
 / \   \
2   4   7

Target = 28

Output: False
"""
from bin_tree_inorder_traversal import TreeNode


def findTarget(self, root: TreeNode, k: int) -> bool:
    def dfs(node):
        if node:
            self.elems.append(node.val)
            dfs(node.left)
            dfs(node.right)


    self.elems = []
    self.complements = []
    dfs(root)

    for elem in self.elems:
        if elem in self.complements:
            return True
        self.complements.append(k - elem)

    return False