"""You are a professional robber planning to rob houses along a street. Each house has a certain amount of money
stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system
connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of
money you can rob tonight without alerting the police.

Example 1:

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
             Total amount you can rob = 1 + 3 = 4.
Example 2:

Input: [2,7,9,3,1]
Output: 12
Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
             Total amount you can rob = 2 + 9 + 1 = 12.
max (i..i+2..i+4, i+1..i+3)
[10, 0, 1, 20, 0]
"""
from typing import List

test = [1, 2, 3, 4, 5]


def rob(nums: List[int]) -> int:
    if not nums:
        return 0

    if len(nums) == 1:
        return nums[0]

    total = [0] * len(nums)
    total[0] = nums[0]
    total[1] = max(total[0], nums[1])

    for i in range(2, len(nums)):
        total[i] = max(total[i - 1], total[i - 2] + nums[i])

    return total[len(nums) - 1]
