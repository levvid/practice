"""
Given an array of integers nums, write a method that returns the "pivot" index of this array.

We define the pivot index as the index where the sum of the numbers to the left of the index is equal to the sum of the numbers to the right of the index.

If no such index exists, we should return -1. If there are multiple pivot indexes, you should return the left-most pivot index.

Example 1:

Input:
nums = [1, 7, 3, 6, 5, 6]
Output: 3
Explanation:
The sum of the numbers to the left of index 3 (nums[3] = 6) is equal to the sum of numbers to the right of index 3.
Also, 3 is the first index where this occurs.


Example 2:

Input:
nums = [1, 2, 3]
Output: -1
Explanation:
There is no index that satisfies the conditions in the problem statement.


Note:

The length of nums will be in the range [0, 10000].
Each element nums[i] will be an integer in the range [-1000, 1000].

"""
from copy import copy
from typing import List

nums = [1, 2, 3, 4]
# i = 0  # 0 1 2 3
# j = len(nums) - i - 1  # 3 2 1 0
# nums = [1, 7, 3, 6, 5, 6]
nums = [1, 2, 1]


nums = [-1,-1,0,1,1,0]
nums = [0,-1,-1,0,1,1]
nums = [0, 1]


def pivotIndex(nums: List[int]) -> int:
    if len(nums) < 3:
        return -1

    for i in range(len(nums)):
        left = nums[:i]
        right = nums[i + 1:]

        if find_array_sum(left) == find_array_sum(right):
            return i

    return -1


def pivotIndex2(nums: List[int]) -> int:
    ascending = nums.copy()
    descending = nums.copy()

    for i in range(1, len(nums)):
        ind_descending = len(nums) - i - 1
        print('descending {} ind {}'.format(descending, ind_descending))
        ascending[i] = ascending[i - 1] + nums[i]
        descending[ind_descending] = descending[ind_descending + 1] + nums[ind_descending]

    for i in range(len(ascending)):
        if ascending[i] == descending[i]:
            return i

    return -1

    # print('arr is       {}'.format(nums))
    # print('ascending is {}'.format(ascending))
    # print('descending   {}'.format(descending))


def find_array_sum(arr: List[int]):
    tmp = 0
    for a in arr:
        tmp += a

    return tmp
