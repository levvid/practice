"""
Given an array A of integers, return true if and only if it is a valid mountain array.

Recall that A is a mountain array if and only if:

A.length >= 3
There exists some i with 0 < i < A.length - 1 such that:
A[0] < A[1] < ... A[i-1] < A[i]
A[i] > A[i+1] > ... > A[A.length - 1]

Example 1:

Input: [2,1]
Output: false
Example 2:

Input: [3,5,5]
Output: false
Example 3:

Input: [0,3,2,1]
Output: true


Note:

0 <= A.length <= 10000
0 <= A[i] <= 10000

"""
from typing import List

arr = [1, 2, 3, 4, 3, 2, 1]
arra = [3,1,3,5,6,7]
arr = [1,1,2,1]
arr = []


def validMountainArray(A: List[int]) -> bool:
    if len(A) < 3:
        return False

    increasing = True
    can_be_decreasing = False
    for i in range(len(A)-1):
        j = i+1
        print('Check i: {} j: {}'.format(i, j))
        if A[i] == A[j]:
            print('False 1 i: {} j: {}'.format(i, j))
            return False
        if increasing and i > 0:
            can_be_decreasing = True
        if not can_be_decreasing:  # should be increasing
            if A[i] > A[j]:
                print('False 2 i: {} j: {}'.format(i, j))
                return False
        else:  # can be decreasing
            if A[i] > A[j]:
                if increasing:
                    print('False 3 i: {} j: {}'.format(i, j))
                    increasing = False
            else:
                if not increasing:
                    print('False 4 i: {} j: {}'.format(i, j))
                    return False

    return True if not increasing else False





