"""
Given a binary tree, flatten it to a linked list in-place.

For example, given the following tree:

    1
   / \
  2   5
 / \   \
3   4   6
The flattened tree should look like:

1
 \
  2
   \
    3
     \
      4
       \
        5
         \
          6
"""

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def flatten(self, root: TreeNode) -> None:
    """
    Do not return anything, modify root in-place instead.
    """

    if not root:
        return root
    lst = []
    self.bst(root, lst)

    dummy = TreeNode(0)
    dummy.right = root
    curr = root
    root.left = None

    print(lst)

    for i in range(1, len(lst)):
        tmp = TreeNode(lst[i])
        curr.right = tmp
        curr = tmp

    return dummy.right


def bst(self, node, lst):
    if not node:
        return lst
    lst.append(node.val)
    self.bst(node.left, lst)
    self.bst(node.right, lst)
