"""
Given a string and an integer k, you need to reverse the first k characters for every 2k characters counting from the start of the string. If there are less than k characters left, reverse all of them. If there are less than 2k but greater than or equal to k characters, then reverse the first k characters and left the other as original.
Example:
Input: s = "abcdefg", k = 2
Output: "bacdfeg"
Restrictions:
The string consists of lower English letters only.
Length of the given string and k will in the range [1, 10000]
"""


def reverseStr(s: str, k: int) -> str:
    ret = ''
    start = 0
    i = 0

    while i < len(s):
        if start + k <= len(s):
            ret += s[start:start + k][::-1]
            start += k
            ret += s[start:start + k]
            start += k
        else:
            ret += s[start:][::-1]
            start += k
            print('here')
        print('i {} start {}'.format(i, start))
        i += 2 * k

    return ret
