"""
Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You may assume no duplicates in the array.

Example 1:

Input: [1,3,5,6], 5
Output: 2
Example 2:

Input: [1,3,5,6], 2
Output: 1
Example 3:

Input: [1,3,5,6], 7
Output: 4
Example 4:

Input: [1,3,5,6], 0
Output: 0
"""
from typing import List
nums = [1,3,5,6,10]
target = 8

def searchInsert(nums: List[int], target: int) -> int:
    return bin_search(nums, 0, len(nums), target)

    # pass


def bin_search(nums: List[int], low: int, high: int, target: int) -> int:
    print('LOW: {} high: {}'.format(low, high))
    mid = low + (high - low) // 2
    if mid > len(nums) - 1:
        return mid

    if low <= high:

        if nums[mid] == target:
            return mid

        if target < nums[mid]:
            return bin_search(nums, low, mid-1, target)
        else:
            return bin_search(nums, mid+1, high, target)
    else:
        return low
