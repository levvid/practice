"""
An image is represented by a 2-D array of integers, each integer representing the pixel value of the image (from 0 to 65535).

Given a coordinate (sr, sc) representing the starting pixel (row and column) of the flood fill,
and a pixel value newColor, "flood fill" the image.

To perform a "flood fill", consider the starting pixel, plus any pixels connected 4-directionally to the
starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels
(also with the same color as the starting pixel), and so on. Replace the color of all of the aforementioned
pixels with the newColor.

At the end, return the modified image.

Example 1:
Input:
image = [[1,1,1],
        [1,1,0],
        [1,0,1]]
sr = 1, sc = 1, newColor = 2
Output: [[2,2,2],[2,2,0],[2,0,1]]
Explanation:
From the center of the image (with position (sr, sc) = (1, 1)), all pixels connected
by a path of the same color as the starting pixel are colored with the new color.
Note the bottom corner is not colored 2, because it is not 4-directionally connected
to the starting pixel.
Note:

The length of image and image[0] will be in the range [1, 50].
The given starting pixel will satisfy 0 <= sr < image.length and 0 <= sc < image[0].length.
The value of each color in image[i][j] and newColor will be an integer in [0, 65535].
"""
from typing import List

IMAGE = [[1, 1, 1],
         [1, 1, 0],
         [1, 0, 1]]

# IMAGE = [[0,0,0],
#          [0,1,1]]
sr = 1
sc = 1
newColor = 1
# IMAGE = [[1, 1, 1],
#          [1, 1, 0]]


def floodFill(image: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:
    old_color = image[sr][sc]
    visited = [[False] * len(image[0])] * len(image)
    image[sr][sc] = newColor
    visited[sr][sc] = True
    fill(image, sr, sc, newColor, old_color, visited)

    print(image)
    # print(visited)


def fill(image: List[List[int]], sr, sc, newColor, old_color, visited):
    related = neighbours(image, sr, sc, old_color, visited)
    # print(related)
    print(visited)
    if len(related) == 0:
        return

    for row, col in related:
        image[row][col] = newColor
        visited[row][col] = True
        fill(image, row, col, newColor, old_color, visited)


def neighbours(image: List[List[int]], i: int, j: int, old_color: int, visited):
    """
    4 directions:
        i - 1, j
        i + 1, j
        i, j + 1
        i, j - 1
    """
    coods = []
    if i - 1 >= 0:
        if image[i - 1][j] == old_color:
            coods.append((i - 1, j))
    if i + 1 < len(image):
        if image[i + 1][j] == old_color:
            coods.append((i + 1, j))
    if j - 1 >= 0:
        if image[i][j - 1] == old_color:
            coods.append((i, j - 1))
    if j + 1 < len(image[0]):
        if image[i][j + 1] == old_color:
            coods.append((i, j + 1))
    ret = []
    for row, col in coods:
        if not visited[row][col]:
            ret.append((row, col))
    return ret
