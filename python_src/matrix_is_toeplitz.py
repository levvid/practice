"""
A matrix is Toeplitz if every diagonal from top-left to bottom-right has the same element.

Now given an M x N matrix, return True if and only if the matrix is Toeplitz.


Example 1:

Input:
matrix = [
  [1,2,3,4],
  [5,1,2,3],
  [9,5,1,2]
]
9 - 2,0
5 - 1,0
5 - 2,1
1 - 2,2
1 - 1,1
2 - 2,3

3 - 1,3
4 - 0, 3


when j == len(mtx[0] - 1: i -1
Output: True
Explanation:
In the above grid, the diagonals are:
"[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]".
In each diagonal all elements are the same, so the answer is True.
Example 2:

Input:
matrix = [
  [1,2],
  [2,2]
]
Output: False
Explanation:
The diagonal "[1, 2]" has different elements.

Note:

matrix will be a 2D array of integers.
matrix will have a number of rows and columns in range [1, 20].
matrix[i][j] will be integers in range [0, 99].

"""
from typing import List


def isToeplitzMatrix(matrix: List[List[int]]) -> bool:
    def check_diagonal(i, j):
        prev = matrix[i][j]
        i -= 1
        j -= 1
        while i >= 0 and j >= 0:
            if matrix[i][j] != prev:
                return False
            i -= 1
            j -= 1
        return True

    i = len(matrix) - 1
    j = 1

    if len(matrix[0]) == 1:
        return True

    while 0 <= i < len(matrix) and 0 <= j < len(matrix[0]):
        if not check_diagonal(i, j):
            return False
        j += 1

        if j == len(matrix[0]):
            j = j - 1
            i -= 1

    return True
