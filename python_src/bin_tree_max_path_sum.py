"""
Given a non-empty binary tree, find the maximum path sum.

For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree along
the parent-child connections. The path must contain at least one node and does not need to go through the root.

Example 1:

Input: [1,2,3]

       1
      / \
     2   3

Output: 6
Example 2:

Input: [-10,9,20,null,null,15,7]

   -10
   / \
  9  20
    /  \
   15   7

Output: 42
"""
import sys

from bin_tree_delete_nodes import TreeNode


class Solution:
    def maxPathSum(self, root: TreeNode) -> int:
        self.res = -sys.maxsize - 1
        self.oneSideSum(root)
        return self.res

    # compute one side maximal sum,
    # (root+left children, or root+right children),
    # root is the included top-most node
    def oneSideSum(self, root):
        if not root:
            return 0
        l = max(0, self.oneSideSum(root.left))
        r = max(0, self.oneSideSum(root.right))
        self.res = max(self.res, l + r + root.val)
        return max(l, r) + root.val
