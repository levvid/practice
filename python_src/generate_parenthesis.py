"""
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

For example, given n = 3, a solution set is:

[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]

"""
from typing import List


def generateParenthesis(self, n: int) -> List[str]:
    def gen(left, right, ret_str, lst):
        if right == n:
            lst.append(ret_str)
            return
        else:
            if right < left:
                gen(left, right + 1, ret_str + ')', lst)
            if left < n:
                gen(left + 1, right, ret_str + '(', lst)

    lst = []
    gen(0, 0, '', lst)

    return lst

def generateParenthesis(n: int) -> List[str]:
    ret = ''
    lst = []

    # print('this is lst b4 {}'.format(lst))
    # r = _generate_parens(0, 0, ret, n, lst)
    #
    # print(r)
    # print('this is lst {}'.format(lst))
    str = [""] * 2 * n;
    if (n > 0):
        _generateParenthesis(str, 0, n, 0, 0, lst);

    print(str)
    print(lst)


def _generateParenthesis(str, pos, n, open, close, lst):
    if close == n:
        lst.append(''.join(str))
        return
    else:
        if open > close:
            str[pos] = ')'
            _generateParenthesis(str, pos + 1, n, open, close + 1, lst)
        if open < n:
            str[pos] = '('
            _generateParenthesis(str, pos + 1, n, open + 1, close, lst)
