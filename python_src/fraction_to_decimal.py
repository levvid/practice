"""
Given two integers representing the numerator and denominator of a fraction, return the fraction in string format.

If the fractional part is repeating, enclose the repeating part in parentheses.

Example 1:

Input: numerator = 1, denominator = 2
Output: "0.5"
Example 2:

Input: numerator = 2, denominator = 1
Output: "2"
Example 3:

Input: numerator = 2, denominator = 3
Output: "0.(6)"
"""
from collections import defaultdict

numerator = 1
denominator = 2


def fractionToDecimal(numerator: int, denominator: int) -> str:
    sign = 1
    ret = ''
    if numerator == 0 or denominator == 0:
        return '0'

    if numerator < 0 or denominator < 0:
        sign = -1
    if numerator < 0 and denominator < 0:
        sign = 1

    if sign == -1:
        ret += '-'

    numerator = abs(numerator)
    denominator = abs(denominator)

    initial = numerator // denominator

    ret += str(initial)
    rem = numerator % denominator
    rem_dict = defaultdict(int)
    if rem == 0:
        print('Returning this {}'.format(rem))
        return ret

    ret += '.'

    is_repeating = False
    index = 0
    while rem > 0 and not is_repeating:
        if rem in rem_dict:
            index = rem_dict[rem]
            is_repeating = True
            break
        else:
            rem_dict[rem] = len(ret)

        rem = rem * 10

        # calculate quotient, append to result and calc next rem
        tmp = rem // denominator
        ret += str(tmp)
        rem = rem % denominator

    if is_repeating:
        ret += ')'
        ret = ret[:index] + '(' + ret[index:]

    return ret