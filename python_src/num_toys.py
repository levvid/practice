from collections import defaultdict
from operator import attrgetter, itemgetter

numToys = 6
topToys = 4
toys = ['elsa', 'legos', 'drone', 'tablet', 'elmo', 'warcraft', 'apple', 'baba']
toys = ['hehe', 'heeyu']
numQuotes = 6
quotes = ['Elmo is the hottest toy. Elmo wish apple',
          'The new elmo dolls are baba',
          'Elsa dolls baba',
          'Elsa and elmo baba',
          'Drone for old apple',
          'warcraft is pops apple baba']


def popularNTs(numToys, topToys, toys, numQuotes, quotes):
    toy_count_dict = defaultdict(int)

    for toy in toys:
        for quote in quotes:
            # change both to lowercase for comparison
            toy = toy.lower()
            quote = quote.lower()
            if toy in quote:
                toy_count_dict[toy] += 1
    sorted_top_toy_list = multisort([(k,v) for k, v in toy_count_dict.items()], ((1, True), (0, False)))  # sort first by the value then key

    return [top_toy for top_toy, _ in sorted_top_toy_list[:topToys]]


def multisort(xs, specs):
    for key, reverse in reversed(specs):
        xs.sort(key=itemgetter(key), reverse=reverse)
    return xs

input_list = ['a', 'b', 'c', 'a', 'b', 'b']

class MaxMin:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def lengthOfEachScene(inputList):
    input_index_dict = defaultdict(list)
    length_of_scene = []
    for i in range(len(inputList)):
        input_index_dict[inputList[i]].append(i)

    keys = list(input_index_dict.keys())

    print(keys)
    included  = []
    for i in range(len(keys)):
        for j in range(i+1, len(keys)):
            if max(input_index_dict[keys[i]]) > max(input_index_dict[keys[j]]):
                included.append(input_index_dict[keys[j]])
            elif min(input_index_dict[keys[j]]) > min(input_index_dict[keys[i]]) and max(input_index_dict[keys[j]]) > max(input_index_dict[keys[i]]):
                included.append(input_index_dict[keys[j]])
                input_index_dict[keys[i]].append(max(input_index_dict[keys[j]]))
                input_index_dict.keys().__module__ == input_index_dict[keys]



    return_list = []
    for key in keys:
        if key not in included:
            return_list.append(max(input_index_dict[key])+1)

    return return_list

    print(input_index_dict)
    pass
if __name__ == '__main__':
    toys = popularNTs(numToys, topToys, toys, numQuotes, quotes)
    print('This are the tops ones: {}'.format(toys))
    # tmp = lengthOfEachScene(input_list)

