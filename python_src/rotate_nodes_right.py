"""
Given a linked list, rotate the list to the right by k places, where k is non-negative.

Example 1:

Input: 1->2->3->4->5->NULL, k = 2
Output: 4->5->1->2->3->NULL
Explanation:
rotate 1 steps to the right: 5->1->2->3->4->NULL
rotate 2 steps to the right: 4->5->1->2->3->NULL
Example 2:

Input: 0->1->2->NULL, k = 4
Output: 2->0->1->NULL
Explanation:
rotate 1 steps to the right: 2->0->1->NULL
rotate 2 steps to the right: 1->2->0->NULL
rotate 3 steps to the right: 0->1->2->NULL
rotate 4 steps to the right: 2->0->1->NULL
"""


# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def rotate_right(head: ListNode, k: int) -> ListNode:
    dummy = ListNode(0)
    dummy.next = head

    curr = head
    prev = dummy
    tail = None
    length = 0
    while curr:
        length += 1
        if not curr.next:
            tail = curr
        curr = curr.next

    curr = head
    i = 0

    while curr:
        index = length - k if k <= length else length - k % length
        if i == index:
            prev.next = None
            tail.next = dummy.next  # make original head come after current tail
            dummy.next = curr  # head
        prev = curr
        curr = curr.next
        i += 1

    return dummy.next
