"""
Given a non-empty binary tree, return the average value of the nodes on each level in the form of an array.
Example 1:
Input:
    3
   / \
  9  20
    /  \
   15   7
Output: [3, 14.5, 11]
Explanation:
The average value of nodes on level 0 is 3,  on level 1 is 14.5, and on level 2 is 11. Hence return [3, 14.5, 11].
Note:
The range of node's value is in the range of 32-bit signed integer.
"""
from collections import defaultdict
from typing import List

from bin_tree_delete_nodes import TreeNode


def averageOfLevels(self, root: TreeNode) -> List[float]:
    def bfs(node, level):
        if node:
            if level in self.tree_dict:
                num_elems, avg = self.tree_dict[level]
                avg = (num_elems * avg + node.val) / (num_elems + 1)
                self.tree_dict[level] = num_elems + 1, avg
            else:
                self.tree_dict[level] = 1, node.val

            bfs(node.left, level + 1)
            bfs(node.right, level + 1)

    self.tree_dict = defaultdict(tuple)
    bfs(root, 0)

    return [y for x, y in self.tree_dict.values()]