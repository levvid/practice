"""
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
"""
nums = [2, 7, 11, 15]
target = 4

def twoSum(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: List[int]
    """
    values = {}

    for i in range(0, len(nums)):
        complement = target - nums[i]
        if complement in values:
            return [values[complement], i]
        values[nums[i]] = i

    return None


def twoSum2(nums, target):
    """

    :param nums:
    :param target:
    :return:
    """
    for i in range(len(nums)):
        compl = target - nums[i]
        tmp = nums[:i] + nums[i+1:]
        if compl in tmp:
            return [i, nums.index(compl)]



