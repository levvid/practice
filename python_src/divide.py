"""
Given two integers dividend and divisor, divide two integers without using multiplication, division and mod operator.

Return the quotient after dividing dividend by divisor.

The integer division should truncate toward zero, which means losing its fractional part.
For example, truncate(8.345) = 8 and truncate(-2.7335) = -2.

Example 1:

Input: dividend = 10, divisor = 3
Output: 3
Explanation: 10/3 = truncate(3.33333..) = 3.
Example 2:

Input: dividend = 7, divisor = -3
Output: -2
Explanation: 7/-3 = truncate(-2.33333..) = -2.
Note:

Both dividend and divisor will be 32-bit signed integers.
The divisor will never be 0.
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range:
[−231,  231 − 1]. For the purpose of this problem, assume that your function returns 231 − 1 when the division result
overflows.
"""


def divide(self, dividend: int, divisor: int) -> int:
    sign = (-1 if ((dividend < 0) ^ (divisor < 0)) else 1)
    dividend = abs(dividend)
    divisor = abs(divisor)

    # Initialize
    # the quotient
    quotient = 0
    temp = 0

    # test down from the highest
    # bit and accumulate the
    # tentative value for valid bit
    for i in range(31, -1, -1):
        if temp + (divisor << i) <= dividend:
            temp += divisor << i
            quotient |= 1 << i

    ret = 2 ** 31 - 1
    tmp = sign * quotient
    if tmp > ret:
        return ret
    return tmp
