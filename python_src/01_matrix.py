"""
Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.

The distance between two adjacent cells is 1.



Example 1:

Input:
[[0,0,0],
 [0,1,0],
 [0,0,0]]

Output:
[[0,0,0],
 [0,1,0],
 [0,0,0]]
Example 2:

Input:
[[0,0,0],
 [0,1,0],
 [1,1,1]]

Output:
[[0,0,0],
 [0,1,0],
 [1,2,1]]


Note:

The number of elements of the given matrix will not exceed 10,000.
There are at least one 0 in the given matrix.
The cells are adjacent in only four directions: up, down, left and right.
"""
from collections import deque
from typing import List


class Solution:
    def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:
        m = len(matrix)
        n = len(matrix[0])

        queue = deque()

        # for reusing original matrix, convert 1 to infinity at initialization
        for i, row in enumerate(matrix):
            for j, ele in enumerate(row):
                if ele:
                    matrix[i][j] = float('inf')
                else:
                    queue.append((i, j))

        directions = ((1, 0), (-1, 0), (0, 1), (0, -1))
        while queue:
            for count in range(len(queue)):
                i, j = queue.popleft()

                for x, y in directions:
                    row, col = i + x, j + y

                    if -1 < row < m and -1 < col < n and matrix[row][col] > matrix[i][j] + 1:
                        matrix[row][col] = matrix[i][j] + 1
                        queue.append((row, col))

        return matrix