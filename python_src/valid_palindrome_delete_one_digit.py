"""
Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.

Example 1:
Input: "aba"
Output: True
Example 2:
Input: "abca"
Output: True
Explanation: You could delete the character 'c'.
Note:
The string will only contain lowercase characters a-z. The maximum length of the string is 50000.
"""

def validPalindrome(self, s: str) -> bool:
    string = s
    low = 0
    high = len(string) - 1
    if self.is_palindrome(s):
        return True
    while low < high:
        if string[low] == string[high]:
            low += 1
            high -= 1
        else:
            if self.isPalindrome(string, low + 1, high):
                return True
            if self.isPalindrome(string, low, high - 1):
                return True
            return False

    return False


def is_palindrome(self, s: str) -> bool:
    return s == s[::-1]


def isPalindrome(self, string: str, low: int, high: int) -> bool:
    while low < high:
        if string[low] != string[high]:
            return False
        low += 1
        high -= 1
    return True



"""
Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

Note: For the purpose of this problem, we define empty string as valid palindrome.

Example 1:

Input: "A man, a plan, a canal: Panama"
Output: true
Example 2:

Input: "race a car"
Output: false
"""


def isPalindrome(self, s: str) -> bool:
    ts = ''.join(filter(str.isalnum, s)).lower()
    # print(s)
    # print(ts)

    return ts == ts[::-1]