"""
Given a column title as appear in an Excel sheet, return its corresponding column number.

For example:

    A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28
    ...
Example 1:

Input: "A"
Output: 1
Example 2:

Input: "AB"
Output: 28
Example 3:

Input: "ZY"
Output: 701
"""


def titleToNumber(self, s: str) -> int:
    return self.col2num(s)


def col2num(self, col) -> int:
    num = 0
    for c in col:
        # if c in string.ascii_letters:
        num = num * 26 + (ord(c.upper()) - ord('A')) + 1
    return num