"""
We have some clickstream data that we gathered on our client's website. Using cookies, we collected snippets of users' anonymized URL histories while they browsed the site. The histories are in chronological order, and no URL was visited more than once per person.

Write a function that takes two users' browsing histories as input and returns the longest contiguous sequence of URLs that appears in both.

Sample input:

user0 = ["/start", "/pink", "/register", "/orange", "/red", "a"]
user1 = ["/start", "/green", "/blue", "/pink/", "/register", "/orange", "/one/two"]


user2 = ["a", "/one", "/two"]
user3 = ["/pink", "/orange", "/yellow", "/plum", "/blue", "/tan", "/red", "/amber", "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow", "/BritishRacingGreen"]
user4 = ["/pink", "/orange", "/amber", "/BritishRacingGreen", "/plum", "/blue", "/tan", "/red", "/lavender", "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow"]
user5 = ["a"]

Sample output:

findContiguousHistory(user0, user1)
   /pink
   /register
   /orange

findContiguousHistory(user1, user2)
   (empty)

findContiguousHistory(user2, user0)
   a


findContiguousHistory(user5, user2)
   a

findContiguousHistory(user3, user4)
   /plum
   /blue
   /tan
   /red

findContiguousHistory(user4, user3)
   /plum
   /blue
   /tan
   /red

n: length of the first user's browsing history
m: length of the second user's browsing history



"""

user0 = ["/start", "/pink", "/register", "/orange", "/red", "a"]
user1 = ["/start", "/green", "/blue", "/pink", "/register", "/orange", "/one/two"]
user2 = ["a", "/one", "/two"]
user3 = ["/pink", "/orange", "/yellow", "/plum", "/blue", "/tan", "/red", "/amber", "/HotRodPink", "/CornflowerBlue",
         "/LightGoldenRodYellow", "/BritishRacingGreen"]
user4 = ["/pink", "/orange", "/amber", "/BritishRacingGreen", "/plum", "/blue", "/tan", "/red", "/lavender",
         "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow"]
user5 = ["a"]


# O(n*m^2) - time
# O(n) - space
def findContiguousHistory(user1, user2):  # n m
    max_count = 0  # O(1)
    max_count_str = []  # O(n)

    for i in range(len(user1)):  # O(n)
        history = user1[i]
        if history in user2:  # O(m)
            idx = user2.index(history)
            j = i
            tmp_count = 0
            while idx < len(user2) and j < len(user1):  # O(m)
                if user2[idx] == user1[j]:
                    tmp_count += 1
                    if idx == len(user2) - 1 or j == len(user1) - 1:
                        if tmp_count > max_count:
                            max_count = tmp_count
                            max_count_str = user1[i:j + 1]
                else:
                    if tmp_count > max_count:
                        max_count = tmp_count
                        max_count_str = user1[i:j]
                    break
                idx += 1
                j += 1

    return max_count_str


# Sample output:

# findContiguousHistory(user0, user1)
#    /pink
#    /register
#    /orange

# findContiguousHistory(user1, user2)
#    (empty)

# findContiguousHistory(user2, user0)
#    a


# findContiguousHistory(user5, user2)
#    a

# findContiguousHistory(user3, user4)
#    /plum
#    /blue
#    /tan
#    /red

# findContiguousHistory(user4, user3)
#    /plum
#    /blue
#    /tan
#    /red

if __name__ == '__main__':
    print(findContiguousHistory(user4, user3))

# FIRST QUESTION

from collections import defaultdict

counts = ["900,google.com",
          "60,mail.yahoo.com",
          "10,mobile.sports.yahoo.com",
          "40,sports.yahoo.com",
          "300,yahoo.com",
          "10,stackoverflow.com",
          "20,overflow.com",
          "2,en.wikipedia.org",
          "1,m.wikipedia.org",
          "1,mobile.sports",
          "1,google.co.uk"]

counts = ["10,com",
          "20,mail.com"]


def num_clicks(domains):
    domain_dict = defaultdict(int)

    for data in domains:  # O(n)
        domain_data = data.split(',')
        count = int(domain_data[0])  # 900
        domain = domain_data[1]  # google.com

        split_domain = domain.split('.')

        for i in range(len(split_domain) - 1, -1, -1):  # m
            domain_dict['.'.join(split_domain[i:])] += count

    print(domain_dict.items())

    return domain_dict

# O(n*m)
# space - O(n)
# if __name__ == '__main__':
#     num_clicks(counts)





