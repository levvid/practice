"""
Given a binary tree, return the inorder traversal of its nodes' values.

Example:

Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,3,2]
Follow up: Recursive solution is trivial, could you do it iteratively?
"""
from typing import List


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def inorderTraversal(self, root: TreeNode) -> List[int]:
    lst = []

    self.traverse(root, lst)

    return lst


def traverse(self, node: TreeNode, lst: list) -> None:
    if node:
        self.traverse(node.left, lst)
        lst.append(node.val)
        self.traverse(node.right, lst)

    def inorderTraversal(self, root: TreeNode) -> List[int]:
        current = root
        stack = []
        ret = []
        while True:
            if current:
                stack.append(current)
                current = current.left
            elif stack:
                current = stack.pop()
                ret.append(current.val)
                current = current.right
            else:
                break

        return ret

        # def dfs(node):
        #     if node:
        #         dfs(node.left)
        #         self.ret.append(node.val)
        #         dfs(node.right)
        #
        # self.ret = []
        # dfs(root)
        #
        # return self.ret
