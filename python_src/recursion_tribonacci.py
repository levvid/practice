"""
The Tribonacci sequence Tn is defined as follows:

T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.

Given n, return the value of Tn.



Example 1:

Input: n = 4
Output: 4
Explanation:
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4
Example 2:

Input: n = 25
Output: 1389537


Constraints:

0 <= n <= 37
The answer is guaranteed to fit within a 32-bit integer, ie. answer <= 2^31 - 1.
"""


def tribonacci_fast(n: int) -> int:
    zero = 0
    one = 1
    two = 1

    if n == 0:
        return zero
    elif n == 1 or n == 2:
        return one
    else:
        for i in range(3, n + 1):
            tmp = two + one + zero
            zero = one
            one = two
            two = tmp
        return two


def tribonacci_two(n: int) -> int:
    mem = [None] * (n + 1)
    if n == 0:
        return 0
    elif n in [1, 2]:
        return 1
    elif mem[n]:
        return mem[n]
    else:
        return tribonacci_two(n-1) + tribonacci_two(n-2) + tribonacci_two(n-3)




def tribonacci_one(self, n: int) -> int:
    if n == 0:
        return 0
    if n in [1, 2]:
        return 1
    return self.tribonacci(n - 1) + self.tribonacci(n - 2) + self.tribonacci(n - 3)
