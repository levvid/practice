"""
Implement FreqStack, a class which simulates the operation of a stack-like data structure.

FreqStack has two functions:

push(int x), which pushes an integer x onto the stack.
pop(), which removes and returns the most frequent element in the stack.
If there is a tie for most frequent element, the element closest to the top of the stack is removed and returned.


Example 1:

Input:
["FreqStack","push","push","push","push","push","push","pop","pop","pop","pop"],
[[],[5],[7],[5],[7],[4],[5],[],[],[],[]]
Output: [null,null,null,null,null,null,null,5,7,5,4]
Explanation:
After making six .push operations, the stack is [5,7,5,7,4,5] from bottom to top.  Then:

pop() -> returns 5, as 5 is the most frequent.
The stack becomes [5,7,5,7,4].

pop() -> returns 7, as 5 and 7 is the most frequent, but 7 is closest to the top.
The stack becomes [5,7,5,4].

pop() -> returns 5.
The stack becomes [5,7,4].

pop() -> returns 4.
The stack becomes [5,7].


Note:

Calls to FreqStack.push(int x) will be such that 0 <= x <= 10^9.
It is guaranteed that FreqStack.pop() won't be called if the stack has zero elements.
The total number of FreqStack.push calls will not exceed 10000 in a single test case.
The total number of FreqStack.pop calls will not exceed 10000 in a single test case.
The total number of FreqStack.push and FreqStack.pop calls will not exceed 150000 across all test cases.

"""
from collections import defaultdict, Counter
from operator import itemgetter


class FreqStack:
    """
    Implementation 1 - fastest
    """

    def __init__(self):
        self.freq = Counter()
        self.group = defaultdict(list)
        self.maxfreq = 0

    def push(self, x):
        f = self.freq[x] + 1
        self.freq[x] = f
        if f > self.maxfreq:
            self.maxfreq = f
        self.group[f].append(x)

    def pop(self):
        x = self.group[self.maxfreq].pop()
        self.freq[x] -= 1
        if not self.group[self.maxfreq]:
            self.maxfreq -= 1

        return x

    """
    Alternative - not so fast
    """

    def __init__(self):
        self.stack = defaultdict(list)
        self.index = 0

    def push(self, x: int) -> None:
        self.stack[x].append(self.index)
        self.index += 1

    def pop(self) -> int:
        def c_key(stack):
            print(stack)
            return len(stack[1]), max(stack[1])

        sorted_stack = sorted(self.stack.items(), key=c_key, reverse=True)
        elem = sorted_stack[0][0]
        self.stack[elem].pop()
        return elem

    """
    Implementation 2
    """
    def __init__(self):
        self.stack = []
        self.stack_dict = defaultdict(list)

    def push(self, x: int) -> None:
        self.stack_dict[x].append(len(self.stack))  # index of x
        self.stack.append(x)

    def pop(self) -> int:
        print('list {}'.format(self.stack))
        print('This is the dict {}'.format(self.stack_dict.items()))
        for key, value in sorted(self.stack_dict.items(), key=self.comparator, reverse=True):
            # print('Key {} Value {}'.format(key, self.stack_dict[key]))
            tmp = self.stack[::-1]
            for i in range(len(self.stack)):
                if tmp[i] == key:
                    tmp = tmp[:i] + tmp[i+1:]
                    self.stack = tmp[::-1]
                    self.stack_dict[key].pop()
                    return key
                # print(i)
            pass

    def comparator(self, stack_dict):
        return len(stack_dict[1]), max(stack_dict[1])





if __name__ == '__main__':
    freq = FreqStack()
    freq.push(1)
    freq.push(2)
    freq.push(2)
    freq.push(2)
    freq.push(3)
    freq.push(1)
    freq.push(3)
    freq.push(3)
    freq.push(1)

    for i in range(1, 100):
        freq.push(i)
    s = freq.pop()
    t = freq.pop()
    print('S is {}'.format(s))
