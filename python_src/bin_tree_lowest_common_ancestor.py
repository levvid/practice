"""
Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as
the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”

Given the following binary tree:  root = [3,5,1,6,2,0,8,null,null,7,4]

Example 1:

Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
Output: 3
Explanation: The LCA of nodes 5 and 1 is 3.
Example 2:

Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
Output: 5
Explanation: The LCA of nodes 5 and 4 is 5, since a node can be a descendant of itself according to the LCA definition.


Note:

All of the nodes' values will be unique.
p and q are different and both values will exist in the binary tree.
"""


def lowestCommonAncestor(root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
    def find_path(node, check, path):
        if node:
            path.append(node)
            if node == check:
                return True

            if find_path(node.left, check, path) or find_path(node.right, check, path):
                return True
            path.pop()
            return False
        else:
            return False

    path_p = []
    path_q = []

    find_path(root, p, path_p)
    find_path(root, q, path_q)

    i = 0

    while i < len(path_p) and i < len(path_q):
        if path_p[i] != path_q[i]:
            break
        i += 1

    # print(path_p)
    # print(path_p[i-1])

    return path_p[i - 1]