"""
Repeated Substring Pattern
Given a non-empty string check if it can be constructed by taking a substring of it and appending multiple copies of the substring together.
You may assume the given string consists of lowercase English letters only and its length will not exceed 10000.

Example 1:

Input: "abab"
Output: True
Explanation: It's the substring "ab" twice.
Example 2:

Input: "aba"
Output: False
Example 3:

Input: "abcabcabcabc"
Output: True
Explanation: It's the substring "abc" four times. (And the substring "abcabc" twice.)
"""

class Solution:
    def repeatedSubstringPattern(self, string: str) -> bool:
        n = len(string)
        lps = [0] * n
        # Preprocess the pattern (calculate lps[] array)
        self.computeLPSArray(string, n, lps)

        # Find length of longest suffix which is also
        # prefix of str.
        length = lps[n-1]

        # If there exist a suffix which is also prefix AND
        # Length of the remaining substring divides total
        # length, then str[0..n-len-1] is the substring that
        # repeats n/(n-len) times (Readers can print substring
        # and value of n/(n-len) for more clarity.
        if length > 0 and n%(n-length) == 0:
            return True
        else:
            False

    def computeLPSArray(self, string, M, lps):
        length = 0        # length of the previous longest prefix suffix
        i = 1
        lps[0] = 0    # lps[0] is always 0

        # the loop calculates lps[i] for i = 1 to M-1
        while i < M:
            if string[i] == string[length]:
                length += 1
                lps[i] = length
                i += 1
            else:
                if length != 0:
                    # This is tricky. Consider the example AAACAAAA
                    # and i = 7.
                    length = lps[length-1]

                    # Also, note that we do not increment i here
                else:
                    lps[i] = 0
                    i += 1
