def swap_cases(word: str) -> str:
    ret = ''
    for c in word:
        if c.isupper():
            ret += c.lower()
        else:
            ret += c.upper()

    return ret


def reverse_words_order_and_swap_cases(sentence: str) -> str:
    words = sentence.split(' ')
    reverse_words = []
    for i in range(len(words)-1, -1, -1):
        swapped_case = swap_cases(words[i])
        reverse_words.append(swapped_case)

    return ' '.join(reverse_words)
