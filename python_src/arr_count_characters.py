"""
You are given an array of strings words and a string chars.

A string is good if it can be formed by characters from chars (each character can only be used once).

Return the sum of lengths of all good strings in words.



Example 1:

Input: words = ["cat","bt","hat","tree"], chars = "atach"
Output: 6
Explanation:
The strings that can be formed are "cat" and "hat" so the answer is 3 + 3 = 6.
Example 2:

Input: words = ["hello","world","leetcode"], chars = "welldonehoneyr"
Output: 10
Explanation:
The strings that can be formed are "hello" and "world" so the answer is 5 + 5 = 10.


Note:

1 <= words.length <= 1000
1 <= words[i].length, chars.length <= 100
All strings contain lowercase English letters only.
"""
from collections import defaultdict
from copy import deepcopy
from typing import List

WORDS = ["cat", "bt", "hat", "tree"]
CHARS = "atach"

WORDS = ["hello", "world", "leetcode"]
CHARS = "welldonehoneyr"


class Solution:
    def countCharacters(self, words: List[str], chars: str) -> int:
        char_dict = defaultdict(int)
        count = 0

        for c in chars:
            char_dict[c] += 1

        for word in words:
            dict_copy = char_dict.copy()
            for i in range(len(word)):
                if word[i] in char_dict.keys():
                    if dict_copy[word[i]] > 0:
                        dict_copy[word[i]] -= 1
                        if i == len(word) - 1:
                            count += len(word)
                    else:
                        break
                else:
                    break

        return count


def countCharacters(self, words: List[str], chars: str) -> int:
    sum_lengths = 0

    def is_good(word, char_dict):
        tmp_dict = deepcopy(char_dict)

        for w in word:
            if w in tmp_dict:
                tmp_dict[w] -= 1
                if tmp_dict[w] == 0:
                    del tmp_dict[w]
            else:
                return False
        return True

    char_dict = defaultdict(int)

    for char in chars:
        char_dict[char] += 1

    for word in words:
        if is_good(word, char_dict):
            sum_lengths += len(word)

    return sum_lengths
