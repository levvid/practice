"""
Parses an update_manifest JSON file and returns the package details as a list of tuples - [(package_name, required,
exported),...]
"""

from typing import List


def get_package_dependencies_or_exports(data_type: str, package_description: str) -> List[str]:
    """
    Parse a package's description for data_type (required or exported) and returns the data_type's variable names as
    a list
    :param data_type: 'required' or 'exported'
    :param package_description: a package's description
    :return: list containing the required or exported variable names
    """
    data_type_index = package_description.index(data_type)  # index of 'required' or 'exported'
    start_index = package_description.index('[', data_type_index)  # index of first '[' after data_type_index
    end_index = package_description.index(']', start_index) + 1  # index of ']' after data_type_index

    # remove braces and quotation marks from dependencies or exported variable names
    data_type_segment = package_description[start_index:end_index].replace('[', '').replace(']', '').replace('"', '')

    # remove all spaces and newline chars from variable names and save to list
    variable_names = []
    for variable_name in data_type_segment.split(','):
        variable_name = variable_name.replace('\n', '').strip()
        if variable_name == '':  # skip if no variable names are present for example when package doesn't have any
            # dependencies
            continue
        else:
            if ' ' in variable_name:  # variable names not properly delimited
                print('invalid')
                return
            variable_names.append(variable_name)

    return variable_names


def get_package_data(package_descriptions: List) -> List[tuple]:
    """
    Parses package descriptions using get_package_dependencies_or_exports and returns the package description as a
    list of tuples in the format [(package_name, required, exported), ...]
    :param package_descriptions:
    :return: package descriptions as a list of tuples [(package_name, required, exported), ...]
    """
    package_details = []
    for package_description in package_descriptions:
        package_name = package_description[:package_description.index(':')].replace('"', '').strip()  # name of package
        required = get_package_dependencies_or_exports('required', package_description)  # runtime dependencies for
        # this package
        exported = get_package_dependencies_or_exports('exported', package_description)  # runtime variables
        # exported by this package

        if not package_name or not exported:
            return
        package_details.append((package_name, required, exported))

    return package_details


def parse_json_file(path: str) -> List[tuple]:
    """
    Opens and reads the json file present at path and parses the json data. Returns the package details as a list of
    tuples if the file is valid or prints invalid and returns None if the file is invalid - empty of contains
    invalid json format.
    :param path: path to json file
    :return: package details as a list of tuples of the form [(package_name, required, exported)]
    """
    with open(path) as json_data:
        json_data = json_data.read().strip()
        if len(json_data) == 0:  # empty file
            print('invalid')
            return

        try:
            json_data = json_data[1:-1].strip()  # remove enclosing brackets
            json_data = json_data[json_data.index('{') + 1:-1]  # remove pkgs, last bracket to only remain with package
        except ValueError:  # an invalid file will not contain the braces and will cause a ValueError
            print('invalid')
            return
        # descriptions
        package_descriptions = json_data.split('},')  # split the semi-parsed json data into 'package description'
        # segments

        return get_package_data(package_descriptions)


if __name__ == '__main__':
    path = '/home/gibson/Documents/docs/stable_auto/json/invalid_json.json'
    print(parse_json_file(path))