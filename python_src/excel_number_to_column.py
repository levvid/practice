"""
Given a positive integer, return its corresponding column title as appear in an Excel sheet.

For example:

    1 -> A
    2 -> B
    3 -> C
    ...
    26 -> Z
    27 -> AA
    28 -> AB
    ...
Example 1:

Input: 1
Output: "A"
Example 2:

Input: 28
Output: "AB"
Example 3:

Input: 701
Output: "ZY"

num = num * 26 + (ord(c.upper()) - ord('A')) + 1
"""


def convertToTitle(n: int) -> str:
    string = [""] * 50
    i = 0

    while n > 0:
        # Find remainder
        rem = n % 26

        # if remainder is 0, then a
        # 'Z' must be there in output
        if rem == 0:
            string[i] = 'Z'
            i += 1
            n = (n // 26) - 1
        else:
            string[i] = chr((rem - 1) + ord('A'))
            i += 1
            n = n // 26
    string[i] = ''

    # Reverse the string and print result
    string = string[::-1]
    print("".join(string))

    return "".join(string)
