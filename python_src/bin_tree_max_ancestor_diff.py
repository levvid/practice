"""
Given the root of a binary tree, find the maximum value V for which there exists different nodes
A and B where V = |A.val - B.val| and A is an ancestor of B.

(A node A is an ancestor of B if either: any child of A is equal to B, or any child of A is an ancestor of B.)

Example 1:

Input: [8,3,10,1,6,null,14,null,null,4,7,13]
Output: 7
Explanation:
We have various ancestor-node differences, some of which are given below :
|8 - 3| = 5
|3 - 7| = 4
|8 - 1| = 7
|10 - 13| = 3
Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.


Note:

The number of nodes in the tree is between 2 and 5000.
Each node will have value between 0 and 100000.
"""
from bin_tree_delete_nodes import TreeNode


def maxAncestorDiff(self, root: TreeNode) -> int:
    def maxDiffUtil(t, res):
        if t is None:
            return 0, res

        """ If leaf node then just return 
            node's value """
        if t.left is None and t.right is None:
            return t.val, res

        """ Recursively calling left and right  
        subtree for minimum value """
        a, res = maxDiffUtil(t.left, res)
        b, res = maxDiffUtil(t.right, res)
        val_ = min(a, b)

        """ Updating res if (node value - minimum  
        value from subtree) is bigger than res """
        res = max(res, t.val - val_)

        """ Returning minimum value got so far """
        return min(val_, t.val), res

    _, val = maxDiffUtil(root, 0)

    print(val)
