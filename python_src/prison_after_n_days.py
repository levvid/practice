"""

There are 8 prison cells in a row, and each cell is either occupied or vacant.

Each day, whether the cell is occupied or vacant changes according to the following rules:

If a cell has two adjacent neighbors that are both occupied or both vacant, then the cell becomes occupied.
Otherwise, it becomes vacant.
(Note that because the prison is a row, the first and the last cells in the row can't have two adjacent neighbors.)

We describe the current state of the prison in the following way: cells[i] == 1 if the i-th cell is occupied, else cells[i] == 0.

Given the initial state of the prison, return the state of the prison after N days (and N such changes described above.)



Example 1:

Input: cells = [0,1,0,1,1,0,0,1], N = 7
Output: [0,0,1,1,0,0,0,0]
Explanation:
The following table summarizes the state of the prison on each day:
Day 0: [0, 1, 0, 1, 1, 0, 0, 1]
Day 1: [0, 1, 1, 0, 0, 0, 0, 0]
Day 2: [0, 0, 0, 0, 1, 1, 1, 0]
Day 3: [0, 1, 1, 0, 0, 1, 0, 0]
Day 4: [0, 0, 0, 0, 0, 1, 0, 0]
Day 5: [0, 1, 1, 1, 0, 1, 0, 0]
Day 6: [0, 0, 1, 0, 1, 1, 0, 0]
Day 7: [0, 0, 1, 1, 0, 0, 0, 0]

Example 2:

Input: cells = [1,0,0,1,0,0,1,0], N = 1000000000
Output: [0,0,1,1,1,1,1,0]


Note:

cells.length == 8
cells[i] is in {0, 1}
1 <= N <= 10^9
"""
from typing import List

cells = [0,1,0,1,1,0,0,1]
N = 7

def prisonAfterNDays(cells: List[int], N: int) -> List[int]:
    ret = cells
    for i in range(N):
        ret = prisonCycle(ret)

    print('Cells: {}'.format(cells))
    print('Ret:   {}'.format(ret))
    # print(ret)


def prisonCycle(cells: List[int]) -> List[int]:
    ret = [None]*len(cells)
    ret[0] = 0
    ret[len(cells)-1] = 0
    for i in range(1, len(cells)-1):
        if cells[i-1] == cells[i+1]:
            ret[i] = 1
        else:
            ret[i] = 0

    return ret


def prisonAfterNDaysTimely(self, cells: List[int], N: int) -> List[int]:
    def nextday(cells):
        return [int(i > 0 and i < 7 and cells[i - 1] == cells[i + 1])
                for i in range(8)]

    seen = {}
    while N > 0:
        c = tuple(cells)
        if c in seen:
            tmp = N
            N %= seen[c] - N
            print('N is {} seen c is {} before N is {}'.format(N, seen[c], tmp))
        seen[c] = N

        if N >= 1:
            N -= 1
            cells = nextday(cells)
    # print(seen)

    return cells
