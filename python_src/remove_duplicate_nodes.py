"""
Given a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list.

Return the linked list sorted as well.

Example 1:

Input: 1->2->3->3->4->4->5
Output: 1->2->5
Example 2:

Input: 1->1->1->2->3
Output: 2->3

"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        ret = set()
        lst = []

        curr = head

        while curr:
            if curr.val in ret:
                if curr.val in lst:
                    lst.remove(curr.val)
                curr = curr.next
                continue
            ret.add(curr.val)

            lst.append(curr.val)
            curr = curr.next

        ret = list(sorted(ret))

        dummy = ListNode(0)
        prev = dummy
        for elem in lst:
            tmp = ListNode(elem)
            if ret.index(elem) == 0:  # set head
                dummy.next = tmp
                prev = tmp
            else:
                prev.next = tmp
                prev = tmp

        return dummy.next


        
