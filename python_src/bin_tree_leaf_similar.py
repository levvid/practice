"""
Consider all the leaves of a binary tree.  From left to right order, the values of those leaves form a leaf value
sequence.

For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).

Two binary trees are considered leaf-similar if their leaf value sequence is the same.

Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.

Constraints:

Both of the given trees will have between 1 and 200 nodes.
Both of the given trees will have values between 0 and 200
"""
from bin_tree_inorder_traversal import TreeNode


def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:
    def find_leaf_val_sequence(node, seq):
        if node:
            if not node.left and not node.right:
                seq.append(node.val)
            find_leaf_val_sequence(node.left, seq)
            find_leaf_val_sequence(node.right, seq)

    seq1 = []
    seq2 = []
    find_leaf_val_sequence(root2, seq1)
    find_leaf_val_sequence(root1, seq2)

    if seq1 == seq2:
        return True
    return False
