"""
Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its minimum depth = 2.
"""

from bin_tree_inorder_traversal import TreeNode


def minDepth(self, root: TreeNode) -> int:
    def dfs(node, height):
        if node:
            if not node.left and not node.right:
                self.height.append(height)
            dfs(node.left, height + 1)
            dfs(node.right, height + 1)

    self.height = []
    if not root:
        return 0
    dfs(root, 1)

    return sorted(self.height)[0]