"""
There are n bulbs that are initially off. You first turn on all the bulbs.
Then, you turn off every second bulb. On the third round, you toggle every third bulb (turning on if it's off or
turning off if it's on). For the i-th round, you toggle every i bulb. For the n-th round, you only toggle the last bulb.
Find how many bulbs are on after n rounds.

Example:

Input: 3
Output: 1
Explanation:
At first, the three bulbs are [off, off, off].
After first round, the three bulbs are [on, on, on].
After second round, the three bulbs are [on, off, on].
After third round, the three bulbs are [on, off, off].

So you should return 1, because there is only one bulb is on.
"""
num = 3
def bulb_switch(n: int) -> int:
    tmp_list = [1]*n
    print(tmp_list)
    for i in range(1, n):
        print('i be4 is {} list is: {}'.format(i, tmp_list))
        if i == 1:
            for i in range(len(tmp_list)):
                if i%2 == 0 and i != 0:
                    tmp_list[i] = 0
        elif i > 1:
            print(' i is {}'.format(i))
            if n%(i-1) == 0:
                if tmp_list[i] == 1:
                    tmp_list[i] = 0
                else:
                    tmp_list[i] = 1
        print('i is {} list is: {}'.format(i, tmp_list))

    num = 0
    for tmp in tmp_list:
        if tmp == 1:
            num += 1

    print(num)
    return num

