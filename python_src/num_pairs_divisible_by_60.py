"""
In a list of songs, the i-th song has a duration of time[i] seconds.

Return the number of pairs of songs for which their total duration in seconds is divisible by 60.  Formally, we want the number of indices i, j such that i < j with (time[i] + time[j]) % 60 == 0.



Example 1:

Input: [30,20,150,100,40]
Output: 3
Explanation: Three pairs have a total duration divisible by 60:
(time[0] = 30, time[2] = 150): total duration 180
(time[1] = 20, time[3] = 100): total duration 120
(time[1] = 20, time[4] = 40): total duration 60
Example 2:

Input: [60,60,60]
Output: 3
Explanation: All three pairs have a total duration of 120, which is divisible by 60.


Note:

1 <= time.length <= 60000
1 <= time[i] <= 500


"""
from typing import List

time = [30,20,150,100,40]
time = [60,60,60]
time = [20, 40, 40]

time = [3,6,9, 12]
def numPairsDivisibleBy60(time: List[int]) -> int:
    if len(time) < 2:
        return 0
    num_pairs = 0

    time_mat = [[None]*len(time)*len(time)]
    print(time_mat)

    for i in range(len(time)-1):
        for j in range(i+1, len(time)):
            if (time[i] + time[j])%60 == 0:
                num_pairs += 1

    return num_pairs

def numPairsTest(time: List[int]) -> int:
    K = 3
    n = len(time)
    # Create a frequency array to count
    # occurrences of all remainders when
    # divided by K
    freq = [0] * K

    # Count occurrences of all remainders
    for i in range(n):
        freq[time[i] % K] += 1

    print(freq)
    # If both pairs are divisible by 'K'
    sum = freq[0] * (freq[0] - 1) / 2

    # count for all i and (k-i)
    # freq pairs
    i = 1
    while (i <= K // 2 and i != (K - i)):
        print(i)
        sum += freq[i] * freq[K - i]  # the quick brown fox
        i += 1

    # If K is even
    if (K % 2 == 0):
        sum += (freq[K // 2] * (freq[K // 2] - 1) / 2)

    return int(sum)
