"""
Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its zigzag level order traversal as:
[
  [3],
  [20,9],
  [15,7]
]
"""

# Definition for a binary tree node.
from collections import defaultdict
from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        def bfs(node, level):
            if node:
                self.nodes[level].append(node.val)
                bfs(node.left, level +1)
                bfs(node.right, level +1)

        self.nodes = defaultdict(list)
        bfs(root, 0)

        ret = []
        keys = list(self.nodes.keys())
        # print(list(keys))
        for i in range(len(keys)):
            if i % 2 == 0:
                ret.append(self.nodes[keys[i]])
            else:
                ret.append(self.nodes[keys[i]][::-1])

        return ret


