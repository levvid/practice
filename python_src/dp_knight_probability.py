"""
https://www.geeksforgeeks.org/probability-knight-remain-chessboard/

On an NxN chessboard, a knight starts at the r-th row and c-th column and attempts to make exactly K moves. The rows and columns are 0 indexed, so the top-left square is (0, 0), and the bottom-right square is (N-1, N-1).

A chess knight has 8 possible moves it can make, as illustrated below. Each move is two squares in a cardinal direction, then one square in an orthogonal direction.







Each time the knight is to move, it chooses one of eight possible moves uniformly at random (even if the piece would go
off the chessboard) and moves there.

The knight continues moving until it has made exactly K moves or has moved off the chessboard. Return the probability
that the knight remains on the board after it has stopped moving.



Example:

Input: 3, 2, 0, 0
Output: 0.0625
Explanation: There are two moves (to (1,2), (2,1)) that will keep the knight on the board.
From each of those positions, there are also two moves that will keep the knight on the board.
The total probability the knight stays on the board is 0.0625.


Note:

N will be between 1 and 25.
K will be between 0 and 100.
The knight always initially starts on the board.

"""


def knightProbability(self, N: int, K: int, r: int, c: int) -> float:
    # Direction vector for the Knight
    dx = [1, 2, 2, 1, -1, -2, -2, -1]
    dy = [2, 1, -1, -2, -2, -1, 1, 2]

    # returns true if the knight
    # is inside the chessboard
    def inside(x, y):

        return (x >= 0 and x < N and y >= 0 and y < N)

        # Bottom up approach for finding the

    # probability to go out of chessboard.
    def findProb(start_x, start_y, steps):

        # dp array
        dp1 = [[[0 for i in range(N + 1)]
                for j in range(N + 1)]
               for k in range(N + 1)]

        # For 0 number of steps, each
        # position will have probability 1
        for i in range(N):

            for j in range(N):
                dp1[i][j][0] = 1

        # for every number of steps s
        for s in range(1, steps + 1):

            # for every position (x,y) after
            # s number of steps
            for x in range(N):

                for y in range(N):
                    prob = 0.0

                    # For every position reachable from (x,y)
                    for i in range(8):
                        nx = x + dx[i]
                        ny = y + dy[i]

                        # if this position lie inside the board
                        if inside(nx, ny):
                            prob += dp1[nx][ny][s - 1] / 8.0

                    # store the result
                    dp1[x][y][s] = prob

                    # return the result
        return dp1[start_x][start_y][steps]

    return findProb(r, c, K)