"""
In a binary tree, the root node is at depth 0, and children of each depth k node are at depth k+1.
Two nodes of a binary tree are cousins if they have the same depth, but have different parents.
We are given the root of a binary tree with unique values, and the values x and y of two different nodes in the tree.
Return true if and only if the nodes corresponding to the values x and y are cousins.

Example 1:

Input: root = [1,2,3,4], x = 4, y = 3
Output: false

Example 2:

Input: root = [1,2,3,null,4,null,5], x = 5, y = 4
Output: true

Example 3:

Input: root = [1,2,3,null,4], x = 2, y = 3
Output: false

Note:
The number of nodes in the tree will be between 2 and 100.
Each node has a unique integer value from 1 to 100.
"""


# Definition for a binary tree node.
from collections import defaultdict
from math import inf


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def isCousins(self, root: TreeNode, x: int, y: int) -> bool:
    tree_dict = defaultdict()

    # print('B4 {}'.format(tree_dict))
    self.traverse_bst(root, tree_dict)
    # print('After {}'.format(tree_dict))

    level_x = tree_dict[x]
    level_y = tree_dict[y]

    if level_x == level_y:
        return True
    return False


def traverse_bst(self, node: TreeNode, tree: dict, level: int = 0) -> dict:
    if node:
        self.traverse_bst(node.left, tree, level + 1)
        self.traverse_bst(node.right, tree, level + 1)
        tree[node.val] = level

