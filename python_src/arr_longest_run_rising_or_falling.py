
def stock_runs(prices):
    is_rising = True
    max_days = 0
    tmp_days = 1

    for i in range(1, len(prices)):
        if prices[i] > prices[ i -1]:
            if is_rising:
                tmp_days += 1
            else:
                is_rising = True
                max_days = max(tmp_days, max_days)
                tmp_days = 1
        elif prices[i] < prices[ i -1]:
            if is_rising:
                is_rising = False
                max_days = max(max_days, tmp_days)
                tmp_days = 2  # first day was the prev day
            else:
                tmp_days += 1
        else:
            tmp_days = 1
    max_days = max(tmp_days, max_days)

    return max_days