"""
Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

Example 1:

Input:
[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
Output: [1,2,3,6,9,8,7,4,5]
Example 2:

Input:
[
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9,10,11,12]
]
Output: [1,2,3,4,8,12,11,10,9,5,6,7]
"""
from typing import List


def spiralOrder(matrix: List[List[int]]) -> List[int]:
    arr = []

    def helper(m, n, a):
        k = 0
        l = 0

        ''' k - starting row index 
            m - ending row index 
            l - starting column index 
            n - ending column index 
            i - iterator '''
        while k < m and l < n:

            # Print the first row from
            # the remaining rows
            for i in range(l, n):
                arr.append(a[k][i])

            k += 1

            # Print the last column from
            # the remaining columns
            for i in range(k, m):
                arr.append(a[i][n - 1])

            n -= 1

            # Print the last row from
            # the remaining rows
            if k < m:

                for i in range(n - 1, (l - 1), -1):
                    arr.append(a[m - 1][i])

                m -= 1

            # Print the first column from
            # the remaining columns
            if l < n:
                for i in range(m - 1, k - 1, -1):
                    arr.append(a[i][l])
                l += 1

    if len(matrix) == 0:
        return arr
    helper(len(matrix), len(matrix[0]), matrix)

    return arr
