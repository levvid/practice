"""
Design your implementation of the circular queue. The circular queue is a linear data structure in which the operations are performed based on FIFO (First In First Out) principle and the last position is connected back to the first position to make a circle. It is also called "Ring Buffer".

One of the benefits of the circular queue is that we can make use of the spaces in front of the queue. In a normal queue, once the queue becomes full, we cannot insert the next element even if there is a space in front of the queue. But using the circular queue, we can use the space to store new values.

Your implementation should support following operations:

MyCircularQueue(k): Constructor, set the size of the queue to be k.
Front: Get the front item from the queue. If the queue is empty, return -1.
Rear: Get the last item from the queue. If the queue is empty, return -1.
enQueue(value): Insert an element into the circular queue. Return true if the operation is successful.
deQueue(): Delete an element from the circular queue. Return true if the operation is successful.
isEmpty(): Checks whether the circular queue is empty or not.
isFull(): Checks whether the circular queue is full or not.

Example:

MyCircularQueue circularQueue = new MyCircularQueue(3); // set the size to be 3
circularQueue.enQueue(1);  // return true
circularQueue.enQueue(2);  // return true
circularQueue.enQueue(3);  // return true
circularQueue.enQueue(4);  // return false, the queue is full
circularQueue.Rear();  // return 3
circularQueue.isFull();  // return true
circularQueue.deQueue();  // return true
circularQueue.enQueue(4);  // return true
circularQueue.Rear();  // return 4

Note:

All values will be in the range of [0, 1000].
The number of operations will be in the range of [1, 1000].
Please do not use the built-in Queue library.
"""


class MyCircularQueue:

    def __init__(self, k: int):
        """
        Initialize your data structure here. Set the size of the queue to be k.
        """
        self.queue = [None] * k
        self.num_elements = 0
        self.size = k
        self.front = 0
        self.back = 0

    def enQueue(self, value: int) -> bool:
        """
        Insert an element into the circular queue. Return true if the operation is successful.
        """
        if self.isFull():
            return False
        else:
            if self.num_elements != 0:
                self.back = (self.back + 1) % self.size
            self.queue[self.back] = value
            self.num_elements += 1
            return True

    def deQueue(self) -> bool:
        """
        Delete an element from the circular queue. Return true if the operation is successful.
        """
        if self.num_elements > 0:
            self.num_elements -= 1
            ret = self.queue[self.front]
            self.front = (self.front + 1) % self.size
            return True

        else:
            return False

    def Front(self) -> int:
        """
        Get the front item from the queue.
        """
        if self.num_elements > 0:
            return self.queue[self.front]
        else:
            return -1

    def Rear(self) -> int:
        """
        Get the last item from the queue.
        """
        if self.num_elements > 0:
            return self.queue[self.back]
        else:
            return -1

    def isEmpty(self) -> bool:
        """
        Checks whether the circular queue is empty or not.
        """
        return self.num_elements == 0

    def isFull(self) -> bool:
        """
        Checks whether the circular queue is full or not.
        """
        return self.num_elements == self.size


if __name__ == '__main__':
    obj = MyCircularQueue(3)
    obj.enQueue(1)
    obj.enQueue(2)
    obj.enQueue(3)
    obj.deQueue()
    obj.num_elements
    s = obj.deQueue()
    print('Front: {}'.format(obj.front))
    print('Back: {}'.format(obj.back))
    print('Front of queue {}'.format(obj.Front()))
    print('Back of queue {}'.format(obj.Rear()))
    # print('S is {}'.format(s))
    print('is full {}'.format(obj.isFull()))
