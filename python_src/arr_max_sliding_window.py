"""
Given an array nums, there is a sliding window of size k which is moving from the very left of the array to the
very right. You can only see the k numbers in the window. Each time the sliding window moves right by one position.
Return the max sliding window.

Follow up:
Could you solve it in linear time?

Example:

Input: nums = [1,3,-1,-3,5,3,6,7], and k = 3
Output: [3,3,5,5,6,7]
Explanation:

Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7


Constraints:

1 <= nums.length <= 10^5
-10^4 <= nums[i] <= 10^4
1 <= k <= nums.length
"""
from collections import deque
from typing import List


class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        ret = []

        for i in range(len(nums)):
            ret.append(max(nums[i:i + k]))
            if i + k >= len(nums) - 1:
                break

        return ret

    def maxSlidingWindowDeque(self, nums: List[int], k: int) -> List[int]:
        Qi = deque()
        ret = []
        arr = nums
        n = len(nums)

        # Process first k (or first window)
        # elements of array
        for i in range(k):

            # For every element, the previous
            # smaller elements are useless
            # so remove them from Qi
            while Qi and arr[i] >= arr[Qi[-1]]:
                Qi.pop()
                # Add new element at rear of queue
            Qi.append(i)

            # Process rest of the elements, i.e.
        # from arr[k] to arr[n-1]
        for i in range(k, n):

            # The element at the front of the
            # queue is the largest element of
            # previous window, so print it
            ret.append(arr[Qi[0]])

            # Remove the elements which are
            # out of this window
            while Qi and Qi[0] <= i - k:
                # remove from front of deque
                Qi.popleft()

            # Remove all elements smaller than
            # the currently being added element
            # (Remove useless elements)
            while Qi and arr[i] >= arr[Qi[-1]]:
                Qi.pop()
                # Add current element at the rear of Qi
            Qi.append(i)

            # Print the maximum element of last window
        ret.append(arr[Qi[0]])

        return ret
