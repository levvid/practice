TEST = 1000


def int_to_roman(num: int) -> str:
    special_cases = [
        1000, 900, 500, 400,
        100, 90, 50, 40,
        10, 9, 5, 4,
        1
    ]
    roman_symbols = [
        "M", "CM", "D", "CD",
        "C", "XC", "L", "XL",
        "X", "IX", "V", "IV",
        "I"
    ]
    roman_numeral = ''
    i = 0
    while num > 0:
        for _ in range(num // special_cases[i]):
            roman_numeral += roman_symbols[i]
            num -= special_cases[i]
        i += 1
    return roman_numeral


def num_x_in_roman() -> int:
    start = 1
    end = 2660
    count = 0

    for i in range(start, end + 1):
        roman_numeral = int_to_roman(i)
        count += roman_numeral.count('X')

    return count


TEST = 'Dealing with failure is easy: Work hard to improve. Success is also easy to handle: ' \
       'You’ve solved the wrong problem. Work hard to improve.'


# TEST = 'iffy'


def sum_of_sentence(sentence: str) -> int:
    vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
    sentence_sum = 0
    for letter in sentence:
        if letter.isalpha():
            if letter in vowels:  # is a vowel, use inverse
                tmp = -ord(letter)
                sentence_sum += tmp
            else:
                sentence_sum += ord(letter)

    return sentence_sum


def is_palindrome(num):
    tmp = str(num)

    return tmp == tmp[::-1]


def palindrome_sum():
    start = 1
    end = 10000
    sum_of_pals = 0

    for i in range(start, end):
        if is_palindrome(i):
            sum_of_pals += i

    return sum_of_pals


fibonacci_nums = [0, 1]


def fibonacci(n):
    if n < 0:
        print("Incorrect input")
    elif n <= len(fibonacci_nums):
        return fibonacci_nums[n - 1]
    else:
        temp_fib = fibonacci(n - 1) + fibonacci(n - 2)
        if temp_fib >= 10000:
            return temp_fib
        fibonacci_nums.append(temp_fib)
        return temp_fib


if __name__ == '__main__':
    # fibonacci(25)
    # sum_odd = 0
    # for i in range(len(fibonacci_nums)):
    #     if fibonacci_nums[i]%2 == 1:
    #         sum_odd += fibonacci_nums[i]
    # print('Sum of odd nums is {}'.format(sum_odd))
    # sum_of_pals = palindrome_sum()
    # print('Sum is {}'.format(sum_of_pals))
    # sentence_sum = sum_of_sentence(TEST)
    # print('Sum of sentence is {}'.format(sentence_sum))
    num_occurrences = num_x_in_roman()
    print('There are {} X in the range 1 - 2660'.format(num_occurrences))
