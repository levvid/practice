"""
Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its level order traversal as:
[
  [3],
  [9,20],
  [15,7]
]
"""
from collections import defaultdict
from typing import List



class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def levelOrder(self, root: TreeNode) -> List[List[int]]:
    node_dict = defaultdict(list)
    self.bst(root, 0, node_dict)
    print(node_dict.values())

    return node_dict.values()


def bst(self, node, level, node_dict) -> None:
    if node:
        node_dict[level].append(node.val)
        self.bst(node.left, level + 1, node_dict)
        self.bst(node.right, level + 1, node_dict)
    else:
        return None
