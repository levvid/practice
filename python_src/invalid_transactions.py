"""
A transaction is possibly invalid if:

the amount exceeds $1000, or;
if it occurs within (and including) 60 minutes of another transaction with the same name in a different city.
Each transaction string transactions[i] consists of comma separated values representing the name, time (in minutes), amount, and city of the transaction.

Given a list of transactions, return a list of transactions that are possibly invalid.  You may return the answer in any order.



Example 1:

Input: transactions = ["alice,20,800,mtv","alice,50,100,beijing"]
Output: ["alice,20,800,mtv","alice,50,100,beijing"]
Explanation: The first transaction is invalid because the second transaction occurs within a difference of 60 minutes, have the same name and is in a different city. Similarly the second one is invalid too.
Example 2:

Input: transactions = ["alice,20,800,mtv","alice,50,1200,mtv"]
Output: ["alice,50,1200,mtv"]
Example 3:

Input: transactions = ["alice,20,800,mtv","bob,50,1200,mtv"]
Output: ["bob,50,1200,mtv"]


Constraints:

transactions.length <= 1000
Each transactions[i] takes the form "{name},{time},{amount},{city}"
Each {name} and {city} consist of lowercase English letters, and have lengths between 1 and 10.
Each {time} consist of digits, and represent an integer between 0 and 1000.
Each {amount} consist of digits, and represent an integer between 0 and 2000.
"""
from collections import defaultdict
from typing import List

# name, time, amount, city
transaction_list = ["alice,20,1100,mtv", "alice,50,100,beijing", "alice,500,100,nairobi"]


def invalidTransactions(transactions: List[str]) -> List[str]:
    invalid_transactions = {}
    name_dict = defaultdict(lambda: defaultdict(list))

    for transaction in transactions:
        # print(transaction)
        transaction_list = transaction.split(',')
        name = transaction_list[0]
        tym = transaction_list[1]
        amt = transaction_list[2]
        city = transaction_list[3]
        name_dict[name][city].append((tym, transaction))
        if int(amt) > 1000:
            invalid_transactions[transaction] = transaction
        # print(transaction)
        # print(invalid_transactions.keys())
        # print(name_dict.values())

    names = name_dict.keys()
    print(name_dict)
    for name in names:
        cities = name_dict[name].keys()
        length = len(cities)
        curr = 0
        city_tmp = cities[0]
        i = 0
        for city in cities:
            if city_tmp != city:
                tym_transactions = name_dict[name][city]


def find_invalids_cross_city(amt_transactions: List) -> dict:
    length = len(amt_transactions)
    invalid_transactions = {}
    if length <= 1:
        return {}

    for i in range(0, len(amt_transactions)):
        if i == len(amt_transactions) - 1:
            break
        for j in range(i + 1, len(amt_transactions)):
            tym1, transaction1 = amt_transactions[i - 1]
            tym2, transaction2 = amt_transactions[i]
            if abs(int(tym1) - int(tym2)) <= 60:
                invalid_transactions[transaction1] = transaction1
                invalid_transactions[transaction2] = transaction2

    return invalid_transactions

def invalidTransactions(self, transactions: List[str]) -> List[str]:
        def isInvalid(maxTime, S, i, name, city):
            for j in range(max(0, i - 60), min(maxTime + 1, i + 61)):
                if (j, name) in S:
                    aux = 1 if city in S[(j, name)] else 0
                    if len(S[(j, name)]) > aux:
                        return True

            return False

        I = []
        maxTime = 0
        for t in transactions:
            name, time, amount, city = t.split(",")
            maxTime = max(maxTime, int(time))

        S = {}
        for t in transactions:
            name, time, amount, city = t.split(",")
            time = int(time)
            S[(time, name)] = S.get((time, name), set())
            S[(time, name)].add(city)

        for t in transactions:
            name, time, amount, city = t.split(",")

            if int(amount) > 1000 or isInvalid(maxTime, S, int(time), name, city):
                I.append(t)

        return I
