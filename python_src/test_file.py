from collections import defaultdict


class Graph:
    def __init__(self, vertices):
        self.V = vertices
        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)

    def find_all_paths(self, src, dst):
        paths = []
        def find_all_paths_util(src, dst, visited, path, paths):
            visited[src] = True
            path.append(src)

            if src == dst:
                paths.append(path)
                print(path)
            else:
                for i in self.graph[src]:
                    if not visited[i]:
                        find_all_paths_util(i, dst, visited, path, paths)

            path.pop()
            visited[src] = False

        visited = [False] * 10
        path = []

        find_all_paths_util(src, dst, visited, path, paths)

        print('Paths {}'.format(path))



def getMinMagic(src, dst, wizards):
    minCost = 0
    minPath = []
    paths = []
    # Put your code here to calculate minCost and minPath
    graph = Graph(len(wizards))

    for i in range(len(wizards)):
        for wizard in wizards[i]:
            graph.addEdge(i, wizard)

    graph.find_all_paths(src, dst)


    minCost = float('inf')
    for i in range(len(paths)):
        tmp_cost = 0
        for j in range(len(paths[i]) - 1):
            tmp_cost += (paths[i][j + 1] - paths[i][j]) ** 2

        if tmp_cost < minCost:
            minCost = tmp_cost
            minPath = paths[i]

    # Return the result, do not change the structure
    return minCost, minPath


if __name__ == '__main__':
    src = 0
    dst = 9
    matrix = [
        [1, 2, 3],
        [8, 6, 4],
        [7, 8, 3],
        [8, 1],
        [6],
        [8, 7],
        [9, 4],
        [4, 6],
        [1],
        [1, 4],
    ]
    minCost, minPath = getMinMagic(src, dst, matrix)

    print('Min cost {} min path {}'.format(minCost, minPath))
