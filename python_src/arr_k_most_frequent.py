"""
Given a non-empty list of words, return the k most frequent elements.

Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency,
then the word with the lower alphabetical order comes first.

Example 1:
Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
Output: ["i", "love"]
Explanation: "i" and "love" are the two most frequent words.
    Note that "i" comes before "love" due to a lower alphabetical order.
Example 2:
Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
Output: ["the", "is", "sunny", "day"]
Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
    with the number of occurrence being 4, 3, 2 and 1 respectively.
Note:
You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
Input words contain only lowercase letters.
Follow up:
Try to solve it in O(n log k) time and O(n) extra space.
"""
from collections import defaultdict
from operator import itemgetter
from typing import List

arr = ["i", "love", "leetcode", "i", "love", "coding", "apple", "apple"]
K = 3


def topKFrequent(words: List[str], k: int) -> List[str]:
    def multisort(xs, specs):
        for key, reverse in reversed(specs):
            xs.sort(key=itemgetter(key), reverse=reverse)
        return xs

    word_dict = defaultdict(int)
    for word in words:
        word_dict[word] += 1

    ret = multisort([(key, value) for key, value in word_dict.items()], ((1, True), (0, False)))[:k]
    print(ret)
    ret = [key for key, _ in ret]

    return ret
