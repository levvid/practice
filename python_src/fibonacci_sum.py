"""
The Fibonacci sequence begins like this:
0, 1, 1, 2, 3, 5, 8, 13, 21, 34
(each number is the sum of the previous two)

What is the sum of all odd numbers in the Fibonacci sequence that are less
than 10,000?
"""


fib_arr = [0, 1]


def fibonacci(n):
    if n < 0:
        print("Incorrect input")
    elif n <= len(fib_arr):
        return fib_arr[n - 1]
    else:
        temp_fib = fibonacci(n - 1) + fibonacci(n - 2)
        fib_arr.append(temp_fib)
        return temp_fib


if __name__ == '__main__':
    print(fibonacci(22))
    print(fib_arr)

    sum_ = 0
    for i in fib_arr:
        if i % 2 == 1 and i < 10000:
            sum_ += i

    print(sum_)