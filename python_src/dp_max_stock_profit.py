"""
Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock),
design an algorithm to find the maximum profit.

Note that you cannot sell a stock before you buy one.

Example 1:

Input: [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
             Not 7-1 = 6, as selling price needs to be larger than buying price.
Example 2:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
"""
from typing import List

prices = [7,1,5,3,6,4]
prices = [7,6,4,3,1]
prices = [4,1,2]


def maxProfit(prices: List[int]) -> int:
    max_profit = 0

    for i in range(len(prices)):
        for j in range(i + 1, len(prices)):
            profit = prices[j] - prices[i]
            if profit > 0:
                max_profit = max(max_profit, profit)

    return max_profit


def maxProfit(self, prices: List[int]) -> int:
    if len(prices) == 0:
        return 0

    max_profit = 0
    min_price = prices[0]

    for i in range(1, len(prices)):
        if prices[i] < min_price:
            min_price = prices[i]
        else:
            # max_profit = max(max_profit, prices[i] - min_price)
            if prices[i] - min_price > max_profit:
                max_profit = prices[i] - min_price

    return max_profit





def maxProfit(self, prices: List[int]) -> int:
    return self.maxProfitHelper(prices, len(prices), 1)


def maxProfitHelper(self, price, n, k):
    # Table to store results of subproblems
    # profit[t][i] stores maximum profit
    # using atmost t transactions up to
    # day i (including day i)
    profit = [[0 for i in range(n + 1)]
              for j in range(k + 1)]

    # Fill the table in bottom-up fashion
    for i in range(1, k + 1):
        prevDiff = float('-inf')

        for j in range(1, n):
            prevDiff = max(prevDiff,
                           profit[i - 1][j - 1] -
                           price[j - 1])
            profit[i][j] = max(profit[i][j - 1],
                               price[j] + prevDiff)

    return profit[k][n - 1]



#     def maxProfitHelper(self, price, start, end) -> int:

#         # If the stocks can't be bought
#         if (end <= start):
#             return 0;

#         # Initialise the profit
#         profit = 0;
#         sorted_list = price.copy()
#         sorted_list.sort(reverse=True)
#         if sorted_list == price:
#             return profit

#         # The day at which the stock
#         # must be bought
#         for i in range(start, end):
#             # The day at which the
#             # stock must be sold
#             for j in range(i+1, end+1):
#                 # If byuing the stock at ith day and
#                 # selling it at jth day is profitable
#                 if (price[j] > price[i]):

#                     # Update the current profit
#                     curr_profit = price[j] - price[i]

#                     # Update the maximum profit so far
#                     profit = max(profit, curr_profit);

#         return profit


