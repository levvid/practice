"""
Implement a SnapshotArray that supports the following interface:

SnapshotArray(int length) initializes an array-like data structure with the given length.  Initially, each element equals 0.
void set(index, val) sets the element at the given index to be equal to val.
int snap() takes a snapshot of the array and returns the snap_id: the total number of times we called snap() minus 1.
int get(index, snap_id) returns the value at the given index, at the time we took the snapshot with the given snap_id


Example 1:

Input: ["SnapshotArray","set","snap","set","get"]
[[3],[0,5],[],[0,6],[0,0]]
Output: [null,null,0,null,5]
Explanation:
SnapshotArray snapshotArr = new SnapshotArray(3); // set the length to be 3
snapshotArr.set(0,5);  // Set array[0] = 5
snapshotArr.snap();  // Take a snapshot, return snap_id = 0
snapshotArr.set(0,6);
snapshotArr.get(0,0);  // Get the value of array[0] with snap_id = 0, return 5


Constraints:

1 <= length <= 50000
At most 50000 calls will be made to set, snap, and get.
0 <= index < length
0 <= snap_id < (the total number of times we call snap())
0 <= val <= 10^9
"""
from collections import defaultdict
from copy import deepcopy


class SnapshotArray:
    def __init__(self, length: int):
        self.snap_id = 0
        self.snap_history = defaultdict(list)
        self.array = [0]*length

    def set(self, index: int, val: int) -> None:
        self.array[index] = val

    def snap(self) -> int:
        self.snap_history[self.snap_id] = deepcopy(self.array)
        self.snap_id += 1
        return self.snap_id - 1

    def get(self, index: int, snap_id: int) -> int:
        return self.snap_history[snap_id][index]


if __name__ == '__main__':
    obj = SnapshotArray(3)
    obj.set(0, 5);
    s1 = obj.snap(); # return snap_id = 0
    print('S1 {}'.format(s1))
    obj.set(0, 6)
    print('Set {}'.format(obj.array))
    ar0 = obj.get(0, 0); # Get the value of array[0] with snap_id = 0, return 5
    print('Arr 0 is {}'.format(ar0))  #

    # # import pdb; pdb.set_trace()
    # # print(obj.)
    # obj.set(0,1)
    #
    # obj.set(1, 2)
    # print('Snap H: {}'.format(obj.snap_history.items()))
    # s = obj.snap()
    # print('S is {} '.format(s))
    # print('Snap H2: {}'.format(obj.snap_history.items()))
    # g = obj.get(1, s)
    # print('G is {}'.format(g))