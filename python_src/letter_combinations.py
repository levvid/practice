"""
Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could
represent.

A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any
letters.

Example:

Input: "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
Note:

Although the above answer is in lexicographical order, your answer could be in any order you want.
"""
import itertools
from typing import List


def letterCombinations(self, digits: str) -> List[str]:
    if len(digits) == 0:
        return []

    num_map = {
        '2': 'abc',
        '3': 'def',
        '4': 'ghi',
        '5': 'jkl',
        '6': 'mno',
        '7': 'pqrs',
        '8': 'tuv',
        '9': 'wxyz'
    }

    letter_list = []

    for digit in digits:
        if digit in num_map.keys():
            letter_list.append(num_map[digit])

    tmp = list(itertools.product(*letter_list))
    # print(tmp)
    return [''.join(i) for i in tmp]
